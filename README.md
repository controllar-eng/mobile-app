
# Controllar Gestão
---
# Enviroment

- Node Version:
```14.18.1```
- Npm Version:
```6.14.15```
- (TIP)
    - Use NVM to controll Node version on machine and use just bash terminal for it:
        ```nvm install 14.18.1```
        ```nvm use 14.18.1```
        ```nvm alias default 14.18.1```

## Android

- Install AndroidStudio (App Store or <a href="https://developer.android.com/studio">Click Here and execute file ```studio.sh```</a>)
- Open AndroidStudio > Settings > Appearance > System Settings > Android SDK > SDK Tools Tab > Select Show Packages Datails
    - Android SDK Build- Tools 33
        ```Select All```
    - NDK:
        ```Select None```
    - CMake:
        ```Select 3.18.1```
    - Select Android Emulator
    - Select Android SDK Platform-Tools

- Note the Sdk installation folder. Ex: ```~/Android/Sdk``` -> ```(PATH_SDK_VERSION)```

- Note the Jdk installation folder. Ex: ```/usr/lib/jvm/java-11-openjdk-amd64``` -> ```(PATH_JDK_VERSION)```

- Search to these files in ```/``` directory: ```~/.zshrc``` or ```~/.bashrc```, an add this lines at start of file: (if this file not exist, you can create) (PATH_JDK_VERSION is the jdk instalation folder noted. and PATH_SDK_VERSION is the sdk instalation folder noted.)

```bash
export JAVA_HOME=PATH_JDK_VERSION
export ANDROID_HOME=~PATH_SDK_VERSION
export PATH=$PATH:$ANDROID_HOME/emulator
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/tools/bin
export PATH=$PATH:$ANDROID_HOME/platform-tools
```

- Go to /android folder
    - Create ```local.properties``` file with:
    ```
    sdk.dir = /home/$USER/PATH_SDK_VERSION
    cmake.dir = /home/$USER/PATH_SDK_VERSION/cmake/3.18.1
    ```
- Install adb:
```
sudo apt install android-tools-adb android-tools-fastboot
```
- Configure Android Studio Emulator

- Android Gradle PLugin Version:
```4.2.2```
- Gradle Version:
```7.2```

## IOS

- Install cocoapods
```
sudo gem install cocoapods
```
---
# Development

- Install dependencies
```
npm install
```
- Startar Server
```
npm run start
```
## Android

- Build App with Gradle
```
npm run build:android-apk-debug
```
- Exec App AndroidStudio Emulator
```
npm run android # If the emulator not open when you run the code, open it with AndroidStudio and verify with (adb devices)
```
- Obs: See Mapped errors section to understand about patches
## IOS

- Build App for IOS
```
npm run build:ios
```
- Exec App with XCODE
```
npm run ios
```

# Production:

- Obs: Files removed to publish code with public repo
    - ```android/controllar-google-key.json```
    ```json
    "type": "service_account",
    "project_id": "xxxx",
    "private_key_id": "xxxx",
    "private_key": "-----BEGIN PRIVATE KEY-----\nxxxx\n-----END PRIVATE KEY-----\n",
    "client_email": "xxxx",
    "client_id": "xxxx",
    "auth_uri": "xxxx",
    "token_uri": "xxxx",
    "auth_provider_x509_cert_url": "xxxx",
    "client_x509_cert_url": "xxxx"
    ```

    - ```android/app/src/main/assets/appcenter-config.json```
    ```json
    {
        "app_secret": "AAAAAAAA-BBBB-CCCC-DDDD-FFFFFFFFFFFF"
    }
    ```

## Generate APK on Android for tests

- Build App
```
npm run build:android-apk-<debug|release>
```
- Find the .apk file in:
```
android/app/build/apk/debug/app-<debug|release>.apk
```

## Generate AAB on Android for upload on Play Console

- Build App
```
npm run build:android-aab-release
```
- Find the .aab file in:
```
android/app/build/bundle/release/app-release.aab
```
- Send to google play console

- Obs (Convert .aab into .apk)
```
java -jar bundletool-all-1.17.1.jar build-apks --bundle=/path/to/file.aab --output=/path/to/file.apks --connected-device && java -jar bundletool-all-1.17.1.jar install-apks --apks=/path/to/file.apks
```
## Deploy the APP with Code Push (Deprecated by Microsoft)

- Install appcenter-cli
```
npm install -g appcenter-cli
```
- Login on AppCenter
```
appcenter login
```
- Publish on Server
```
appcenter CodePush release-react -a {user|organization}/{app name} -d {environment} -t {target app version}
```
- environment:
    - `Staging` its mode for Rrelease builds
    - `Production` its mode for users

- Example on Android:

```
appcenter CodePush release-react -a Controllar/Android -d Production -t 1.2
```

- Example on IOS:

```
appcenter CodePush release-react -a Controllar/iOS -d Production -t 1.2
```

- Versions
    - Names:
        - First number: Release (Ex: Keypad edit release)
        - Second number: Release feature (Ex: Keypad luminos edit feature/ Keypad intel ids edit feature)
        - Third number: Test deployed to user (Ex: Keypad luminos edit test with double fast click)
    - Examples:
        - 13.0.0 - Old release
        - 13.0.1 - Keypad luminos edit, first test deploy
        - 13.0.2 - Keypad luminos edit, fixes
        - 13.0.2 - Keypad luminos edit, fixes
        - 13.1.0 - Keypad luminos edit feature
        - 13.1.1 - Keypad intel ids edit, first test deploy
        - 13.1.2 - Keypad luminos edit, fixes
        - 14.0.0 - Keypad edit release

# Mapped errors

- Packages with problems
    - Solution: Aply patches
        - Old patches (Before the version 13 of app on PlayStore)
            ```
            git apply patches/react-native-udp+4.1.6.patch
            ```
            ```
            git apply patches/react-native-wifi-reborn+4.7.0.patch
            ```
            ```
            git apply patches/react-native-reanimated+1.9.0.patch
            ```
        - New patches (After the version 13 we just need to fix de AndroidManifest wifi package from reborn)
            ```
            git apply patches/react-native-wifi-reborn+4.12.1
            ```
            - If patch don't work, edit AndroidManifest same this issue (https://github.com/JuanSeBestia/react-native-wifi-reborn/issues/343#issuecomment-2028129463)

- Duplicated res files
    - Solution: Remove res files duplicated on res folder before to build

- Package react-native-community/cli-platform-android with problem
    - Solution:
        ```
        npm i  @react-native-community/cli-platform-android
        ```
- Node version problem on build App (Transform File)
    - Solution: Use correct version of node `14.18.1`, bash terminal and use nvm `alias` cmd to set default node version
    - Stackoverflow issue: (https://stackoverflow.com/questions/69647332/cannot-read-properties-of-undefined-reading-transformfile-at-bundler-transfo)

- General problems:
    - Reinstall dependencies:
        - `rm -rf node_modules/`
        - `npm cache verify`
        - `npm cache clean --force`
        - `npm install`
    - Clean Gradle:
        - `npm run gradle-clean`
- Package react-native-gesture-handler queueMicrotask error
    - Solution:
        - Ugrage package and instal core as that issue: (https://github.com/software-mansion/react-native-reanimated/issues/4559#issuecomment-1730166106)

# Old README.md

## ./android - Android specific resources

- Build tool: `./gradlew`
- Custom permissions: `./app/src/main/AndroidManifest.xml`
- Custom assets (e.g. icon fonts): `./app/build.gradle`

## ./ios - iOS specific resources

- Build tool: `xcodebuild`
- Custom assets (e.g. icon fonts): `./app/Info.plist`, `./Podfile`

## Development

- npm start - start Metro server
- npm run ios - run iOS simulator
- npm run android - run Android emulator
