package com.controllar.app;

import com.facebook.react.ReactActivity;
import com.facebook.react.modules.core.DeviceEventManagerModule;
import com.facebook.react.bridge.ReactContext;
import com.facebook.react.ReactInstanceManager;
import android.content.Intent;
import android.os.Bundle;

public class MainActivity extends ReactActivity {

  /**
   * Returns the name of the main component registered from JavaScript. This is used to schedule
   * rendering of the component.
   */
  @Override
  protected String getMainComponentName() {
    return "ControllarApp";
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      handleIntent(getIntent());  // Adicionado aqui também
  }

  @Override
  public void onNewIntent(Intent intent) {
      super.onNewIntent(intent);
      handleIntent(intent);
  }

  private void handleIntent(Intent intent) {
      if ("com.controllar.app.ACTION_WIDGET_01_CLICKED".equals(intent.getAction())) {
          MainApplication.setStartedFromWidget(1);
      }
      if ("com.controllar.app.ACTION_WIDGET_02_CLICKED".equals(intent.getAction())) {
          MainApplication.setStartedFromWidget(2);
      }
      if ("com.controllar.app.ACTION_WIDGET_03_CLICKED".equals(intent.getAction())) {
          MainApplication.setStartedFromWidget(3);
      }
      if ("com.controllar.app.ACTION_WIDGET_04_CLICKED".equals(intent.getAction())) {
          MainApplication.setStartedFromWidget(4);
      }
      if (intent.getAction().equals("com.controllar.app.ACTION_WIDGET_01_CLICKED")) {
          ReactInstanceManager reactInstanceManager = getReactNativeHost().getReactInstanceManager();
          ReactContext reactContext = reactInstanceManager.getCurrentReactContext();
          if (reactContext != null) {
              reactContext
                  .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                  .emit("widget01Clicked", null);
          }
      }
      if (intent.getAction().equals("com.controllar.app.ACTION_WIDGET_02_CLICKED")) {
          ReactInstanceManager reactInstanceManager = getReactNativeHost().getReactInstanceManager();
          ReactContext reactContext = reactInstanceManager.getCurrentReactContext();
          if (reactContext != null) {
              reactContext
                  .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                  .emit("widget02Clicked", null);
          }
      }
      if (intent.getAction().equals("com.controllar.app.ACTION_WIDGET_03_CLICKED")) {
          ReactInstanceManager reactInstanceManager = getReactNativeHost().getReactInstanceManager();
          ReactContext reactContext = reactInstanceManager.getCurrentReactContext();
          if (reactContext != null) {
              reactContext
                  .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                  .emit("widget03Clicked", null);
          }
      }
      if (intent.getAction().equals("com.controllar.app.ACTION_WIDGET_04_CLICKED")) {
          ReactInstanceManager reactInstanceManager = getReactNativeHost().getReactInstanceManager();
          ReactContext reactContext = reactInstanceManager.getCurrentReactContext();
          if (reactContext != null) {
              reactContext
                  .getJSModule(DeviceEventManagerModule.RCTDeviceEventEmitter.class)
                  .emit("widget04Clicked", null);
          }
      }
  }

}
