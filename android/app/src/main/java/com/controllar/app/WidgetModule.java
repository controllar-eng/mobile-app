// Arquivo: android/app/src/main/java/com/controllar/app/WidgetModule.java

package com.controllar.app;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;
import com.facebook.react.bridge.ReactMethod;
import com.facebook.react.bridge.Promise;

public class WidgetModule extends ReactContextBaseJavaModule {

    public WidgetModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return "WidgetModule";
    }

    @ReactMethod
    public void wasStartedFromWidget(Promise promise) {
        promise.resolve(MainApplication.wasStartedFromWidget());
    }
    
    @ReactMethod
    public void resetWidget() {
        MainApplication.resetWidget();
    }
}
