package com.controllar.app;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.widget.RemoteViews;
import android.content.SharedPreferences;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Intent;
import android.app.PendingIntent;

public class Widget01 extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        SharedPreferences sharedPref = context.getSharedPreferences("DATA", Context.MODE_PRIVATE);
        String appString1 = sharedPref.getString("appData1", "{\"text\":'Cena'}");
        String appString2 = sharedPref.getString("appData2", "{\"text\":'Imóvel'}");
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget01);
        try {
            JSONObject appData1 = new JSONObject(appString1);
            views.setTextViewText(R.id.appwidget_text, appData1.getString("text"));
            JSONObject appData2 = new JSONObject(appString2);
            views.setTextViewText(R.id.appwidget_text2, appData2.getString("text"));
            JSONObject widgetData = new JSONObject("{\"text\":''}");
            views.setTextViewText(R.id.widget_01_label, widgetData.getString("text"));
        }catch (JSONException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(context, MainActivity.class);
        intent.setAction("com.controllar.app.ACTION_WIDGET_01_CLICKED");
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.relative, pendingIntent);
        appWidgetManager.updateAppWidget(appWidgetId, views);

    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
    }

    @Override
    public void onDisabled(Context context) {
    }
}