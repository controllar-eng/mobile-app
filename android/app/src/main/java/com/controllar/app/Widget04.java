package com.controllar.app;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.widget.RemoteViews;
import android.content.SharedPreferences;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Intent;
import android.app.PendingIntent;

public class Widget04 extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        SharedPreferences sharedPref = context.getSharedPreferences("DATA", Context.MODE_PRIVATE);
        String appString7 = sharedPref.getString("appData7", "{\"text\":'Cena'}");
        String appString8 = sharedPref.getString("appData8", "{\"text\":'Imóvel'}");
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget04);
        try {
            JSONObject appData7 = new JSONObject(appString7);
            views.setTextViewText(R.id.appwidget_text, appData7.getString("text"));
            JSONObject appData8 = new JSONObject(appString8);
            views.setTextViewText(R.id.appwidget_text2, appData8.getString("text"));
            JSONObject widgetData = new JSONObject("{\"text\":''}");
            views.setTextViewText(R.id.widget_04_label, widgetData.getString("text"));
        }catch (JSONException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(context, MainActivity.class);
        intent.setAction("com.controllar.app.ACTION_WIDGET_04_CLICKED");
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.relative, pendingIntent);
        appWidgetManager.updateAppWidget(appWidgetId, views);

    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
    }

    @Override
    public void onDisabled(Context context) {
    }
}