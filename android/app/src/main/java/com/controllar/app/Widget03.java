package com.controllar.app;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.widget.RemoteViews;
import android.content.SharedPreferences;
import org.json.JSONException;
import org.json.JSONObject;
import android.content.Intent;
import android.app.PendingIntent;

public class Widget03 extends AppWidgetProvider {

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {
        SharedPreferences sharedPref = context.getSharedPreferences("DATA", Context.MODE_PRIVATE);
        String appString5 = sharedPref.getString("appData5", "{\"text\":'Cena'}");
        String appString6 = sharedPref.getString("appData6", "{\"text\":'Imóvel'}");
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget03);
        try {
            JSONObject appData5 = new JSONObject(appString5);
            views.setTextViewText(R.id.appwidget_text, appData5.getString("text"));
            JSONObject appData6 = new JSONObject(appString6);
            views.setTextViewText(R.id.appwidget_text2, appData6.getString("text"));
            JSONObject widgetData = new JSONObject("{\"text\":''}");
            views.setTextViewText(R.id.widget_03_label, widgetData.getString("text"));
        }catch (JSONException e) {
            e.printStackTrace();
        }
        Intent intent = new Intent(context, MainActivity.class);
        intent.setAction("com.controllar.app.ACTION_WIDGET_03_CLICKED");
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_IMMUTABLE | PendingIntent.FLAG_UPDATE_CURRENT);
        views.setOnClickPendingIntent(R.id.relative, pendingIntent);
        appWidgetManager.updateAppWidget(appWidgetId, views);

    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        for (int appWidgetId : appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
    }

    @Override
    public void onDisabled(Context context) {
    }
}