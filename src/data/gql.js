import { gql } from '@apollo/client';

// Enum
export const TIMER_TYPE = {
  ONOFF_WEEKDAY: 'ONOFF_WEEKDAY',
  ONOFF_DATE: 'ONOFF_DATE',
  COUNTDOWN: 'COUNTDOWN',
};
export const DEVICE_TYPE = {
  DIMMER: 'DIMMER',
  SWITCH: 'SWITCH',
  PULSE: 'PULSE',
  SENSOR: 'SENSOR',
  MULTIMEDIA: 'MULTIMEDIA',
};

export const TIMER_FRAGMENT = gql`
  fragment TimerInfo on Timer {
    id
    name
    active
    offOnly
    startTime
    endTime
    type
    startOn
  }
`;

export const SCENE_FRAGMENT = gql`
  fragment SceneInfo on Scene {
    id
    name
    icon
    offOnly
    actions {
      id
      code
      intensity
      storeState
      sensor
    }
  }
`;

export const SCENE_FRAGMENT_WITHOUT_STORE_STATE = gql`
  fragment SceneInfo on Scene {
    id
    name
    icon
    offOnly
    actions {
      id
      code
      intensity
    }
  }
`;

export const CREATE_MULTIMEDIA_DEVICE = gql`
  mutation ($input: MultimediaDeviceCreateInput!) {
    createMultimediaDevice(input: $input) {
      device {
        id
        name
        group_id
        on
        intensity
        type
        indicator
      }
      error {
        name
      }
    }
  }
`;

export const UPDATE_MULTIMEDIA_DEVICE = gql`
  mutation ($device: MultimediaDeviceCreateInput) {
    updateMultimediaDevice(device: $device)
  }
`;

export const SAVE_PATH = gql`
  mutation (
    $bigger: Int
    $buttonName: String
    $multimedia_device_name: String
    $emitter: String
    $signal: String
    $channel: Int
    $freq: Int
    $copy: String
    $type: String
    $macAddress: String
  ) {
    savePath(
      bigger: $bigger
      buttonName: $buttonName
      multimedia_device_name: $multimedia_device_name
      emitter: $emitter
      signal: $signal
      channel: $channel
      freq: $freq
      copy: $copy
      type: $type
      macAddress: $macAddress
    )
  }
`;

export const CANCEL_RECORD = gql`
  mutation ($param: String) {
    cancelRecord(param: $param)
  }
`;

export const RESET_MQTT_SERVICES = gql`
  mutation ($param: String) {
    resetMqttServices(param: $param)
  }
`;

export const DELETE_MULTIMEDIA_DEVICE = gql`
  mutation ($id: Int) {
    deleteMultimediaDevice(id: $id)
  }
`;

export const LIST_BROADLINKS_CONNECTED = gql`
  mutation ($param: String) {
    listBroadlinksConnected(param: $param)
  }
`;

export const DELETE_NOTIFICATIONS = gql`
  mutation ($param: String) {
    deleteNotifications(param: $param)
  }
`;

export const LIST_NOTIFICATIONS = gql`
  {
    listNotifications {
      mac
      date
    }
  }
`;

export const BROADLINK_ERROR = gql`
  subscription {
    broadlinkError {
      error
    }
  }
`;

export const BROADLINKS_CONNECTEDS = gql`
  subscription {
    broadlinksConnecteds
  }
`;

export const LIST_BUTTONS = gql`
  {
    listButtons {
      id
      multimedia_device_id
      buttonName
      emitter
      signal
      cmdFmt
      commandProtocol
      commandFunction
      commandDevice
      commandSubdevice
      commandFile
      channel
      freq
      macAddress
    }
  }
`;

export const LIST_IRDB_MODELS = gql`
  {
    listIrdbModels
  }
`;

export const LIST_IRDB_DEVICES = gql`
  mutation ($model: String) {
    listIrdbDevices(model: $model)
  }
`;

export const LIST_IRDB_CSVS = gql`
  mutation ($model: String, $device: String) {
    listIrdbCsvs(model: $model, device: $device)
  }
`;

export const UPDATE_YAML_FILE = gql`
  mutation ($buttons: [MultimediaDeviceButtonCreateInput]) {
    updateYamlFile(buttons: $buttons)
  }
`;

export const LIST_IRDB_BUTTONS = gql`
  mutation (
    $model: String
    $device: String
    $csv: String
    $emitter: String
    $channel: Int
    $freq: Int
    $macAddress: String
  ) {
    listIrdbButtons(
      model: $model
      device: $device
      csv: $csv
      emitter: $emitter
      channel: $channel
      freq: $freq
      macAddress: $macAddress
    ) {
      id
      functionname
      protocol
      device
      subdevice
      _function
    }
  }
`;

export const BROADLINKS = gql`
  {
    broadlinks {
      id
      name
      mac
    }
  }
`;

export const CREATE_BROADLINK = gql`
  mutation ($id: Int, $name: String!, $mac: String!) {
    createBroadlink(id: $id, name: $name, mac: $mac) {
      id
      name
      mac
    }
  }
`;

export const CREATE_TIMER = gql`
  mutation ($input: TimerCreateInput!) {
    createTimer(input: $input) {
      timer {
        ...TimerInfo
      }
      error {
        name
      }
    }
  }
  ${TIMER_FRAGMENT}
`;
export const UPDATE_TIMER = gql`
  mutation ($input: TimerUpdateInput!) {
    updateTimer(input: $input) {
      timer {
        ...TimerInfo
      }
      error {
        name
      }
    }
  }
  ${TIMER_FRAGMENT}
`;

export const DELETE_TIMER = gql`
  mutation ($id: Int!) {
    deleteTimer(id: $id) {
      success
    }
  }
`;

export const CREATE_SCENE = gql`
  mutation ($input: SceneCreateInput!) {
    createScene(input: $input) {
      scene {
        ...SceneInfo
      }
      error {
        name
      }
    }
  }
  ${SCENE_FRAGMENT_WITHOUT_STORE_STATE}
`;
export const UPDATE_SCENE = gql`
  mutation ($input: SceneUpdateInput!) {
    updateScene(input: $input) {
      scene {
        ...SceneInfo
      }
      error {
        name
      }
    }
  }
  ${SCENE_FRAGMENT_WITHOUT_STORE_STATE}
`;

export const DELETE_SCENE = gql`
  mutation ($id: Int!) {
    deleteScene(id: $id) {
      success
    }
  }
`;

export const CREATE_PROFILE = gql`
  mutation ($input: ProfileCreateInput!) {
    createProfile(input: $input) {
      profile {
        id
        name
      }
      error {
        name
      }
    }
  }
`;
export const UPDATE_PROFILE = gql`
  mutation ($input: ProfileUpdateInput!) {
    updateProfile(input: $input) {
      profile {
        id
        name
      }
      error {
        name
      }
    }
  }
`;

export const DELETE_PROFILE = gql`
  mutation ($id: Int!) {
    deleteProfile(id: $id) {
      success
    }
  }
`;

export const CREATE_DEVICE_GROUP = gql`
  mutation ($input: DeviceGroupCreateInput!) {
    createDeviceGroup(input: $input) {
      deviceGroup {
        id
        name
      }
      error {
        name
      }
    }
  }
`;
export const UPDATE_DEVICE_GROUP = gql`
  mutation ($input: DeviceGroupUpdateInput!) {
    updateDeviceGroup(input: $input) {
      deviceGroup {
        id
        name
      }
      error {
        name
      }
    }
  }
`;

export const DELETE_DEVICE_GROUP = gql`
  mutation ($id: Int!) {
    deleteDeviceGroup(id: $id) {
      success
    }
  }
`;
export const SET_DEVICE_GROUP = gql`
  mutation ($groupId: Int, $deviceIds: [Int!]!) {
    setDeviceGroup(groupId: $groupId, deviceIds: $deviceIds) {
      success
    }
  }
`;

export const ONOFF_TIMERS = gql`
  query ($profile_id: Int!) {
    timers(where: { profile_id: $profile_id, type: [ONOFF_WEEKDAY, ONOFF_DATE] }) {
      ...TimerInfo
    }
  }
  ${TIMER_FRAGMENT}
`;
export const COUNTDOWN_TIMERS = gql`
  query ($profile_id: Int!) {
    now
    timers(where: { profile_id: $profile_id, type: COUNTDOWN }) {
      ...TimerInfo
    }
  }
  ${TIMER_FRAGMENT}
`;

export const SCENES = gql`
  query ($profile_id: Int!) {
    scenes(where: { profile_id: $profile_id }) {
      ...SceneInfo
    }
  }
  ${SCENE_FRAGMENT}
`;

export const SCENES_WITHOUT_STORE_STATE = gql`
  query ($profile_id: Int!) {
    scenes(where: { profile_id: $profile_id }) {
      ...SceneInfo
    }
  }
  ${SCENE_FRAGMENT_WITHOUT_STORE_STATE}
`;

export const TIMER_ACTIONS = gql`
  query ($timerId: Int!) {
    timer(id: $timerId) {
      id
      actions {
        id
        code
        intensity
        storeState
        sensor
      }
    }
  }
`;

export const TIMER_ACTIONS_WITHOUT_STORE_STATE = gql`
  query ($timerId: Int!) {
    timer(id: $timerId) {
      id
      actions {
        id
        code
        intensity
      }
    }
  }
`;

export const PROFILES = gql`
  {
    profiles {
      id
      name
    }
  }
`;

export const DEVICE_GROUPS = gql`
  {
    deviceGroups {
      id
      name
    }
  }
`;

export const DEVICE_STATES = gql`
  {
    deviceStates {
      id
      name
      group_id
      on
      intensity
      type
      indicator
    }
  }
`;

export const SET_DEVICE_STATE = gql`
  mutation ($id: Int!, $intensity: Int!) {
    setDeviceState(id: $id, intensity: $intensity) {
      id
      on
      intensity
    }
  }
`;

export const UPDATE_DEVICE = gql`
  mutation ($id: Int!, $name: String!, $indicator: String!) {
    updateDevice(id: $id, name: $name, indicator: $indicator) {
      device {
        id
        name
        indicator
      }
      error {
        name
      }
    }
  }
`;

export const EXECUTE_SCENE = gql`
  mutation ($id: Int!, $type: ExecutionType!) {
    executeScene(id: $id, type: $type) {
      id
      on
      intensity
    }
  }
`;

export const DEVICE_STATE_CHANGED = gql`
  subscription {
    deviceStateChanged {
      id
      intensity
      on
    }
  }
`;

export const STATE_FRAGMENT = gql`
  fragment State on DeviceState {
    id
    on
    intensity
  }
`;

export const SERVER_TOKEN = gql`
  {
    serverToken {
      id
      token
    }
  }
`;
