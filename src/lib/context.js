import { createContext } from 'react';

export const AppContext = createContext({});

export const INSTANCE_LOCAL_NET = { id: 0, name: 'Rede Local' };
export const DEFAULT_PROFILE = { id: 0, name: 'Geral' };
