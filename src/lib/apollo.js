import { ApolloClient, InMemoryCache, split, HttpLink } from '@apollo/client';
import { WebSocketLink } from '@apollo/link-ws';
import { getMainDefinition } from '@apollo/client/utilities';
import { ApolloLink } from '@apollo/client/link/core/ApolloLink';
import { setContext } from '@apollo/client/link/context';
import { RetryLink } from '@apollo/client/link/retry';
import { onError } from '@apollo/client/link/error';
import { SubscriptionClient } from 'subscriptions-transport-ws';

const HOST = 'aplicacao.controllar.com:4000';

export function createSubscriptionClient({ id, host = HOST, token }) {
  const path = id ? `/${id}/graphql` : '/graphql';

  const hostWithPort = host.includes(':') ? host : `${host}:4000`;

  const fullHost = `ws://${hostWithPort}${path}`;

  console.log('creating sub client', fullHost);

  return new SubscriptionClient(fullHost, {
    reconnect: true,
    lazy: true,
    connectionParams: () => {
      return token ? { authorization: `Bearer ${token}` } : {};
    },
  });
}

export function createApolloClient(instance, setShowPrompt) {
  const { id, host = HOST, token } = instance;

  const authLink = setContext((_, { headers }) => ({
    headers: { ...headers, authorization: token ? `Bearer ${token}` : '' },
  }));

  const hostWithPort = host.includes(':') ? host : `${host}:4000`;

  const path = id ? `/${id}/graphql` : '/graphql';
  const uri = `http://${hostWithPort}${path}`;
  const httpLink = new HttpLink({ uri });

  console.log('creating apollo client', uri);

  /* const ws = createSubscriptionClient(instance)
  const wsLink = new WebSocketLink(ws)

  const splitLink = split(
    ({ query }) => {
      const definition = getMainDefinition(query)
      return (
        definition.kind === "OperationDefinition" &&
        definition.operation === "subscription"
      )
    },
    wsLink,
    httpLink
  )*/

  const retryLink = new RetryLink({
    attempts: (count, operation, error) => {
      const isMutation =
        operation &&
        operation.query &&
        operation.query.definitions &&
        Array.isArray(operation.query.definitions) &&
        operation.query.definitions.some(def => def.kind === 'OperationDefinition' && def.operation === 'mutation');
      if (isMutation) {
        return !!error && count < 25;
      }
      return !!error && count < 6;
    },
  });

  const errorLink = onError(({ graphQLErrors, networkError }) => {
    if (graphQLErrors)
      graphQLErrors.map(({ message, locations, path }) => {
        console.log(`[GraphQL error]: Message: ${message}, Location: ${locations}, Path: ${path}`);
      });
    if (networkError) {
      console.log('[Network error]', uri, networkError);
    }
  });

  return new ApolloClient({
    cache: new InMemoryCache(),
    link: ApolloLink.from([errorLink, retryLink, authLink, httpLink]),
    assumeImmutableResults: true,
    queryDeduplication: true,
  });
}
