const abs = Math.abs;

export const pad = num => `0${num}`.slice(-2);

export const subtract = (aStr, bStr) => {
  const [h1, m1, s1] = aStr ? aStr.split(':') : [0, 0, 0];
  const [h2, m2, s2] = bStr ? bStr.split(':') : [0, 0, 0];
  return `${pad(abs(h1 - h2))}:${pad(abs(m1 - m2))}:${pad(abs(s1 - s2))}`;
};

export const add = (aStr, bStr) => {
  const [h1, m1, s1] = aStr.split(':');
  const [h2, m2, s2] = bStr.split(':');
  return `${pad(+h1 + +h2)}:${pad(+m1 + +m2)}:${pad(+s1 + +s2)}`;
};

export const duration = str =>
  (str.replace(':', 'h').replace(':', 'min') + 's').replace(/00\D+/g, '').replace(/0(\d)/g, '$1');

/* Hermes doesn't support Intl (used by localeCompare) yet
   https://github.com/facebook/hermes/issues/23
*/
/* arr.sort((a, b) =>
    a[attr].localeCompare(b[attr], undefined, {
      numeric: true,
      sensitivity: "base"
    })
  )*/
/* ********************************************************************
 * Alphanum sort() function version - case insensitive
 *  - Slower, but easier to modify for arrays of objects which contain
 *    string properties
 *
 * http://www.davekoelle.com/alphanum.html
 */
export const sort = (arr, attr) =>
  [...arr].sort((a, b) => {
    function chunkify(t) {
      var tz = new Array();
      var x = 0,
        y = -1,
        n = 0,
        i,
        j;

      while ((i = (j = t.charAt(x++)).charCodeAt(0))) {
        var m = i == 46 || (i >= 48 && i <= 57);
        if (m !== n) {
          tz[++y] = '';
          n = m;
        }
        tz[y] += j;
      }
      return tz;
    }

    var aa = chunkify(a[attr].toLowerCase());
    var bb = chunkify(b[attr].toLowerCase());

    for (let x = 0; aa[x] && bb[x]; x++) {
      if (aa[x] !== bb[x]) {
        var c = Number(aa[x]),
          d = Number(bb[x]);
        if (c == aa[x] && d == bb[x]) {
          return c - d;
        } else return aa[x] > bb[x] ? 1 : -1;
      }
    }
    return aa.length - bb.length;
  });

export function debounce(func, wait) {
  var timeout;
  return function () {
    var context = this,
      args = arguments;
    var later = function () {
      timeout = null;
      func.apply(context, args);
    };
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
  };
}

export const arrayToHash = (array, id = 'id') => array.reduce((obj, item) => ((obj[item[id]] = item), obj), {});

export const theme = {
  colors: {
    black: '#000000',
    gray850: '#303030',
    gray800: '#333333',
    gray700: '#666666',
    gray600: '#939393',
    gray500: '#BBBBBB',
    gray400: '#cccccc',
    gray200: '#e7e7e7',
    gray100: '#F5F5F5',
    white: '#ffffff',
    blue500: '#2098F3',
    red: '#ff0000',
  },
};
