import Zeroconf from 'react-native-zeroconf';

const NAME = 'picontrollar';
const TIMEOUT = 3000;

export function findServerAddress() {
  const zeroconf = new Zeroconf();

  const promise = new Promise(resolve => {
    zeroconf.scan(NAME);

    let address;

    zeroconf.on('start', () => {
      console.log('Zeroconf [Start]');
    });
    zeroconf.on('stop', () => {
      console.log('Zeroconf [Stop]');
      zeroconf.removeDeviceListeners();
      resolve(address);
    });
    zeroconf.on('error', err => {
      console.log('Zeroconf [Error]', err);
      zeroconf.stop();
    });

    zeroconf.on('resolved', ({ addresses }) => {
      address = addresses[0];
      console.log('Zeroconf [Resolved] ', address);
      zeroconf.stop();
    });
  });

  return [
    promise,
    () => {
      zeroconf.removeDeviceListeners();
      zeroconf.stop();
    },
  ];
}
