import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { ActivityIndicator, Colors, IconButton, Paragraph, TextInput, Title } from 'react-native-paper';
import InstancePicker from '../components/instance/InstancePicker';
import { AppContext, INSTANCE_LOCAL_NET } from '../lib/context';
import { findServerAddress } from '../lib/serverDiscovery';

export default function LoadingScreen({}) {
  const { setShowPrompt, instance, createClient, setOldInstance } = useContext(AppContext);

  const [address, setAddress] = useState();

  const local = !instance.id;

  useEffect(() => {
    setAddress(instance.host);
  }, [instance.host]);

  useEffect(() => {
    let timeout, stop;

    const tick = () => {
      if (stop) stop();

      if (local) {
        const [newAddress, stopFn] = findServerAddress();
        stop = () => {
          stopFn();
          stop = null;
        };

        newAddress.then(newHost => {
          if (newHost) {
            createClient({ ...INSTANCE_LOCAL_NET, host: newHost });
            setOldInstance({ ...INSTANCE_LOCAL_NET, host: newHost });
          }
        });
      } else {
        createClient(instance);
        setOldInstance(instance);
      }

      timeout = setTimeout(tick, 2000);
    };

    tick();

    return () => {
      if (stop) stop();
      clearTimeout(timeout);
    };
  }, [local]);

  const handleSubmit = () => {
    setShowPrompt(false);
    createClient({ ...INSTANCE_LOCAL_NET, host: address });
  };

  return (
    <View style={{ paddingTop: 4, paddingHorizontal: 20 }}>
      <InstancePicker />
      <View>
        <View style={styles.title}>
          <Title>{local ? 'Buscando na rede local...' : 'Tentando conexão remota...'}</Title>
          <ActivityIndicator size='small' />
        </View>
        <View>
          <Paragraph style={styles.spacer}>O servidor ainda não foi encontrado.</Paragraph>
          <Paragraph style={styles.spacer}>
            Você pode escolher uma instalação memorizada acima ou preencher um endereço manualmente abaixo:
          </Paragraph>
          <View style={[styles.address, styles.spacer]}>
            <TextInput
              placeholder='Endereço'
              dense
              value={address}
              onChangeText={setAddress}
              style={styles.input}
              onSubmitEditing={handleSubmit}
            />
            <IconButton icon='check' color={Colors.blue500} disabled={!address} onPress={handleSubmit} />
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  title: { flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 10 },
  address: { flexDirection: 'row', alignItems: 'center' },
  input: { flex: 1 },
  spacer: { marginTop: 8 },
});
