import React, { useState, useContext, useEffect } from 'react';
import { View, StyleSheet, Alert } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { format, parse, parseISO } from 'date-fns';
import { Button, Subheading, useTheme } from 'react-native-paper';
import { useMutation } from '@apollo/client';
import DateTimePicker from '../components/timer/DateTimePicker';
import { TimePicker } from '../components/timer/TimePicker';
import { CREATE_TIMER, UPDATE_TIMER, ONOFF_TIMERS, TIMER_TYPE } from '../data/gql';
import LabeledCheck from '../components/LabeledCheck';
import { AppContext } from '../lib/context';

const WEEKDAYS = ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'];

const FMT = { TIME: 'HH:mm', DATE: 'yyyy-MM-dd' };

export default function OnOffTimerScreen({ navigation, route }) {
  const { profile, createClient } = useContext(AppContext);

  const theme = useTheme();

  const [createTimer] = useMutation(CREATE_TIMER, {
    update(cache, { data: { createTimer } }) {
      if (!createTimer.timer) return;
      try {
        const { timers } = cache.readQuery({
          query: ONOFF_TIMERS,
          // eslint-disable-next-line camelcase
          variables: { profile_id: profile.id },
        });
        cache.writeQuery({
          query: ONOFF_TIMERS,
          data: { timers: [...timers, createTimer.timer] },
          // eslint-disable-next-line camelcase
          variables: { profile_id: profile.id },
        });
      } catch (e) {
        console.log('ROOT_QUERY.timers not on cache yet', e);
      }
    },
  });
  const [updateTimer] = useMutation(UPDATE_TIMER);

  const timer = route.params?.timer || {};

  const [startTime, setStartTime] = useState(
    timer.startTime ? parse(timer.startTime, FMT.TIME, new Date()) : new Date(),
  );
  const [endTime, setEndTime] = useState(timer.endTime ? parse(timer.endTime, FMT.TIME, new Date()) : new Date());

  const [startOn, setStartOn] = useState(
    timer.startOn && timer.type == TIMER_TYPE.ONOFF_DATE ? parseISO(timer.startOn) : new Date(),
  );

  const weekdays = WEEKDAYS.map(_ => false);
  if (timer.type == TIMER_TYPE.ONOFF_WEEKDAY) Array.from(timer.startOn).forEach(ch => (weekdays[ch] = true));

  const [selectedDays, setSelectedDays] = useState(weekdays);

  // Toggle controls
  const [hasEndTime, setHasEndTime] = useState(!!timer.endTime);
  const [dateIsWeekday, setDateIsWeekday] = useState(timer.type == TIMER_TYPE.ONOFF_WEEKDAY);

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Button
          onPress={async () => {
            const newTimer = {
              // eslint-disable-next-line camelcase
              profile_id: profile.id,
              name: timer.name,
              startTime: format(startTime, FMT.TIME),
              endTime: hasEndTime ? format(endTime, FMT.TIME) : null,
              type: dateIsWeekday ? TIMER_TYPE.ONOFF_WEEKDAY : TIMER_TYPE.ONOFF_DATE,
              startOn: dateIsWeekday
                ? selectedDays
                    .map((_, idx) => idx)
                    .filter((_, idx) => selectedDays[idx])
                    .join('')
                : format(startOn, FMT.DATE),
              actions: timer.actions,
              offOnly: timer.offOnly,
            };

            try {
              if (timer.id) {
                const { data } = await updateTimer({
                  variables: { input: { ...newTimer, id: timer.id } },
                });

                if (data.updateTimer.error) Alert.alert(data.updateTimer.error.name);
                else {
                  navigation.popToTop();
                  createClient(); // TODO: Use cache after it
                }
              } else {
                const { data } = await createTimer({
                  variables: { input: { active: true, ...newTimer } },
                });

                if (data.createTimer.error) Alert.alert(data.createTimer.error.name);
                else {
                  navigation.popToTop();
                  createClient(); // TODO: Use cache after it
                }
              }
            } catch (e) {
              console.log('error', e);
            }
          }}
        >
          <Icon size={26} name='checkmark' />
        </Button>
      ),
    });
  });

  return (
    <View style={styles.container}>
      <View style={styles.time}>
        <View style={styles.startTime}>
          <View style={styles.startTitle}>
            <Subheading>
              <Icon name='md-time-outline' size={18} /> Hora de Ativação
            </Subheading>
          </View>

          <TimePicker value={startTime} onChange={setStartTime} />
        </View>
        <View style={styles.separator} />
        <View style={styles.endTime}>
          <LabeledCheck
            style={styles.endCheck}
            label='Desativação'
            checked={hasEndTime}
            onPress={() => setHasEndTime(!hasEndTime)}
          />

          <TimePicker disabled={!hasEndTime} value={endTime} onChange={setEndTime} />
        </View>
      </View>
      <View>
        <LabeledCheck
          style={styles.repeatCheck}
          label='Repetir em dias da semana'
          checked={dateIsWeekday}
          onPress={() => setDateIsWeekday(!dateIsWeekday)}
        />
        {dateIsWeekday ? (
          <View style={styles.weekday}>
            {WEEKDAYS.map((weekday, idx) => (
              <Button
                key={`${weekday}${idx}`}
                mode='contained'
                compact
                onPress={() => {
                  setSelectedDays(
                    selectedDays.map((_, innerIdx) => (innerIdx === idx ? !selectedDays[idx] : selectedDays[innerIdx])),
                  );
                }}
                labelStyle={{
                  fontSize: 15,
                  color: selectedDays[idx] ? theme.colors.background : theme.colors.text,
                }}
                style={{
                  height: 38,
                  width: 38,
                  borderRadius: 38,
                  backgroundColor: selectedDays[idx] ? theme.colors.primary : theme.colors.background,
                }}
              >
                {weekday}
              </Button>
            ))}
          </View>
        ) : (
          <DateTimePicker
            mode='date'
            value={startOn}
            locale='pt-BR'
            is24Hour={true}
            onChange={(_, time) => {
              if (time) setStartOn(time);
            }}
          />
        )}
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  name: { marginBottom: 10 },
  container: { padding: 20 },
  time: { flexDirection: 'row' },
  endTime: { flex: 1 },
  startTime: { flex: 1 },
  startTitle: { height: 38, justifyContent: 'center' },
  separator: {
    borderLeftWidth: 1,
    borderLeftColor: 'lightgrey',
    margin: 10,
  },
  endCheck: { height: 38 },
  repeatCheck: { marginTop: 10, marginBottom: 10 },
  weekday: {
    marginTop: 10,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});
