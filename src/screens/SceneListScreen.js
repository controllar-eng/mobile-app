import { useMutation, useQuery } from '@apollo/client';
import React, { useContext, useEffect, useState } from 'react';
import { FlatList, RefreshControl, StyleSheet, View } from 'react-native';
import { Button, Colors, IconButton, List, Subheading } from 'react-native-paper';
import SceneContextMenu from '../components/scene/SceneContextMenu';
import SceneIcon from '../components/scene/SceneIcon';
import { DEVICE_STATES, EXECUTE_SCENE, SCENES, SCENES_WITHOUT_STORE_STATE } from '../data/gql';
import { AppContext } from '../lib/context';
import { arrayToHash, sort } from '../lib/utils';

export default function SceneListScreen() {
  const { createClient, profile } = useContext(AppContext);

  const { loading, error, data } = useQuery(SCENES, {
    // eslint-disable-next-line camelcase
    variables: { profile_id: profile.id },
  });

  const result = useQuery(SCENES_WITHOUT_STORE_STATE, {
    // eslint-disable-next-line camelcase
    variables: { profile_id: profile.id },
  });

  const [executeScene] = useMutation(EXECUTE_SCENE);

  const { data: deviceData } = useQuery(DEVICE_STATES);

  const [states, setStates] = useState();
  useEffect(() => {
    setStates(deviceData ? arrayToHash(deviceData.deviceStates) : {});
  }, [deviceData]);

  const listEmptyComp = data && <Subheading style={styles.empty}>Nenhuma Cena criada no perfil atual.</Subheading>;

  return (
    <FlatList
      refreshControl={<RefreshControl refreshing={loading && result.loading} onRefresh={createClient} />}
      extraData={states}
      ListEmptyComponent={listEmptyComp}
      ListFooterComponent={<View />}
      ListFooterComponentStyle={styles.footer}
      keyExtractor={item => item.id.toString()}
      data={(data || result?.data) && deviceData && states && sort(data?.scenes || result.data.scenes, 'name')}
      renderItem={({ item }) => {
        const actions = Object.assign([], item.actions).sort((a, b) => a.id - b.id);
        const scene = { ...item, actions };
        const sensors = scene.actions.filter(a => a.sensor);
        const actionsCopy = Object.assign([], scene.actions).sort((a, b) => a.id - b.id);
        sensors.map(sensorDevice => {
          if (sensorDevice) {
            const newDevice = deviceData?.deviceStates.find(d => d.id === sensorDevice.sensor);
            const newAction = {
              id: 3000,
              code: newDevice.id,
              intensity: newDevice.intensity,
              storeState: null,
              sensor: 0,
            };
            actionsCopy.push(newAction);
          }
        });
        const sceneActive =
          actionsCopy
            .filter(action => action.code < 1000)
            .filter(action => deviceData?.deviceStates.find(d => d.id === action.code)?.type !== 'PULSE')
            .filter(action => {
              const state = states[action.code];
              return action.intensity === (state?.on ? state.intensity : 0);
            }).length ===
          actionsCopy
            .filter(action => action.code < 1000)
            .filter(action => deviceData?.deviceStates.find(d => d.id === action.code)?.type !== 'PULSE').length;

        const offActive =
          actionsCopy
            .filter(action => action.code < 1000)
            .filter(action => deviceData?.deviceStates.find(d => d.id === action.code)?.type !== 'PULSE')
            .filter(action => {
              const state = states[action.code];
              return state?.on;
            }).length > 0;

        const isPulse =
          actionsCopy
            .filter(action => action.code < 1000)
            .filter(action => deviceData?.deviceStates.find(d => d.id === action.code)?.type === 'PULSE').length ===
            1 && actionsCopy.filter(action => action.code < 1000).length === 1;
        const toggleOff = () =>
          executeScene({
            variables: {
              id: scene.id,
              type: offActive ? 'OFF' : 'FIRST',
            },
          });

        const toggleScene = () =>
          executeScene({
            variables: {
              id: scene.id,
              type: sceneActive ? 'OFF' : 'ON',
            },
          });

        return (
          <SceneContextMenu
            key={scene.id}
            editing={scene}
            query={SCENES}
            anchor={
              <List.Item
                onPress={scene.offOnly ? toggleOff : toggleScene}
                left={({ color }) => <SceneIcon name={scene.icon} size={32} color={color} style={styles.sceneIcon} />}
                title={<Subheading>{scene.name}</Subheading>}
                right={props => (
                  <View style={[isPulse ? { display: 'none' } : styles.rightView]}>
                    {!scene.offOnly && (
                      <IconButton
                        style={styles.iconButton}
                        color={sceneActive ? Colors.yellowA700 : Colors.grey400}
                        icon={sceneActive ? 'lightbulb-on' : 'lightbulb-outline'}
                        onPress={toggleScene}
                      />
                    )}
                    <Button
                      compact
                      color={offActive ? Colors.yellowA700 : Colors.grey400}
                      icon={offActive ? 'lightbulb-on' : 'lightbulb-outline'}
                      onPress={toggleOff}
                    >
                      OFF
                    </Button>
                  </View>
                )}
              />
            }
          />
        );
      }}
      windowSize={10}
    />
  );
}

const styles = StyleSheet.create({
  sceneIcon: { marginRight: 8 },
  iconButton: { marginTop: 0, marginBottom: 0 },
  rightView: { flexDirection: 'row', alignItems: 'center' },
  footer: { marginBottom: 80 },
  empty: { padding: 16 },
});
