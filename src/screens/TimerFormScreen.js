import { useQuery } from '@apollo/client';
import React, { useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { HelperText, TextInput } from 'react-native-paper';
import Actions from '../components/Actions';
import { TIMER_ACTIONS, TIMER_ACTIONS_WITHOUT_STORE_STATE, TIMER_TYPE } from '../data/gql';

export function getTimerRoute(timer) {
  return timer.type === TIMER_TYPE.COUNTDOWN ? 'CountdownTimer' : 'OnOffTimer';
}

export default function TimerFormScreen({ navigation, route }) {
  const timer = route.params?.timer || {};

  const [name, setName] = useState(timer.name || '');
  const [actions, setActions] = useState(timer.actions);
  const [sensor, setVerificationLumino] = useState(false);

  const [error, setError] = useState();

  // TODO: Pegar o timer com e sem storeState para passar para o actions
  const { data } = useQuery(TIMER_ACTIONS, {
    variables: { timerId: timer.id },
    fetchPolicy: 'cache-and-network',
    skip: !timer.id,
  });

  const result = useQuery(TIMER_ACTIONS_WITHOUT_STORE_STATE, {
    variables: { timerId: timer.id },
    fetchPolicy: 'cache-and-network',
    skip: !timer.id,
  });

  useEffect(() => {
    if (data || result.data) setActions(data?.timer?.actions || result.data?.timer?.actions || []);
  }, [data, result?.data]);

  useEffect(() => {
    navigation.setOptions({
      headerTitle: sensor ? 'Lumino de verificação de status' : timer.id ? 'Editar Timer' : 'Novo Timer',
    });
  }, []);

  return (
    <View style={styles.container}>
      <View style={!sensor ? styles.nameView : { display: 'none' }}>
        <TextInput label='Nome' value={name} onChangeText={setName} error={error && !!error.name} />
        <HelperText type='error' visible={error && !!error.name}>
          {error && error.name}
        </HelperText>
      </View>
      <Actions
        icon='md-chevron-forward'
        actions={actions}
        initialOffOnly={timer.offOnly}
        navigation={navigation}
        sensor={sensor}
        setVerificationLumino={setVerificationLumino}
        updateFn={async (actions, offOnly) => {
          actions = Object.assign([], actions).map(action => {
            if (action.code > 3000) action.code = 3000;
            return action;
          });
          const newTimer = { ...timer, name, actions, offOnly };

          // TODO validate name

          navigation.navigate(getTimerRoute(timer), { timer: newTimer });
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: { flex: 1 },
  nameView: { padding: 10 },
});
