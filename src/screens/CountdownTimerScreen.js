import React, { useState, useContext, useEffect } from 'react';
import { View, StyleSheet, Alert, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { Subheading, Text } from 'react-native-paper';
import { useMutation } from '@apollo/client';
import { CREATE_TIMER, UPDATE_TIMER, COUNTDOWN_TIMERS, TIMER_TYPE } from '../data/gql';
import LabeledCheck from '../components/LabeledCheck';
import { TimePickerWithSecs } from '../components/timer/TimePicker';
import { subtract, add, theme } from '../lib/utils';
import { AppContext } from '../lib/context';

const ZERO = '00:00:00';
const isIOS = Platform.OS === 'ios';

export default function CountdownTimerScreen({ navigation, route }) {
  const { profile, createClient } = useContext(AppContext);

  const [createTimer] = useMutation(CREATE_TIMER, {
    update(cache, { data: { createTimer } }) {
      if (!createTimer.timer) return;
      try {
        const { timers, now } = cache.readQuery({
          query: COUNTDOWN_TIMERS,
          // eslint-disable-next-line camelcase
          variables: { profile_id: profile.id },
        });
        cache.writeQuery({
          query: COUNTDOWN_TIMERS,
          data: { now, timers: [...timers, createTimer.timer] },
          // eslint-disable-next-line camelcase
          variables: { profile_id: profile.id },
        });
      } catch (e) {
        console.log('ROOT_QUERY.timers not on cache yet', e);
      }
    },
  });
  const [updateTimer] = useMutation(UPDATE_TIMER);

  const timer = (route.params && route.params.timer) || {};

  // Timer fields
  const [startTime, setStartTime] = useState(timer.startTime || ZERO);
  const [duration, setDuration] = useState(subtract(timer.endTime, startTime) || ZERO);

  // Toggle
  const [hasDelay, setHasDelay] = useState(startTime !== ZERO);

  return (
    <>
      <View style={styles.container}>
        <View style={styles.durationView}>
          <View style={styles.durationLabel}>
            <Icon name='md-time-outline' size={22} style={styles.timeIcon} />
            <Subheading>Duração</Subheading>
          </View>

          <TimePickerWithSecs value={duration} onChange={setDuration} />
        </View>

        <View style={styles.delayView}>
          <LabeledCheck label='Atrasar início em...' checked={hasDelay} onPress={() => setHasDelay(!hasDelay)} />

          <TimePickerWithSecs disabled={!hasDelay} value={startTime} onChange={setStartTime} />
        </View>
      </View>
      <View style={{ marginVertical: isIOS ? 35 : 10 }}>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <TouchableOpacity
            onPress={async () => {
              const newStart = hasDelay ? startTime : ZERO;
              const newTimer = {
                // eslint-disable-next-line camelcase
                profile_id: profile.id,
                name: timer.name,
                startTime: newStart,
                endTime: add(newStart, duration),
                type: TIMER_TYPE.COUNTDOWN,
                actions: timer.actions,
                offOnly: timer.offOnly,
              };

              try {
                if (timer.id) {
                  const { data } = await updateTimer({
                    variables: { input: { ...newTimer, id: timer.id } },
                  });

                  if (data.updateTimer.error) Alert.alert(data.updateTimer.error.name);
                  else {
                    navigation.popToTop();
                    createClient(); // TODO: Use cache after it
                  }
                } else {
                  const { data } = await createTimer({
                    variables: { input: { active: true, ...newTimer } },
                  });

                  if (data.createTimer.error) Alert.alert(data.createTimer.error.name);
                  else {
                    navigation.popToTop();
                    createClient(); // TODO: Use cache after it
                  }
                }
              } catch (e) {
                console.log('error', e);
              }
            }}
            style={styles.selectButton}
          >
            <Text style={styles.saveText}>Salvar</Text>
            <Icon name={'save-outline'} size={18} color='white' />
          </TouchableOpacity>
        </View>
      </View>
    </>
  );
}

const styles = StyleSheet.create({
  timeIcon: { padding: 8 },
  container: { padding: 20, flexGrow: 1 },
  durationView: { justifyContent: 'flex-start' },
  durationLabel: { flexDirection: 'row', alignItems: 'center' },
  delayView: { marginTop: 10, justifyContent: 'flex-start' },
  saveText: { color: 'white', fontSize: 13, marginRight: 4 },
  selectButton: {
    borderWidth: 1,
    padding: 4,
    borderRadius: 20,
    width: '35%',
    alignItems: 'center',
    backgroundColor: theme.colors.blue500,
    flexDirection: 'row',
    justifyContent: 'center',
  },
});
