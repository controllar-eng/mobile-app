import React, { useState, useEffect, useContext, useCallback } from 'react';
import { View, RefreshControl, StyleSheet, FlatList } from 'react-native';
import { useQuery, useMutation } from '@apollo/client';
import { COUNTDOWN_TIMERS, UPDATE_TIMER } from '../data/gql';
import { differenceInSeconds, isAfter, add } from 'date-fns';
import { List, IconButton, Paragraph, Subheading, Colors } from 'react-native-paper';
import TimerContextMenu from '../components/timer/TimerContextMenu';
import { AppContext } from '../lib/context';
import { subtract, duration, sort } from '../lib/utils';

export default function CountdownListScreen({ navigation }) {
  const [diff, setDiff] = useState(0);
  const [time, setTime] = useState();

  const updateTime = () => setTime(add(new Date(), { seconds: diff }));

  const { createClient, profile } = useContext(AppContext);

  const { loading, error, data } = useQuery(COUNTDOWN_TIMERS, {
    // eslint-disable-next-line camelcase
    variables: { profile_id: profile.id },
    onCompleted: useCallback(data => {
      if (!data) return;
      const { now } = data;
      // calculate the differece between local time and server time
      setDiff(differenceInSeconds(Date.parse(now), new Date()));
      updateTime();
    }, []), // https://github.com/apollographql/apollo-client/issues/6301
  });

  const [updateTimer] = useMutation(UPDATE_TIMER);

  useEffect(() => {
    const interval = setInterval(updateTime, 1000);
    return () => clearInterval(interval);
  }, [time]);

  const listEmptyComp = data && (
    <Subheading style={styles.empty}>Nenhum Timer Regressivo criado no perfil atual.</Subheading>
  );

  return (
    <FlatList
      refreshControl={<RefreshControl refreshing={loading} onRefresh={createClient} />}
      ListEmptyComponent={listEmptyComp}
      ListFooterComponent={<View />}
      ListFooterComponentStyle={styles.footer}
      keyExtractor={item => item.id.toString()}
      data={data && sort(data.timers, 'name')}
      renderItem={({ item: timer }) => {
        // isRunning:  AND (!endTime || startOn+endTime isFuture)
        const parsedStart = Date.parse(timer.startOn);
        const [startHour, startMin, startSec] = timer.startTime.split(':');
        // startOn + startTime isFuture
        const isStartFuture = isAfter(
          add(parsedStart, {
            hours: startHour,
            minutes: startMin,
            seconds: startSec,
          }),
          time,
        );
        let isEndFuture = false;
        if (timer.endTime) {
          const [endHour, endMin, endSec] = timer.endTime.split(':');
          // startOn + endTime isFuture
          isEndFuture = isAfter(
            add(parsedStart, {
              hours: endHour,
              minutes: endMin,
              seconds: endSec,
            }),
            time,
          );
        }

        const isRunning = isStartFuture || isEndFuture;

        const executeTimer = () =>
          updateTimer({
            variables: {
              input: {
                id: timer.id,
                startOn: isRunning ? 'STOP' : 'START',
              },
            },
          });

        const during = duration(subtract(timer.startTime, timer.endTime));

        return (
          <TimerContextMenu
            key={timer.id}
            editing={timer}
            query={COUNTDOWN_TIMERS}
            anchor={
              <List.Item
                onPress={() => navigation.navigate('TimerForm', { timer })}
                title={
                  <Paragraph>
                    {timer.startTime !== '00:00:00' && 'Em '}
                    <Subheading style={styles.title}>
                      {timer.startTime === '00:00:00' ? 'Imediato' : duration(timer.startTime)}
                    </Subheading>
                    {during && (
                      <>
                        {' '}
                        durante <Subheading style={styles.title}>{during}</Subheading>
                      </>
                    )}
                  </Paragraph>
                }
                description={timer.name}
                right={() => (
                  <View style={styles.right}>
                    <IconButton
                      icon={isRunning ? 'stop' : 'play'}
                      color={Colors.green600}
                      style={styles.playIcon}
                      size={32}
                      animated
                      onPress={executeTimer}
                    />
                  </View>
                )}
              />
            }
          />
        );
      }}
      windowSize={10}
    />
  );
}
const styles = StyleSheet.create({
  footer: { marginBottom: 80 },
  title: { fontWeight: 'bold' },
  right: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  playIcon: { marginTop: 0, marginBottom: 0 },
  empty: { padding: 16 },
});
