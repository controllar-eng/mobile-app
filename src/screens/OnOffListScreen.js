import React, { useContext } from 'react';
import { View, RefreshControl, StyleSheet, FlatList } from 'react-native';
import { useQuery, useMutation } from '@apollo/client';
import { Switch, List, Title, Subheading, useTheme, Menu } from 'react-native-paper';
import { format, parseISO } from 'date-fns';
import { ONOFF_TIMERS, UPDATE_TIMER, TIMER_TYPE } from '../data/gql';
import TimerContextMenu from '../components/timer/TimerContextMenu';
import { AppContext } from '../lib/context';
import { sort } from '../lib/utils';

const WEEKDAYS = ['D', 'S', 'T', 'Q', 'Q', 'S', 'S'];

export default function OnOffListScreen({ navigation }) {
  const { createClient, profile } = useContext(AppContext);

  const { loading, error, data } = useQuery(ONOFF_TIMERS, {
    // eslint-disable-next-line camelcase
    variables: { profile_id: profile.id },
  });
  const [updateTimer] = useMutation(UPDATE_TIMER);
  const theme = useTheme();

  const listEmptyComp = data && (
    <Subheading style={styles.empty}>Nenhum Timer On Off criado no perfil atual.</Subheading>
  );
  return (
    <FlatList
      refreshControl={<RefreshControl refreshing={loading} onRefresh={createClient} />}
      ListEmptyComponent={listEmptyComp}
      ListFooterComponent={<View />}
      ListFooterComponentStyle={styles.footer}
      keyExtractor={item => item.id.toString()}
      data={data && sort(data.timers, 'name')}
      renderItem={({ item: timer }) => {
        const toggleTimer = value =>
          updateTimer({
            variables: { input: { id: timer.id, active: value } },
            optimisticResponse: {
              __typename: 'Mutation',
              updateTimer: {
                __typename: 'TimerResult',
                timer: { __typename: 'Timer', ...timer, active: value },
                error: null,
              },
            },
          });

        return (
          <TimerContextMenu
            key={timer.id}
            editing={timer}
            query={ONOFF_TIMERS}
            anchor={
              <List.Item
                onPress={() => navigation.navigate('TimerForm', { timer })}
                title={
                  <Title>
                    {timer.startTime}
                    {timer.endTime && ` - ${timer.endTime}`}
                  </Title>
                }
                description={timer.name}
                right={props => (
                  <View style={styles.right}>
                    <View style={styles.date}>
                      {timer.type === TIMER_TYPE.ONOFF_WEEKDAY ? (
                        WEEKDAYS.map((weekday, idx) => (
                          <Subheading
                            key={`${weekday}-${idx}`}
                            style={[
                              { fontWeight: 'bold' },
                              timer.startOn && timer.startOn.includes(idx)
                                ? { color: theme.colors.primary }
                                : { color: theme.colors.disabled },
                            ]}
                          >
                            {weekday}{' '}
                          </Subheading>
                        ))
                      ) : (
                        <Subheading>{format(parseISO(timer.startOn), 'dd/MM/yyyy')} </Subheading>
                      )}
                    </View>
                    <View>
                      <Switch value={timer.active} onValueChange={toggleTimer} />
                    </View>
                  </View>
                )}
              />
            }
          />
        );
      }}
      windowSize={10}
    />
  );
}
const styles = StyleSheet.create({
  footer: { marginBottom: 80 },
  right: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  date: { flexDirection: 'row', marginRight: 5 },
  empty: { padding: 16 },
});
