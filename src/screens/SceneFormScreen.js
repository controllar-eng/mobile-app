import { useMutation } from '@apollo/client';
import AsyncStorage from '@react-native-community/async-storage';
import React, { useContext, useEffect, useState } from 'react';
import { StyleSheet, View } from 'react-native';
import { HelperText, TextInput, TouchableRipple } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import Actions from '../components/Actions';
import IconPickerDialog from '../components/scene/IconPickerDialog';
import SceneIcon from '../components/scene/SceneIcon';
import { CREATE_SCENE, SCENES, UPDATE_SCENE } from '../data/gql';
import { AppContext } from '../lib/context';

export default function SceneFormScreen({ navigation, route }) {
  const { profile, createClient } = useContext(AppContext);
  const [sensor, setVerificationLumino] = useState(false);

  const [createScene] = useMutation(CREATE_SCENE, {
    update(cache, { data: { createScene } }) {
      if (!createScene.scene) return;
      try {
        const { scenes } = cache.readQuery({
          query: SCENES,
          // eslint-disable-next-line camelcase
          variables: { profile_id: profile.id },
        });
        cache.writeQuery({
          query: SCENES,
          data: { scenes: [...scenes, createScene.scene] },
          // eslint-disable-next-line camelcase
          variables: { profile_id: profile.id },
        });
      } catch (e) {
        console.log('ROOT_QUERY.scenes not on cache yet', e);
      }
    },
  });
  const [updateScene] = useMutation(UPDATE_SCENE);

  const scene = route.params?.scene || {};

  const [iconPickerOpen, setIconPickerOpen] = useState(false);

  // Scene fields
  const [name, setName] = useState(scene.name || '');
  const [icon, setIcon] = useState(scene.icon);

  const [error, setError] = useState();

  useEffect(() => {
    if (scene.id) navigation.setOptions({ headerTitle: 'Editar Cena' });
    if (sensor) navigation.setOptions({ headerTitle: 'Lumino de verificação de status' });
  }, [sensor]);

  return (
    <View style={styles.container}>
      <IconPickerDialog
        visible={iconPickerOpen}
        onDismiss={() => setIconPickerOpen(false)}
        onConfirm={icon => {
          setIcon(icon);
          setIconPickerOpen(false);
        }}
        current={icon}
        style={!sensor && { display: 'none' }}
      />

      <View style={!sensor ? styles.titleRow : { display: 'none' }}>
        <TouchableRipple style={styles.iconPicker} onPress={() => setIconPickerOpen(true)}>
          <>
            <SceneIcon name={icon} size={32} style={styles.sceneIcon} />
            <Icon name='chevron-down' size={20} />
          </>
        </TouchableRipple>

        <View style={styles.name}>
          <TextInput label='Nome' value={name} onChangeText={setName} error={error && !!error.name} />
          <HelperText type='error' visible={error && !!error.name}>
            {error && error.name}
          </HelperText>
        </View>
      </View>

      <Actions
        actions={scene.actions}
        initialOffOnly={scene.offOnly}
        navigation={navigation}
        sensor={sensor}
        setVerificationLumino={setVerificationLumino}
        updateFn={async (actions, offOnly) => {
          actions = Object.assign([], actions).map(action => {
            if (action.code > 3000) action.code = 3000;
            return action;
          });
          const newScene = {
            // eslint-disable-next-line camelcase
            profile_id: profile.id,
            name,
            actions,
            offOnly,
            icon,
          };

          if (scene.id) {
            const { data } = await updateScene({
              variables: { input: { ...newScene, id: scene.id } },
            });

            setError(data.updateScene.error);

            if (!data.updateScene.error) {
              const widget1Scene = await AsyncStorage.getItem('@widget_1_scene');
              const widget2Scene = await AsyncStorage.getItem('@widget_2_scene');
              const widget3Scene = await AsyncStorage.getItem('@widget_3_scene');
              const widget4Scene = await AsyncStorage.getItem('@widget_4_scene');
              if (widget1Scene) {
                const widget1SceneJson = JSON.parse(widget1Scene);
                if (widget1SceneJson.id === scene.id)
                  await AsyncStorage.setItem('@widget_1_scene', JSON.stringify({ ...newScene, id: scene.id }));
              } else if (widget2Scene) {
                const widget2SceneJson = JSON.parse(widget2Scene);
                if (widget2SceneJson.id === scene.id)
                  await AsyncStorage.setItem('@widget_2_scene', JSON.stringify({ ...newScene, id: scene.id }));
              } else if (widget3Scene) {
                const widget3SceneJson = JSON.parse(widget3Scene);
                if (widget3SceneJson.id === scene.id)
                  await AsyncStorage.setItem('@widget_3_scene', JSON.stringify({ ...newScene, id: scene.id }));
              } else if (widget4Scene) {
                const widget4SceneJson = JSON.parse(widget4Scene);
                if (widget4SceneJson.id === scene.id)
                  await AsyncStorage.setItem('@widget_4_scene', JSON.stringify({ ...newScene, id: scene.id }));
              }
              navigation.popToTop();
              createClient(); // TODO: Use cache after it
            }
          } else {
            const { data } = await createScene({
              variables: { input: newScene },
            });

            setError(data.createScene.error);

            if (!data.createScene.error) {
              navigation.popToTop();
              createClient(); // TODO: Use cache after it
            }
          }
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: { flex: 1 },
  sceneIcon: { marginRight: 3 },
  iconPicker: {
    flexDirection: 'row',
    paddingRight: 8,
    paddingLeft: 8,
    marginRight: 5,
    marginBottom: 20,
    alignItems: 'center',
    opacity: 0.54,
  },
  titleRow: { flexDirection: 'row', padding: 10, paddingBottom: 0 },
  name: { flexGrow: 1 },
});
