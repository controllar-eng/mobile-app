import { useMutation, useQuery } from '@apollo/client';
import Clipboard from '@react-native-community/clipboard';
import React, { useContext, useEffect, useState } from 'react';
import { Alert, BackHandler, Dimensions, Keyboard, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import CountDown from 'react-native-countdown-component';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import { Button, Dialog, IconButton, Portal } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import ActionsDevice from '../components/ActionsDevice';
import KeyboardAwareDialog from '../components/KeyboardAwareDialog';
import { LoadingControlls } from '../components/LoadingControlls';
import TextInputInAPortal from '../components/TextInputInPortal';
import { createControlls } from '../components/controlls/createControlls';
import {
  BROADLINKS,
  CANCEL_RECORD,
  CREATE_MULTIMEDIA_DEVICE,
  LIST_BROADLINKS_CONNECTED,
  LIST_IRDB_BUTTONS,
  LIST_IRDB_CSVS,
  LIST_IRDB_DEVICES,
  LIST_IRDB_MODELS,
  SAVE_PATH,
  SET_DEVICE_STATE,
  UPDATE_MULTIMEDIA_DEVICE,
  UPDATE_YAML_FILE,
} from '../data/gql';
import { AppContext } from '../lib/context';
import { theme } from '../lib/utils';
import EmitterScreen from './EmitterScreen';

const isIOS = Platform.OS === 'ios';

export default function DeviceFormScreen({ navigation, route }) {
  const [dimensions] = useState(Dimensions.get('window'));

  const { createClient, client } = useContext(AppContext);
  const device = route.params?.device || null;
  const screenType = route.params?.screenType || null;

  const [buttonLuminos, setButtonLuminos] = useState([]);
  const [buttonLuminosModal, setButtonLuminosModal] = useState(false);
  const [luminos, setLuminos] = useState([]);

  const windowButtons = {
    power: { id: 0, indicator: { 0: 'OPEN_CURTAIN', 1: 'OPEN_SHUTTER1', 2: 'OPEN_SHUTTER2' }, name: 'ABRIR ' },
    stop: { id: 1, indicator: { 0: 'STOP_CURTAIN', 1: 'STOP_SHUTTER1', 2: 'STOP_SHUTTER2' }, name: 'PARAR ' },
    'power-off': {
      id: 2,
      indicator: { 0: 'CLOSE_CURTAIN', 1: 'CLOSE_SHUTTER1', 2: 'CLOSE_SHUTTER2' },
      name: 'FECHAR ',
    },
  };
  const fan1Buttons = ['lightbulb', 'speed-one', 'speed-two', 'speed-three', 'ventilation', 'exhaust'];
  const fan2Buttons = ['direction', 'speed-plus', 'speed-minus'];

  const indicatorsArray = [];

  Object.keys(windowButtons).map(key =>
    Object.values(windowButtons[key].indicator).map(value => indicatorsArray.push(value)),
  );

  const hasButtons = () => device && device.buttons[0];

  const broadlinks = useQuery(BROADLINKS); // List Broadlink Names
  const models = useQuery(LIST_IRDB_MODELS); // IRDB Models 'Samsung, LG'
  const [listIrdbDevices] = useMutation(LIST_IRDB_DEVICES); // IRDB Devices 'Air Conditioner, TV'
  const [listIrdbCsvs] = useMutation(LIST_IRDB_CSVS); // IRDB Csvs '8,1.csv'
  const [listIrdbButtons] = useMutation(LIST_IRDB_BUTTONS); // IRDB Buttons: 'volume-down'
  const [savePath] = useMutation(SAVE_PATH); // Start Record Mode at Emitter
  const [cancelRecord] = useMutation(CANCEL_RECORD); // Start Record Mode at Emitter
  const [setDeviceState] = useMutation(SET_DEVICE_STATE); // Send Actions to API set on Socket
  const [updateYamlFile] = useMutation(UPDATE_YAML_FILE); // Rewrite Yaml File When User change some values
  const [createMultimediaDevice] = useMutation(CREATE_MULTIMEDIA_DEVICE); // Create Multimedia Device Query
  const [updateMultimediaDevice] = useMutation(UPDATE_MULTIMEDIA_DEVICE); // Update Multimedia Device Query
  const [listBroadlinksConnected] = useMutation(LIST_BROADLINKS_CONNECTED); // Mac Array

  const [modelsKey, setmodelsKey] = useState([]); // Parse Models Array to Array with key and label
  const [devicesDB, setDeviceDB] = useState([]); // Array with IRDB Devices
  const [csvsDB, setcsvsDB] = useState([]); // Array with IRDB CSVS
  const [luminosAvailable] = useState([]); // Array with Luminos Availabel

  const [loadingData, setLoadingData] = useState(false); // Its true when IRDB Data Query isn't finished
  const [buttonRecord, setButtonRecord] = useState(''); // Set Record Mode
  const [modalCopy, setModalCopy] = useState(''); // Set Copy Mode
  const [isSaving, setSaving] = useState(false); // Its true when data is saving on DB

  const [multimediaName, setMultimediaName] = useState(device ? device.name : '');
  const [controllType, setControllType] = useState(device ? device.indicator : '');
  const [lumino, setLumino] = useState(null);
  const [emitter, setEmitter] = useState(hasButtons() ? device.buttons[0].emitter : 'broadlink');

  const [channel, setChannel] = useState(hasButtons() ? device.buttons[0].channel : null);
  const [freq, setFreq] = useState(
    hasButtons() && device.buttons[0].freq != undefined ? device.buttons[0].freq : 38000,
  );
  const [macAddress, setMacAddress] = useState(
    hasButtons() && device.buttons[0].macAddress ? device.buttons[0].macAddress : '',
  );
  const [macArray, setMacArray] = useState([]);
  const [oldDevices] = useState([]);

  const [multimediaDevice] = useState({ type: 'MULTIMEDIA', buttons: [] });

  const isWindowButton = indicatorsArray.includes(controllType);
  const [iconWindow, setIconWindow] = useState(0);
  const [signalGlobal, setSignalGlobal] = useState(
    device &&
      (isWindowButton ||
        device.indicator === 'GATE' ||
        device.indicator === 'RF_DEFAULT' ||
        device.indicator === 'WINDOW')
      ? 'rf'
      : 'ir',
  );

  const [clipboardIOSModal, setClipboardIOSModal] = useState(false);
  const [clipboardIOSText, setClipboardIOSText] = useState('');

  const [controllStyle, setControllStyle] = useState(
    device &&
      (device.indicator.includes('TV_') || device.indicator.includes('AIR_') || device.indicator.includes('FAN'))
      ? device.indicator.split('_')[1]
      : '',
  );
  const [isOpemControll, setOpemControll] = useState(false);
  const [textInputFocus, setTextInputFocus] = useState(false);

  const controlls = createControlls(
    handleButton,
    multimediaDevice,
    setModalCopy,
    buttonRecord,
    buttonLuminos,
    device,
    isOpemControll,
    controllType,
    null,
    true,
  );

  useEffect(() => {
    const luminosKey = [];
    Array.from({ length: 200 }, (_, i) => i + 600).map(
      number => luminosAvailable.includes(number) && luminosKey.push({ key: number, label: number, searhKey: number }),
    );
    setLuminos(luminosKey);
    if (buttonLuminos.length === 0) setButtonLuminos(luminosAvailable.slice(1, 7));
  }, [luminosAvailable.length, controllType === 'WINDOW', controllType.includes('FAN')]);

  useEffect(() => {
    multimediaName[3] === '#' && setMultimediaName(multimediaName.substring(11).trim());

    Object.values(windowButtons).map(value => {
      if (Object.values(value.indicator).includes(device?.indicator))
        setIconWindow(Object.values(value.indicator).indexOf(device.indicator));
    });
    loadBroadlinks();

    const luminosUnavailable = [];
    const devices = client.cache.data.data;

    Object.keys(devices).map(i => {
      if (i.includes('DeviceState')) oldDevices.push(devices[i]);
      if (!devices[i].name?.includes('Lumino') && devices[i].id >= 600 && devices[i].id < 800)
        luminosUnavailable.push(devices[i].id);
    });
    Array.from({ length: 200 }, (_, i) => i + 600).map(
      number => !luminosUnavailable.includes(number) && luminosAvailable.push(number),
    );
  }, []);

  // If Mode is edit, set buttons on const
  useEffect(() => {
    if (hasButtons()) {
      let count = 2000;
      const auxButtons = [];
      device.buttons.map(button => {
        const aux = Object.assign({}, button);
        delete aux.__typename;
        aux.id = count++;
        auxButtons.push(aux);
      });
      multimediaDevice.buttons = auxButtons;
      updateYamlFilebyArray(emitter, channel, macAddress, freq);
    }
  }, [device]);

  // Parse Models Array to Array with key and label and Set Luminos Available to array
  useEffect(() => {
    if (models.data) {
      let modelsKeyAux = [];
      models.data.listIrdbModels.map(model => modelsKeyAux.push({ key: model, label: model, searhKey: model }));
      setmodelsKey(modelsKeyAux);
    }
  }, [models.data]);

  // If query returns a error, its because API is Old on Board
  useEffect(() => {
    if (models.error) {
      navigation.popToTop();
      Alert.alert('Atenção', 'Seu sistema necessita de atualizações para funcionar com dispositivos multimídia');
    }
  }, [models.error]);

  useEffect(() => {
    navigation.setOptions({
      headerTitle:
        buttonRecord === ''
          ? `Novo ${screenType === 'emitter' ? 'Emissor' : 'Dispositivo'}`
          : 'Gravando... Aguarde a finalização',
      headerLeft:
        buttonRecord !== ''
          ? () => null
          : () => (
              <IconButton
                onPress={() => {
                  buttonRecord === '' && navigation.popToTop();
                }}
                icon='arrow-left'
                color={theme.colors.blue500}
              />
            ),
      headerRight:
        buttonRecord !== '' || screenType === 'emitter'
          ? () => null
          : () => <IconButton onPress={() => saveData()} icon='content-save' color={theme.colors.blue500} />,
    });
  }, [
    buttonRecord,
    navigation,
    multimediaName,
    controllType,
    buttonLuminos,
    controllStyle,
    lumino,
    iconWindow,
    emitter,
    screenType,
  ]);

  useEffect(() => {
    const backAction = () => buttonRecord !== '';
    const backHandler = BackHandler.addEventListener('hardwareBackPress', backAction);
    return () => backHandler.remove();
  }, [buttonRecord, navigation]);

  async function loadBroadlinks() {
    const { data } = await listBroadlinksConnected();
    const macArrayAux = [];

    for (const device of data.listBroadlinksConnected) {
      if (device !== '') {
        const all = device.slice(0, -1);
        const devicePart = device.substring(0, device.indexOf('-'));
        const mac = device.substring(device.indexOf('-') + 2, device.length - 1);

        macArrayAux.push({ all, device: devicePart, mac });
      }
    }

    setMacArray(macArrayAux);
  }

  // Send Action to API Send to Socket
  async function handleButton(data) {
    data &&
      multimediaDevice.buttons.filter(e => e.buttonName === data.buttonName).length &&
      buttonRecord === '' &&
      setDeviceState({ variables: { id: data.id, intensity: 50 } });
  }

  // When user press for a long time
  async function setControll(data, copy) {
    if (multimediaName == '') {
      Alert.alert('Erro ao salvar o botão, verifique se o nome do dispositivo foi preenchido');
      return;
    }

    if (buttonRecord !== '') return; // IF Record mode isn't in use

    let copyPath = '';
    if (copy) {
      // if user Press 'Copiar' on modal
      copyPath = isIOS ? clipboardIOSText : await Clipboard.getString(); // Get Clipboard
      if (!copyPath.includes('sendir')) return Alert.alert('Formato de Código Invalido');
      setLoadingData(true);
    } else {
      setButtonRecord(data); // Set Record Mode
    }

    // Search the the biggest ID from buttons array to send from API increment on yaml file
    // ex: 2000, 2001, 2002 (its because at the moment, the buttons doesn't have id by db)
    const bigger = Math.max(...multimediaDevice.buttons.map(butt => butt.id), 0);
    multimediaDevice.buttons = multimediaDevice.buttons.filter(el => el.buttonName !== data);
    // If Buttons is already exists, its removed because the server delete folder when a new record is started
    multimediaDevice.buttons = multimediaDevice.buttons.filter(el => el.buttonName !== data);

    const deviceName = multimediaName[3] === '#' ? multimediaName.substring(11).trim() : multimediaName;
    const response = await savePath({
      variables: {
        bigger,
        // eslint-disable-next-line camelcase
        multimedia_device_name: deviceName,
        buttonName: data,
        emitter: emitter,
        signal: signalGlobal,
        channel: channel,
        freq: freq,
        copy: copy ? `'${copyPath.replace(/\s/g, '')}'` : null,
        type: controllType,
        macAddress: macAddress?.toLowerCase(),
      },
    });

    if (response.data.savePath === 'error_1') {
      Alert.alert(`Botão não ${copy ? 'Salvo' : 'Gravado'}, tente novamente...`);
    } else {
      // If Returns a path ex: 'TVSamsung/power-on'
      addRecordButtonOnArray(data, response.data.savePath, bigger); // Add Button on array
      Alert.alert(copy ? 'Código [' + copyPath.substring(0, 15) + '...] Salvo com sucesso' : 'Sucesso na Gravação');
    }
    setButtonRecord(''); // Reset Record Mode
    setLoadingData(false);
  }

  function addRecordButtonOnArray(data, thispath, bigger) {
    const button = {
      buttonName: data,
      emitter: emitter,
      signal: signalGlobal,
      cmdFmt: 'path',
      commandFile: thispath,
      channel,
      freq: freq,
      id: bigger === 0 ? 2000 : bigger + 1,
      macAddress,
    };
    multimediaDevice.buttons = [...multimediaDevice.buttons, button];
  }

  function updateButtonsOnArray({ value, dataValue, updateType }) {
    Keyboard.dismiss();
    setLoadingData(true);
    updateType === 'freq'
      ? setFreq(value)
      : updateType === 'mac'
      ? setMacAddress(value)
      : updateType === 'channel'
      ? setChannel(value)
      : updateType === 'emitter' && setEmitter(value);
    if (
      dataValue.device !== '' &&
      dataValue.model !== '' &&
      dataValue.style !== '' &&
      multimediaDevice.buttons.filter(el => !el.cmdFmt === 'path').length
    )
      updateType === 'freq'
        ? addDBButtonsOnArray(dataValue, emitter, channel, macAddress, value)
        : updateType === 'mac'
        ? addDBButtonsOnArray(dataValue, emitter, channel, value, freq)
        : updateType === 'channel'
        ? addDBButtonsOnArray(dataValue, emitter, value, macAddress, freq)
        : updateType === 'emitter' && addDBButtonsOnArray(dataValue, value, channel, macAddress, freq);
    else
      updateType === 'freq'
        ? updateYamlFilebyArray(emitter, channel, macAddress, value)
        : updateType === 'mac'
        ? updateYamlFilebyArray(emitter, channel, value, freq)
        : updateType === 'channel'
        ? updateYamlFilebyArray(emitter, value, macAddress, freq)
        : updateType === 'emitter' && updateYamlFilebyArray(value, channel, macAddress, freq);
  }

  async function updateYamlFilebyArray(_emitter, _channel, _mac, _freq) {
    const buttons = multimediaDevice.buttons.filter(el => {
      el.emitter = _emitter;
      el.channel = _channel;
      el.freq = _freq;
      el.macAddress = _mac?.toLowerCase();
      return el;
    });
    await updateYamlFile({ variables: { buttons } });
    setLoadingData(false);
  }

  async function addDBButtonsOnArray({ model, device, style: csv }, _emitter, _channel, _mac, _freq) {
    if (model === '') return;

    const devices = await listIrdbDevices({ variables: { model: model } });
    devices.data && setDeviceDB(devices.data.listIrdbDevices);

    if (device === '') return;

    const csvs = await listIrdbCsvs({ variables: { model: model, device: device } });
    csvs.data && setcsvsDB(csvs.data.listIrdbCsvs);

    if (csv === '') return;

    const { data } = await listIrdbButtons({
      variables: {
        model,
        device,
        csv,
        emitter: _emitter,
        channel: _channel,
        freq: _freq,
        macAddress: _mac?.toLowerCase(),
      },
    });

    if (data.listIrdbButtons) {
      multimediaDevice.buttons = []; // Clearing all buttons

      const newButtons = [];
      for (const {
        functionname: buttonName,
        id,
        protocol: commandProtocol,
        device: commandDevice,
        subdevice: commandSubdevice,
        _function: commandFunction,
      } of data.listIrdbButtons) {
        if (buttonName) {
          newButtons.push({
            id,
            buttonName,
            emitter: _emitter,
            signal: 'ir',
            cmdFmt: 'protocol',
            commandProtocol,
            commandDevice,
            commandSubdevice,
            commandFunction,
            channel: _emitter === 'THMedia' ? _channel : null,
            freq: _freq,
            macAddress,
          });
        }
      }
      multimediaDevice.buttons = newButtons; // Updating all buttons at once
      setLoadingData(false);
    }
  }

  async function saveData() {
    const finalName = multimediaName.replace(/^###(.{11})/, '$1');

    const isFan = controllType.includes('FAN');

    const compareNames = (name1, name2) => name1.replace(/\s/g, '') === name2.replace(/\s/g, '');

    const isNameMatch = name => compareNames(name, finalName) || compareNames(name.substring(11), finalName);

    // Verify if exists a device with same name and type
    const hasDeviceWithSameName = () =>
      oldDevices.some(data => {
        if (data && data.type === 'MULTIMEDIA') {
          const sameName = isNameMatch(data.name);
          const sameType =
            data.indicator === controllType ||
            (controllType === 'WINDOW' && indicatorsArray.some(el => el === data.indicator));
          const notEditMode = !(device && compareNames(data.name, device.name));

          return sameName && sameType && notEditMode;
        }
        return false;
      });

    // Verify if window Luminos Array contains duplicated values
    const hasDuplicated = arr => arr.some(x => arr.indexOf(x) !== arr.lastIndexOf(x));

    // Verify is exists a device on db and update the register
    const hasLuminoOnDB = _id => oldDevices.some(value => value.id === _id);

    const updateFn = async device =>
      hasLuminoOnDB(device.id)
        ? await updateMultimediaDevice({ variables: { device: device } })
        : await createMultimediaDevice({ variables: { input: device } });

    const getDeviceFromButton = (buttom, id, indicator, name) => {
      const buttons = [];
      // eslint-disable-next-line camelcase
      buttons.push({ ...buttom, buttonName: 'power-off', multimedia_device_id: id });
      if (buttom.buttonName.includes('lightbulb'))
        // eslint-disable-next-line camelcase
        buttons.push({ ...buttom, buttonName: 'power', multimedia_device_id: id });
      return {
        id,
        indicator,
        name,
        type: 'MULTIMEDIA',
        buttons,
      };
    };

    const validateForm = () => {
      if (!device && isFan && buttonLuminos.includes(lumino))
        return 'O Lumino do dispositivo não pode ser igual aos luminos dos botões';
      if (hasDuplicated(buttonLuminos)) return 'Os Luminos Precisam ser diferentes';
      if (hasDeviceWithSameName()) return 'Dispositivo existente com o mesmo nome';
      if (multimediaName === '' || controllType === '' || (isFan && !controllStyle))
        return 'Preencha os campos obrigatórios';
      return null;
    };

    const editWindowDevice = () => {
      multimediaDevice.buttons = [multimediaDevice.buttons[multimediaDevice.buttons.length - 1]];
      const buttonType = /^OPEN_.*$/.test(controllType)
        ? 'power'
        : /^STOP_.*$/.test(controllType)
        ? 'stop'
        : 'power-off';
      multimediaDevice.indicator = Object.values(windowButtons[buttonType].indicator)[iconWindow];
    };

    const editFanOrTVOrAirDevice = () => {
      multimediaDevice.buttons = multimediaDevice.buttons.map(button => {
        if (button.buttonName === 'power-off') {
          const powerButton = multimediaDevice.buttons.find(b => b.buttonName === 'power');
          return { ...button, ...powerButton, buttonName: 'power-off' };
        }
        return button;
      });
    };

    const editFanDevice = async () => {
      const mergedArray = [...fan1Buttons, ...fan2Buttons];
      const devicesUpdated = [];

      await Promise.all(
        oldDevices.map(async d => {
          if (mergedArray.some(el => d.name.includes(el) && d.name.includes(multimediaDevice.id))) {
            const name = mergedArray.filter(el => d.name.includes(el))[0];
            const newButton = multimediaDevice.buttons.filter(b => b.buttonName === name)[0];
            devicesUpdated.push(name);
            await updateMultimediaDevice({
              variables: { device: getDeviceFromButton(newButton, d.id, d.indicator, d.name) },
            });
          }
        }),
      );

      await Promise.all(
        multimediaDevice.buttons.map(b => {
          if (!devicesUpdated.includes(b.buttonName) && !b.buttonName.includes('power')) {
            const fanButtons = controllStyle == 1 ? fan1Buttons : fan2Buttons;
            fanButtons.map(async (name, count) => {
              if (name === b.buttonName) {
                const button = multimediaDevice.buttons.filter(b => b.buttonName === name)[0];
                const indicator = name === 'lightbulb' ? 'LIGHTBULB' : multimediaDevice.indicator;
                const id = buttonLuminos[count];
                const _name = `${multimediaDevice.name} - ${multimediaDevice.id} - ${button.buttonName}`;
                await createMultimediaDevice({
                  variables: { input: getDeviceFromButton(button, id, indicator, _name) },
                });
              }
            });
          }
        }),
      );
    };

    if (validateForm()) {
      Alert.alert(validateForm());
    } else {
      setSaving(true);
      multimediaDevice.id = device ? device.id : lumino ?? luminosAvailable[0];

      multimediaDevice.indicator = controllType.startsWith('TV')
        ? controllStyle
          ? `${controllType.split('_')[0]}_${controllStyle}`
          : 'TV'
        : controllType.startsWith('AIR')
        ? controllStyle
          ? `${controllType.split('_')[0]}_${controllStyle}`
          : 'AIR'
        : controllType.startsWith('FAN')
        ? `${controllType.split('_')[0]}_${controllStyle}`
        : controllType;

      // If type is a window and is a edit mode, set name with id and '#' to list with order
      const presetName =
        device && isWindowButton
          ? multimediaDevice.id +
            '# ' +
            windowButtons[Object.keys(windowButtons)[parseInt(indicatorsArray.indexOf(controllType) / 3)]].name
          : '';
      multimediaDevice.name = multimediaName[3] === '#' ? multimediaName : presetName + multimediaName;

      // If mode is edit, delete the button.id from buttons for API can update
      multimediaDevice.buttons.map(button => {
        // eslint-disable-next-line camelcase
        if (hasLuminoOnDB(multimediaDevice.id)) button.multimedia_device_id = multimediaDevice.id;
        delete button.id;
      });

      // If controll type is WINDOW or type FAN on create mode, one device is created for each button
      if ((controllType === 'WINDOW' || isFan) && !device) {
        multimediaDevice.buttons.map(async button => {
          const bn = button.buttonName;
          const fanButtons = controllStyle == 1 ? fan1Buttons : fan2Buttons;
          const id = buttonLuminos[isFan ? fanButtons.indexOf(bn) : windowButtons[bn].id];
          const indicator = isFan
            ? bn === 'lightbulb'
              ? 'LIGHTBULB'
              : multimediaDevice.indicator
            : windowButtons[bn].indicator[iconWindow];
          const name = isFan
            ? `${multimediaDevice.name} - ${multimediaDevice.id} - ${bn}`
            : buttonLuminos[windowButtons[bn].id] + '# ' + windowButtons[bn].name + multimediaDevice.name;

          if (controllType === 'WINDOW' || (isFan && !name.includes('power')))
            await updateFn(getDeviceFromButton(button, id, indicator, name));
        });
      }
      if (controllType !== 'WINDOW') {
        // Update Device
        if (device) {
          if (isWindowButton) editWindowDevice();
          if (controllStyle) editFanOrTVOrAirDevice();
          if (isFan) await editFanDevice();
        }

        // If buttons array just has one power, the other power button is create with same values
        const powerButton = multimediaDevice.buttons.find(el => el.buttonName === 'power');
        const powerOffButton = multimediaDevice.buttons.find(el => el.buttonName === 'power-off');

        if (powerButton && !powerOffButton) {
          multimediaDevice.buttons.push({ ...powerButton, buttonName: 'power-off' });
        } else if (
          powerOffButton &&
          !powerButton &&
          !isFan &&
          controllType !== 'RF_DEFAULT' &&
          controllType !== 'GATE'
        ) {
          multimediaDevice.buttons.push({ ...powerOffButton, buttonName: 'power' });
        }

        // Save or Update Device
        await updateFn(multimediaDevice);
      }
      navigation.popToTop();
      createClient(); // TODO: Use cache after the create device
      setSaving(false);
    }
  }

  // Clean Values when user change some selects
  function updatePicker(data) {
    if (data.model !== '' && data.device === '' && data.style === '') {
      setcsvsDB([]);
      setDeviceDB([]);
      multimediaDevice.buttons = [];
    } else if (data.model !== '' && data.device !== '' && data.style === '') {
      setcsvsDB([]);
      multimediaDevice.buttons = [];
    } else setLoadingData(true);
    addDBButtonsOnArray(data, emitter, channel, macAddress, freq);
  }

  function controllTypeChange(data) {
    setOpemControll(false);
    Keyboard.dismiss();
    setControllType(data);
    if (data === 'WINDOW' || data === 'GATE' || data === 'FANRF' || data === 'RF_DEFAULT') setSignalGlobal('rf');
    else setSignalGlobal('ir');
    multimediaDevice.buttons = [];
  }

  if (screenType == 'emitter') return <EmitterScreen navigation={navigation} />;
  if (models.loading || isSaving)
    return <LoadingControlls title={!isSaving ? 'Carregando dados dos controles...' : 'Salvando Controle'} />;

  return (
    <View
      style={[
        { flex: 1, justifyContent: 'space-between' },
        buttonRecord !== '' && { backgroundColor: theme.colors.gray600 },
      ]}
    >
      <View style={styles.container}>
        <ActionsDevice
          device={device}
          macArray={macArray}
          modelsKey={modelsKey}
          devicesDB={devicesDB}
          csvsDB={csvsDB}
          buttonRecord={buttonRecord}
          luminosAvailable={luminosAvailable}
          broadlinks={broadlinks}
          controllStyle={controllStyle}
          isOpemControll={isOpemControll}
          iconWindow={iconWindow}
          updatePicker={updatePicker}
          loadBroadlinks={loadBroadlinks}
          setIconWindow={setIconWindow}
          setControllStyle={setControllStyle}
          setTextInputFocus={setTextInputFocus}
          controllType={controllType}
          controllTypeChange={controllTypeChange}
          lumino={lumino}
          setLumino={setLumino}
          multimediaName={multimediaName}
          setMultimediaName={setMultimediaName}
          emitter={emitter}
          setEmitter={updateButtonsOnArray}
          macAddress={macAddress}
          setMacAddress={updateButtonsOnArray}
          channel={channel}
          setChannel={updateButtonsOnArray}
          freq={freq}
          setFreq={updateButtonsOnArray}
        />
      </View>
      <View style={styles.modal}>
        {modalCopy != '' && buttonRecord === '' && (
          <View
            style={[
              styles.alertUpControll,
              { marginHorizontal: controllType === 'WINDOW' || controllType.includes('FAN') ? '5%' : '20%' },
            ]}
          >
            <TouchableOpacity
              onPress={() => {
                if (!isIOS) {
                  setControll(modalCopy, true);
                  setModalCopy('');
                } else setClipboardIOSModal(true);
              }}
              style={[styles.buttonConfirm, { marginRight: 13 }]}
            >
              <Text style={styles.textButtonConfirm}>Colar Código</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setControll(modalCopy, false);
                setModalCopy('');
              }}
              style={[styles.buttonConfirm, { marginRight: 13 }]}
            >
              <Text style={styles.textButtonConfirm}>Gravar</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setModalCopy('')}
              style={[
                styles.buttonConfirm,
                (controllType === 'WINDOW' || controllType.includes('FAN')) && { marginRight: 13 },
              ]}
            >
              <Text style={styles.textButtonConfirm}>Fechar</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => setButtonLuminosModal(true)}
              style={[
                styles.buttonConfirm,
                ((controllType !== 'WINDOW' && !controllType.includes('FAN')) ||
                  (device?.buttons?.map(button => button.buttonName).includes(modalCopy) ?? false) ||
                  (controllType.includes('FAN') && modalCopy.includes('power'))) && { display: 'none' },
              ]}
            >
              <Text style={styles.textButtonConfirm}>Definir Lumino</Text>
              <ModalFilterPicker
                visible={buttonLuminosModal}
                cancelButtonText='Cancelar'
                placeholderText='Digite Aqui...'
                onSelect={({ key }) => {
                  const position =
                    controllType === 'WINDOW'
                      ? windowButtons[modalCopy].id
                      : (controllStyle == 1 ? fan1Buttons : fan2Buttons).indexOf(modalCopy);

                  buttonLuminos[buttonLuminos.indexOf(key)] = buttonLuminos[position];
                  buttonLuminos[position] = key;

                  setButtonLuminos(buttonLuminos);
                  setModalCopy('');
                  setButtonLuminosModal(false);
                }}
                onCancel={() => setButtonLuminosModal(false)}
                options={luminos}
              />
            </TouchableOpacity>
          </View>
        )}
        {buttonRecord !== '' && (
          <View>
            <View style={styles.alertUpControll}>
              <Icon onPress={async () => await cancelRecord()} name='progress-close' size={30} color='red' />
              <Text style={styles.textAlert}>Copiando [{signalGlobal}]</Text>
              <CountDown
                until={emitter === 'THMedia' ? 16 : 41}
                size={17}
                digitStyle={{ backgroundColor: theme.colors.gray100 }}
                digitTxtStyle={{ color: 'red', fontSize: 25 }}
                timeToShow={['S']}
                timeLabels={{ s: '' }}
              />
            </View>
          </View>
        )}
        <View
          style={[
            controllType.startsWith('TV') && !isOpemControll && { position: 'absolute', top: -dimensions.height / 9 },
            controllType.startsWith('TV') && { alignItems: 'center' },
          ]}
        >
          <IconButton
            style={[
              controllType.startsWith('TV') && !modalCopy && !buttonRecord && !loadingData && !textInputFocus
                ? styles.openControllIcon
                : { display: 'none' },
            ]}
            onPress={() => {
              setOpemControll(!isOpemControll);
            }}
            icon={`chevron-${isOpemControll ? 'down' : 'up'}`}
            color='white'
            size={28}
          />
          {loadingData ? (
            <LoadingControlls title={'Carregando dados do controle...'} />
          ) : (
            !textInputFocus &&
            controlls[
              controllType.startsWith('TV')
                ? controllStyle
                  ? `${controllType.split('_')[0]}_${controllStyle}`
                  : 'TV'
                : controllType.startsWith('AIR')
                ? controllStyle
                  ? `${controllType.split('_')[0]}_${controllStyle}`
                  : 'AIR'
                : controllType.startsWith('FAN')
                ? `${controllType.split('_')[0]}_${controllStyle}`
                : controllType
            ]
          )}
        </View>
        <Portal>
          <KeyboardAwareDialog
            visible={clipboardIOSModal}
            onDismiss={() => {
              setClipboardIOSModal(false);
              setModalCopy('');
              setClipboardIOSText('');
            }}
          >
            <Dialog.Title>Cole o código no formato correto</Dialog.Title>
            <Dialog.Content>
              <View>
                <TextInputInAPortal
                  style={{ flexGrow: 1 }}
                  autoFocus
                  mode='outlined'
                  placeholder='sendir,1:1,1,37000,1,1...'
                  value={clipboardIOSText}
                  onChangeText={setClipboardIOSText}
                />
              </View>
            </Dialog.Content>
            <Dialog.Actions>
              <Button
                onPress={() => {
                  setClipboardIOSModal(false);
                  setModalCopy('');
                  setClipboardIOSText('');
                }}
              >
                Fechar
              </Button>
              <Button
                onPress={() => {
                  setControll(modalCopy, true);
                  setModalCopy('');
                  setClipboardIOSModal(false);
                  setClipboardIOSText('');
                }}
              >
                Confirmar
              </Button>
            </Dialog.Actions>
          </KeyboardAwareDialog>
        </Portal>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: { justifyContent: 'space-around' },
  modal: { justifyContent: 'flex-end' },
  alertUpControll: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
    borderRadius: 20,
    flexDirection: 'row',
    backgroundColor: theme.colors.gray100,
    borderWidth: 1,
  },
  buttonConfirm: { backgroundColor: theme.colors.gray400, padding: 7, borderRadius: 6 },
  textButtonConfirm: { color: 'black', fontSize: 14 },
  textAlert: { color: 'black', fontSize: 14, paddingHorizontal: 5 },
  openControllIcon: { position: 'absolute', elevation: 50, top: -36, backgroundColor: theme.colors.blue500 },
});
