import { useMutation, useQuery } from '@apollo/client';
import React, { useCallback, useContext, useEffect, useState } from 'react';
import { Dimensions, FlatList, Image, StyleSheet, Text, View } from 'react-native';
import { IconButton, List } from 'react-native-paper';
import iconNotifications from '../assets/iconNotifications.png';
import { BROADLINKS, DELETE_NOTIFICATIONS, LIST_NOTIFICATIONS } from '../data/gql';
import { AppContext } from '../lib/context';
import { theme } from '../lib/utils';

export default function NotificationsListScreen({ navigation }) {
  const { SET_APP_NOTIFICATIONS, APP_NOTIFICATIONS } = useContext(AppContext);

  const broadlinks = useQuery(BROADLINKS); // List Broadlink Names
  const listNotifications = useQuery(LIST_NOTIFICATIONS);
  const [deleteNotifications] = useMutation(DELETE_NOTIFICATIONS);
  const [dimensions] = useState(Dimensions.get('window'));
  const [dataNot, setDataNot] = useState([]);

  useEffect(() => {
    const database = listNotifications?.data?.listNotifications.map(value => {
      if (value && value?.__typename) {
        delete value.__typename;
        return value;
      }
    });
    if (database) {
      let array = [...database, ...APP_NOTIFICATIONS];
      setDataNot(array.reverse());
    }
  }, [listNotifications.data, broadlinks.data, APP_NOTIFICATIONS.length]);

  const renderItem = useCallback(
    ({ item }) => {
      const broadlink = broadlinks?.data?.broadlinks.filter(el => el?.mac === item?.mac);
      const name = broadlink?.length > 0 ? broadlink[0].name : null;
      return (
        <List.Section key={Math.random().toString()}>
          <List.Item
            title='Erro no Blaster!!'
            titleStyle={{ fontWeight: 'bold', fontSize: 14 }}
            description={(name ?? item?.mac) + ' não foi ativado'}
            descriptionStyle={{ fontSize: 12 }}
            style={styles.containerList}
            left={() => <List.Icon icon='information' color='red' />}
            right={() => (
              <View style={{ justifyContent: 'center' }}>
                <Text style={styles.textDate}> {item.date} </Text>
              </View>
            )}
          />
        </List.Section>
      );
    },
    [listNotifications, broadlinks],
  );

  const image = (
    <View style={[styles.imageContainer, { height: dimensions.height / 1.7 }]}>
      <Image source={iconNotifications} style={{ width: 200, height: 200 }} />
      <Text style={styles.text1}>Você não possui notificações.</Text>
      <Text style={styles.text2}>Nós avisaremos quando tiver alguma novidade por aqui</Text>
    </View>
  );

  const keyExtractor = useCallback(item => Math.random().toString(), []);

  return (
    <View>
      <View style={styles.containerModal}>
        <FlatList
          removeClippedSubviews={true}
          maxToRenderPerBatch={1}
          initialNumToRender={1}
          ListEmptyComponent={image}
          keyExtractor={keyExtractor}
          data={dataNot}
          renderItem={renderItem}
          windowSize={10}
        />
        <IconButton
          style={{ position: 'absolute', bottom: 0, right: 0 }}
          icon='trash-can'
          color={theme.colors.blue500}
          onPress={async () => {
            await deleteNotifications();
            SET_APP_NOTIFICATIONS([]);
            setDataNot([]);
          }}
        />
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  containerList: { marginHorizontal: 14, padding: 0, borderBottomWidth: 1, borderBottomColor: theme.colors.gray200 },
  imageContainer: { flex: 1, alignItems: 'center', justifyContent: 'flex-end' },
  textDate: { fontSize: 12, color: theme.colors.gray700 },
  text1: { color: theme.colors.blue500, fontSize: 18, fontWeight: 'bold' },
  text2: { color: theme.colors.gray700, marginTop: 10, paddingHorizontal: '20%', textAlign: 'center' },
  containerModal: { height: '100%', width: '100%', backgroundColor: 'white' },
});
