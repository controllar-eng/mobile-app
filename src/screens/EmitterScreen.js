import React, { useContext, useEffect, useState } from 'react';
import {
  View,
  Image,
  StyleSheet,
  Keyboard,
  KeyboardAvoidingView,
  Alert,
  Dimensions,
  ScrollView,
  PermissionsAndroid,
} from 'react-native';
import { Text, TextInput, Button, IconButton } from 'react-native-paper';
import WifiManager from 'react-native-wifi-reborn';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import imgWifi from '../assets/imgWifi.png';
import indicator from '../assets/indicator.gif';
import { theme } from '../lib/utils';
import { AppContext } from '../lib/context';
import ModalFilterPicker from 'react-native-modal-filter-picker';

const isIOS = Platform.OS === 'ios';

export default function EmitterScreen({ navigation }) {
  const { PASSWORD, setSSID, setPASSWORD, setSECURITY, SSID } = useContext(AppContext);

  const [ssid, setSsid] = useState(SSID);
  const [password, setPassoword] = useState(PASSWORD);
  const [security, setSecurity] = useState(3);
  const [isStep, setIsStep] = useState(false);
  const [isKeyboardVisible, setKeyboardVisible] = useState(false);
  const [dimensions] = useState(Dimensions.get('window'));
  const [pickerVisible, setPickerVisible] = useState(false);
  const securityText = ['Nenhuma', 'WEP', 'WPA1', 'WPA2', 'WPA1/2'];
  const [securities, setSecurities] = useState([]);

  useEffect(() => {
    const subscription = Dimensions.addEventListener('change', () => {});
    if (subscription != undefined) return () => subscription.remove();
  }, []);

  useEffect(() => {
    const keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', () => {
      setKeyboardVisible(true); // or some other action
    });
    const keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => {
      setKeyboardVisible(false); // or some other action
    });

    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
    };
  }, []);

  useEffect(() => {
    setSSID(ssid);
    setPASSWORD(password);
    setSECURITY(security);
  }, [password, ssid, security]);

  useEffect(() => {
    loadWifi();
    const securitiesKeys = [
      { key: 3, label: 'WPA2', searhKey: 'WPA2' },
      { key: 0, label: 'Nenhuma', searhKey: 'Nenhuma' },
      { key: 1, label: 'WEP', searhKey: 'WEP' },
      { key: 2, label: 'WPA1', searhKey: 'WPA1' },
      { key: 4, label: 'WPA1/2', searhKey: 'WPA1/2' },
    ];
    setSecurities(securitiesKeys);
  }, []);

  const loadWifi = async () => {
    if (!isIOS) {
      const granted = await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        try {
          await WifiManager.getCurrentWifiSSID().then(ssid => setSsid(ssid));
        } catch (error) {
          Alert.alert('Erro ao listar redes, verifique as permissões de localização e se o Local está ativado');
        }
      }
    }
  };

  return isStep ? (
    <View style={styles.blueContainerView}>
      <Image source={imgWifi} style={styles.image} />
      <Text style={styles.helpText}>
        Agora acesse as configurações da rede Wi-Fi do seu Dispositivo e conecte-se a rede
        <Text style={{ fontWeight: 'bold', color: 'white' }}> Blaster_Wifi_Device</Text>, ou outra com nome similar.
        Feito essa conexão, volte aqui e aguarde
        <Text style={{ fontWeight: 'bold', color: 'white' }}> alguns segundos</Text> para terminar a configuração
      </Text>
    </View>
  ) : (
    <KeyboardAvoidingView behavior={Platform.OS === 'ios' ? 'padding' : 'height'} style={styles.container}>
      <ScrollView>
        <View style={{ alignItems: 'center' }}>
          <Text style={styles.title}>Configurar Blaster:</Text>
          {dimensions.height > 600 && <Image style={{ width: 150, height: 150 }} source={indicator} />}
        </View>
        <View accessible={false}>
          <Text style={[styles.stepText, { fontWeight: 'bold', fontSize: 16 }]}>Siga os seguintes passos:</Text>
          <Text style={styles.stepText}>1. Resete seu blaster pressionando com um objeto fino na entrada RESET;</Text>
          <Text style={styles.stepText}>
            2. Matenha pressionado até piscar o led piscar 3 vezes lentamente como na imagem acima;
          </Text>
          <Text style={styles.stepText}>3. Abaixo, Escolha a rede que será vinculada ao emissor: </Text>
        </View>
        <View>
          <View style={{ flexDirection: 'row-reverse' }}>
            <View
              style={{
                width: '20%',
                height: 40,
                marginVertical: 5,
                justifyContent: 'center',
                alignItems: 'flex-end',
              }}
            >
              <Button onPress={() => loadWifi()}>
                <Icon size={26} name='refresh' color='black' style={styles.plusIcon} />
              </Button>
            </View>
            <View style={[styles.containerPicker, { width: '80%' }]}>
              <View>
                <TextInput
                  style={{ height: 40 }}
                  placeholder='Digite o nome da sua rede WIFI'
                  value={ssid}
                  onChangeText={value => setSsid(value.trim())}
                />
              </View>
            </View>
          </View>
          <TextInput
            mode='outlined'
            style={{ height: 40 }}
            placeholder='Digite a senha da rede'
            value={password}
            autoCapitalize='none'
            onChangeText={value => setPassoword(value.trim())}
          />
          <View style={[styles.containerPicker, { marginTop: 10 }]}>
            <Button onPress={() => setPickerVisible(true)}>
              <Text style={[{ fontSize: 14 }, { color: security != '' ? theme.colors.black : theme.colors.gray700 }]}>
                {security ? securityText[security] : 'Definir Segurança'}
              </Text>
            </Button>
            <ModalFilterPicker
              visible={pickerVisible}
              cancelButtonText='Cancelar'
              placeholderText='Digite Aqui...'
              onSelect={({ key }) => {
                setSecurity(key);
                setPickerVisible(false);
              }}
              onCancel={() => setPickerVisible(false)}
              options={securities}
            />
          </View>
        </View>
        <View style={{ marginBottom: 20 }}>
          <Button
            style={styles.button}
            onPress={async () => {
              Keyboard.dismiss();
              setIsStep(true);
              navigation.setOptions({
                headerStyle: { backgroundColor: theme.colors.blue500 },
                headerTintColor: theme.colors.white,
                headerLeft: () => (
                  <IconButton onPress={() => navigation.popToTop()} icon='arrow-left' color={theme.colors.white} />
                ),
              });
            }}
          >
            <Text style={styles.textButton}>Conectar agora</Text>
          </Button>
        </View>
      </ScrollView>
    </KeyboardAvoidingView>
  );
}

const styles = StyleSheet.create({
  blueContainerView: {
    backgroundColor: theme.colors.blue500,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: { width: 300, height: 286, marginVertical: 30 },
  helpText: { textAlign: 'center', color: 'white', width: 300, fontSize: 16 },
  container: { paddingHorizontal: 20, flex: 1 },
  titleView: { alignItems: 'center', width: '100%' },
  title: { fontSize: 18, fontWeight: 'bold', paddingVertical: 20, textAlign: 'center' },
  containerPicker: {
    borderWidth: 1,
    borderColor: theme.colors.gray700,
    borderRadius: 5,
    height: 40,
    backgroundColor: theme.colors.gray100,
    marginVertical: 5,
  },
  button: { backgroundColor: theme.colors.blue500, padding: 8, alignItems: 'center', borderRadius: 6 },
  textButton: { color: 'white', fontSize: 16, fontWeight: 'bold', padding: 3 },
  stepText: { fontSize: 14, letterSpacing: 1, marginBottom: 4, textAlign: 'justify' },
});
