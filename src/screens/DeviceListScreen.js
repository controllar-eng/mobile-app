import { useMutation, useQuery } from '@apollo/client';
import React, { useCallback, useContext, useEffect, useLayoutEffect, useMemo, useRef, useState } from 'react';
import {
  Alert,
  AppState,
  BackHandler,
  Dimensions,
  FlatList,
  Modal,
  Platform,
  RefreshControl,
  StyleSheet,
  TouchableOpacity,
  View,
} from 'react-native';
import { Colors, IconButton, Subheading } from 'react-native-paper';
import Toast from 'react-native-toast-message';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import AsyncStorage from '@react-native-community/async-storage';
import { createControlls } from '../components/controlls/createControlls';
import DeviceEdit from '../components/device/DeviceEdit';
import { getGroupFilter } from '../components/device/DeviceGroupPicker';
import DeviceItem from '../components/device/DeviceItem';
import SetDeviceListGroup from '../components/device/SetDeviceListGroup';
import {
  BROADLINKS,
  DELETE_MULTIMEDIA_DEVICE,
  DEVICE_STATES,
  EXECUTE_SCENE,
  LIST_BUTTONS,
  RESET_MQTT_SERVICES,
  SET_DEVICE_STATE,
} from '../data/gql';
import { AppContext } from '../lib/context';
import { sort, theme } from '../lib/utils';
import { MainContext } from '../navigation/MainNavigator';

const ITEM_HEIGHT = 56;

function useAppResumeListener(handleResume) {
  const appState = useRef(AppState.currentState);

  const _handleAppStateChange = nextAppState => {
    if (appState.current.match(/inactive|background/) && nextAppState === 'active') handleResume();

    appState.current = nextAppState;
  };
  useEffect(() => {
    AppState.addEventListener('change', _handleAppStateChange);
    return () => AppState.removeEventListener('change', _handleAppStateChange);
  }, []);
}

export default function DeviceListScreen({ navigation }) {
  const {
    createClient,
    deviceGroup,
    setDeviceGroup,
    BROADLINK_ERROR,
    SET_BROADLINK_ERROR,
    SET_APP_NOTIFICATIONS,
    APP_NOTIFICATIONS,
    BROADLINKS_CONNECTEDS,
    selectedDevices,
    fromWidget,
    setFromWidget,
    setSelectedDevices,
    oldInstance,
  } = useContext(AppContext);
  const { setHeaderProps } = useContext(MainContext);

  const [clonedData, setClonedData] = useState();
  const [isModalOpem, setModalOpen] = useState(false); // Open or Close Controll Modal
  const [dimensions] = useState(Dimensions.get('window'));
  const [dataButtons, setDataButtons] = useState();
  const [multimediaDevice, setmultimediaDevice] = useState({});
  const [multimediaDeviceId, setmultimediaDeviceId] = useState();

  const listButtons = useQuery(LIST_BUTTONS); // List Device Buttons From API
  const [executeScene] = useMutation(EXECUTE_SCENE);
  const [setDeviceState] = useMutation(SET_DEVICE_STATE);
  const [resetMqttServices] = useMutation(RESET_MQTT_SERVICES);
  // Polling as a workaround for https://github.com/apollographql/apollo-client/issues/6520
  const { data, loading, refetch } = useQuery(DEVICE_STATES);
  const broadlinks = useQuery(BROADLINKS); // List Broadlink Names

  const fanButtonDevices = [
    'lightbulb',
    'speed-one',
    'speed-two',
    'speed-three',
    'direction',
    'speed-plus',
    'speed-minus',
    'ventilation',
    'exhaust',
  ];

  const [deleteMultimediaDevice] = useMutation(DELETE_MULTIMEDIA_DEVICE, {
    async update() {
      setSelectedDevices([]);
      createClient();
    },
  });

  useAppResumeListener(refetch);

  useEffect(() => {
    if (multimediaDeviceId) {
      setmultimediaDevice(dataButtons.deviceStates.filter(el => el.id === multimediaDeviceId)[0]);
      setModalOpen(true);
      setmultimediaDeviceId();
    }
  }, [multimediaDeviceId]);

  // If Selected Devices Mode is On, when user click on back button the app doesn't close
  useEffect(() => {
    const backAction = () => (selectedDevices.length > 0 ? setSelectedDevices([]) || true : BackHandler.exitApp());
    const backHandler = BackHandler.addEventListener('hardwareBackPress', backAction);
    return () => backHandler.remove();
  }, [selectedDevices.length > 0]);

  // TODO: Implement to all screens
  useEffect(() => {
    toggleSceneByWidget(fromWidget, dataButtons?.deviceStates, 1);
  }, [fromWidget, dataButtons, data]);

  useEffect(() => {
    handleBroadlinkError(BROADLINK_ERROR, broadlinks);
  }, [BROADLINK_ERROR]);

  useEffect(() => {
    const subscription = Dimensions.addEventListener('change');
    if (subscription != undefined) return () => subscription.remove();
  }, []);

  useEffect(() => {
    // Push Buttons on Devices Array when API returns the Buttons
    if (data) {
      const newData = { deviceStates: {} };
      newData.deviceStates = data?.deviceStates.map(value => {
        const buttons = listButtons?.data?.listButtons?.filter(button => button.multimedia_device_id === value.id);
        return { ...value, buttons };
      });
      setDataButtons(newData);
    }
  }, [data, listButtons.data]);

  useLayoutEffect(() => {
    if (selectedDevices.length === 0) setClonedData(data);
  }, [selectedDevices.length === 0, data]);

  useLayoutEffect(() => {
    const len = selectedDevices.length;
    setHeaderProps({
      headerLeft:
        len > 0 &&
        (() => (
          <IconButton onPress={() => setSelectedDevices([])} icon='arrow-left' color='white' style={styles.arrowLeft} />
        )),
      headerRight:
        len > 0
          ? () => (
              <View style={styles.right}>
                {len === 1 && (
                  <DeviceEdit
                    editMultimedia={async device => {
                      await new Promise(r => setTimeout(r, 300)); // Time to recreate buttons
                      setSelectedDevices([]);
                      navigation.navigate('DeviceForm', { device });
                    }}
                    device={selectedDevices[0]}
                    onConfirm={() => setSelectedDevices([])}
                  />
                )}
                {len === 1 && selectedDevices[0].type === 'MULTIMEDIA' && (
                  <IconButton
                    onPress={handleConfirmDeleteDevice}
                    color='white'
                    style={dimensions.width < 400 && { marginLeft: -2 }}
                    icon={props => <Icon size={26} name={'delete'} color='white' />}
                  />
                )}

                <SetDeviceListGroup
                  devices={selectedDevices}
                  onConfirm={newGroup => {
                    setSelectedDevices([]);
                    if (newGroup) setDeviceGroup(newGroup);
                  }}
                />
              </View>
            )
          : () => (
              <View style={styles.right}>
                <IconButton
                  onPress={async () => await resetMqttServices()}
                  color='white'
                  style={dimensions.width < 400 && { marginLeft: -2 }}
                  icon={props => <Icon size={26} name={'lock-reset'} color='white' />}
                />
                <DeviceEdit icon='playlist-edit' listen />
              </View>
            ),
      headerTitle: len > 0 ? `${len} lumino${len > 1 ? 's' : ''}` : 'Controllar',
      headerTitleStyle: {
        color: Colors.grey100,
        marginRight: Platform.OS === 'ios' ? (len > 1 ? 4 * 32 : len > 0 ? 5 * 32 : undefined) : undefined,
      },
    });
  }, [selectedDevices.length]);

  const refreshControl = useMemo(
    () => (
      <RefreshControl
        refreshing={loading}
        onRefresh={() => {
          createClient();
          SET_APP_NOTIFICATIONS([]);
        }}
      />
    ),
    [loading, createClient],
  );

  const getItemLayout = useCallback(
    (_, index) => ({
      length: ITEM_HEIGHT,
      offset: ITEM_HEIGHT * index,
      index,
    }),
    [],
  );

  const keyExtractor = useCallback(item => item.id.toString(), []);

  const renderItem = useCallback(
    ({ item }) => {
      if (!fanButtonDevices.some(value => item.name.includes(value)) || item.name.includes('lightbulb'))
        return (
          <DeviceItem
            device={item}
            selected={selectedDevices.includes(item)}
            select={() => setSelectedDevices([...selectedDevices, item])}
            unselect={() => setSelectedDevices(selectedDevices.filter(dev => dev !== item))}
            selectionMode={selectedDevices.length > 0}
            setDeviceState={setDeviceState}
            openModal={() => setmultimediaDeviceId(item.id)}
          />
        );
    },
    [selectedDevices, computedData, dataButtons?.deviceStates, clonedData],
  );

  const enhanceItem = item => {
    item.isConnected =
      item.type !== 'MULTIMEDIA' ||
      item?.buttons?.[0]?.emitter === 'THMedia' ||
      BROADLINKS_CONNECTEDS.some(v => {
        const extractedValue = v.split('-')[1]?.substring(1, v.split('-')[1].length - 1) ?? '';
        return extractedValue === item?.buttons?.[0]?.macAddress;
      });
    item.startFan =
      item.indicator.includes('FAN') &&
      dataButtons.deviceStates?.find(d => d.name.includes('speed-one') && d.name.includes(item.id));
    return item;
  };

  const computedData = useMemo(() => {
    if (!dataButtons || !dataButtons.deviceStates) return [];

    return sort(
      (selectedDevices.length > 0 ? clonedData : dataButtons).deviceStates.filter(getGroupFilter(deviceGroup)),
      'name',
    ).map(item => enhanceItem(item));
  }, [dataButtons, clonedData, selectedDevices, deviceGroup, dataButtons?.deviceStates, BROADLINKS_CONNECTEDS.length]);

  const listEmptyComp = computedData && (
    <Subheading style={styles.empty}>Nenhum lumino vinculado ao setor atual.</Subheading>
  );

  const controlls = createControlls(
    handleButton,
    multimediaDevice,
    () => {},
    '',
    [],
    false,
    false,
    '',
    computedData,
    false,
  );

  async function toggleSceneByWidget(fromWidget, devices, trie) {
    if (trie > 5) {
      Alert.alert(
        'Widget não executado',
        'Tente novamente.',
        [
          {
            text: 'Ok',
            onPress: () => {
              if (oldInstance) createClient(oldInstance);
              BackHandler.exitApp();
            },
          },
        ],
        {
          cancelable: false,
        },
      );
      return;
    }
    if (!fromWidget || fromWidget == 0) return;
    if (!devices || devices.length == 0) return;
    setFromWidget(0);
    const scene = await AsyncStorage.getItem(`@widget_${fromWidget}_scene`);
    if (!scene) {
      Alert.alert('Nenhuma Cena Selecionada, defina uma cena no Widget');
      return;
    }

    const sceneData = JSON.parse(scene);
    if (!sceneData.actions || sceneData.actions.length === 0) {
      Alert.alert('Cena sem Luminos, adicione-os na cena');
      return;
    }

    const offOnly = sceneData.offOnly;
    const sensors = sceneData.actions.filter(a => a.sensor);
    sensors.map(sensorDevice => {
      if (sensorDevice) {
        const newDevice = devices.find(d => d.id === sensorDevice.sensor);
        const newAction = {
          id: 3000,
          code: newDevice.id,
          intensity: newDevice.intensity,
          storeState: null,
          sensor: 0,
        };
        sceneData.actions.push(newAction);
      }
    });
    const sceneActive = offOnly
      ? sceneData.actions
          .filter(action => action.code < 1000)
          .filter(action => devices.find(d => d.id === action.code)?.type !== 'PULSE')
          .filter(action => {
            const state = devices.filter(s => s.id === action.code)[0];
            return state?.on;
          }).length > 0
      : sceneData.actions
          .filter(action => action.code < 1000)
          .filter(action => devices.find(d => d.id === action.code)?.type !== 'PULSE')
          .filter(action => {
            const state = devices.filter(s => s.id === action.code)[0];
            return action.intensity === (state?.on ? state.intensity : 0);
          }).length ===
        sceneData.actions
          .filter(action => action.code < 1000)
          .filter(action => devices.find(d => d.id === action.code)?.type !== 'PULSE').length;
    const result = await executeScene({
      variables: { id: sceneData.id, type: sceneActive ? 'OFF' : offOnly ? 'FIRST' : 'ON' },
    });
    const res = result?.data?.executeScene?.[0]?.on;
    if (res !== true && res !== false) toggleSceneByWidget(fromWidget, devices, trie + 1);
    if (oldInstance) createClient(oldInstance);
    BackHandler.exitApp();
  }
  function handleButton(button) {
    // Send Action to API send to socket
    if (!button) return;

    const INTENSITY_MAPPING = {
      'power-off': 0,
      'temp-fifteen': 15,
      'temp-nineteen': 19,
      'temp-twenty-two': 22,
      'temp-thirty': 30,
    };

    const match = button.buttonName.match(/(temp-(?:fifteen|nineteen|twenty-two|thirty))(?:-\d+)?/);
    const buttonName = match ? match[1] : button.buttonName;

    const int = multimediaDevice.indicator.includes('TV_')
      ? multimediaDevice.intensity == 50
        ? 0
        : 50
      : multimediaDevice.indicator.includes('FAN')
      ? 0
      : INTENSITY_MAPPING[buttonName] ?? 50;

    let fanButtons = [];
    let powerTemp = /power|temp/.test(buttonName);

    dataButtons.deviceStates.forEach(data => {
      if (data.id === button.multimedia_device_id && /power|temp/.test(button.buttonName)) {
        data.intensity = int;
        multimediaDevice.intensity = int;
      }

      if (fanButtonDevices.some(el => data.name.includes(el)) && data.name.includes(multimediaDevice.id)) {
        fanButtons.push(data);
      }
      return data;
    });
    multimediaDevice.buttons.forEach(butt => {
      if (button.buttonName !== butt.buttonName) return;

      const fanButton = fanButtons?.filter(d => d.name.includes(butt.buttonName))[0];
      let id, intensity;

      if (fanButton) {
        id = fanButton?.buttons[0]?.multimedia_device_id;
        intensity = butt.buttonName === 'lightbulb' ? (fanButton?.intensity === 50 ? 0 : 50) : 0;

        dataButtons.deviceStates.forEach(data => {
          if (data.id == id) {
            data.intensity = intensity;
            setmultimediaDeviceId(data.id);
            setmultimediaDeviceId(button.multimedia_device_id);
          }
        });
      } else {
        id = powerTemp ? multimediaDevice.id : button.id;
        intensity = int;
      }

      if (button.buttonName.includes('speed')) {
        setDeviceState({ variables: { id, intensity: 50 } });
        setTimeout(() => setDeviceState({ variables: { id, intensity: 0 } }), 100);
      } else {
        setDeviceState({ variables: { id, intensity } });
      }
    });
  }

  function handleConfirmDeleteDevice() {
    const d = selectedDevices[0];
    Alert.alert(
      'Deseja Deletar o Dispositivo?',
      d.name[2] === '#'
        ? d.name.substring(4)
        : d.name.includes('lightbulb')
        ? d.name.substring(0, d.name.length - 18)
        : d.name,
      [
        { text: 'Cancelar', onPress: () => setSelectedDevices([]), style: 'cancel' },
        { text: 'OK', onPress: async () => await deleteMultimediaDevice({ variables: { id: selectedDevices[0].id } }) },
      ],
    );
  }

  function handleBroadlinkError(BROADLINK_ERROR, broadlinks) {
    if (BROADLINK_ERROR) {
      const broadlink = broadlinks?.data?.broadlinks.filter(el => el?.mac === BROADLINK_ERROR);
      const name = broadlink.length > 0 ? broadlink[0].name : null;
      Toast.show({
        type: 'error',
        text1: 'O Blaster acionado não está respondendo! 🙁',
        text2: name ?? BROADLINK_ERROR,
      });
      let date = new Date();
      const hora =
        date.getHours().toString().length === 1 ? '0' + date.getHours().toString() : date.getHours().toString();
      const minutos =
        date.getMinutes().toString().length === 1 ? '0' + date.getMinutes().toString() : date.getMinutes().toString();
      const segundos =
        date.getSeconds().toString().length === 1 ? '0' + date.getSeconds().toString() : date.getSeconds().toString();

      date = date.toLocaleDateString('pt-BR') + ' às ' + hora + ':' + minutos + ':' + segundos;
      APP_NOTIFICATIONS.push({ mac: name ?? BROADLINK_ERROR, date });
      SET_APP_NOTIFICATIONS(APP_NOTIFICATIONS);
      SET_BROADLINK_ERROR('');
    }
  }

  return (
    <View>
      <Modal animationType='none' transparent={true} visible={isModalOpem} onRequestClose={() => setModalOpen(false)}>
        <View style={{ opacity: 0.4, height: dimensions.height, width: dimensions.width, position: 'absolute' }}>
          <View style={styles.containerGray} />
        </View>
        <TouchableOpacity style={{ flex: 1 }} onPress={() => setModalOpen(false)} />
        <View style={styles.modal}>{controlls[multimediaDevice.indicator]}</View>
      </Modal>
      <FlatList
        removeClippedSubviews={true}
        maxToRenderPerBatch={1}
        initialNumToRender={5}
        keyExtractor={keyExtractor}
        ListEmptyComponent={listEmptyComp}
        extraData={selectedDevices}
        getItemLayout={getItemLayout}
        refreshControl={refreshControl}
        ListFooterComponentStyle={styles.footer}
        data={computedData}
        renderItem={renderItem}
        marginBottom={25}
        windowSize={10}
      />
      <Toast />
    </View>
  );
}

const styles = StyleSheet.create({
  arrowLeft: { marginLeft: 0 },
  right: { flexDirection: 'row' },
  footer: { marginBottom: ITEM_HEIGHT },
  empty: { padding: 16 },
  containerGray: { width: '100%', backgroundColor: theme.colors.gray850, flex: 1 },
  modal: { justifyContent: 'flex-end' },
});
