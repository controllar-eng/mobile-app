import React, { useState, useContext } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { Button, Menu, Divider, TouchableRipple, Colors } from 'react-native-paper';
import ProfileDialog from './ProfileDialog';
import { PROFILES } from '../../data/gql';
import { useQuery } from '@apollo/client';
import { AppContext, DEFAULT_PROFILE } from '../../lib/context';
import { View, StyleSheet, Text } from 'react-native';

export default function ProfilePicker() {
  const { profile, setProfile } = useContext(AppContext);

  const [open, setOpen] = useState(false);

  const [dialogOpen, setDialogOpen] = useState(false);
  const [editingProfile, setEditingProfile] = useState();

  const { loading, error, data } = useQuery(PROFILES);

  return (
    <>
      <ProfileDialog
        editing={editingProfile}
        visible={dialogOpen}
        onDismiss={() => setDialogOpen(false)}
        onConfirm={value => {
          // if new OR current, replace
          if (!editingProfile || editingProfile === profile) setProfile(value);
          setDialogOpen(false);
        }}
        onDelete={() => {
          if (editingProfile.id === profile.id) setProfile(DEFAULT_PROFILE);
          setDialogOpen(false);
        }}
      />
      <Menu
        statusBarHeight={32}
        visible={open}
        onDismiss={() => setOpen(false)}
        anchor={
          <View style={styles.parentView}>
            <Button icon='account' color='white' style={styles.button} onPress={() => setOpen(true)}>
              <Text style={styles.textbutton}>
                {profile?.name} <Icon name='chevron-down' />
              </Text>
            </Button>
          </View>
        }
      >
        {data &&
          data.profiles.map(prof => (
            <TouchableRipple
              key={prof.id}
              onPress={() => {
                setOpen(false);
                setProfile(prof);
              }}
              onLongPress={() => {
                if (prof.id !== DEFAULT_PROFILE.id) {
                  setOpen(false);
                  setEditingProfile(prof);
                  setDialogOpen(true);
                }
              }}
            >
              <Menu.Item
                icon={prof.id === profile?.id ? 'account' : 'account-switch'}
                title={prof.name}
                titleStyle={prof.id === profile?.id ? { fontWeight: '700' } : undefined}
              />
            </TouchableRipple>
          ))}

        <Divider />

        <Menu.Item
          icon='account-plus'
          title='Novo Perfil...'
          onPress={() => {
            setOpen(false);
            setEditingProfile();
            setDialogOpen(true);
          }}
        />
      </Menu>
    </>
  );
}

const styles = StyleSheet.create({
  button: { borderRadius: 0, justifyContent: 'space-between' },
  parentView: { backgroundColor: Colors.blue300, elevation: 2, borderRadius: 25 },
  textbutton: { fontSize: 9 },
});
