import React, { useState, useEffect } from 'react';
import { Button, TextInput, Portal, Dialog, HelperText, IconButton, useTheme } from 'react-native-paper';
import { useMutation } from '@apollo/client';
import KeyboardAwareDialog from '../KeyboardAwareDialog';
import { CREATE_PROFILE, PROFILES, UPDATE_PROFILE, DELETE_PROFILE } from '../../data/gql';
import { Alert } from 'react-native';
import TextInputInAPortal from '../TextInputInPortal';

const CustomDialog = Platform.OS === 'ios' ? KeyboardAwareDialog : Dialog;

export default function ProfileDialog({ visible, onConfirm, onDismiss, onDelete, editing }) {
  const [name, setName] = useState();
  const [error, setError] = useState();

  const [update] = useMutation(UPDATE_PROFILE);

  const [del] = useMutation(DELETE_PROFILE, {
    update(cache, { data: { deleteProfile } }) {
      if (!deleteProfile.success) return;
      const { profiles } = cache.readQuery({ query: PROFILES });
      cache.writeQuery({
        query: PROFILES,
        data: {
          profiles: profiles.filter(profile => profile.id != editing.id),
        },
      });
    },
  });

  const [create] = useMutation(CREATE_PROFILE, {
    update(cache, { data: { createProfile } }) {
      if (!createProfile.profile) return;
      const { profiles } = cache.readQuery({ query: PROFILES });
      cache.writeQuery({
        query: PROFILES,
        data: { profiles: [...profiles, createProfile.profile] },
      });
    },
  });

  useEffect(() => setName(editing ? editing.name : ''), [editing, visible]);

  const theme = useTheme();

  return (
    <Portal>
      <CustomDialog visible={visible} onDismiss={onDismiss}>
        <Dialog.Title>{editing ? `Editar Perfil: ${editing.name}` : 'Novo Perfil'}</Dialog.Title>
        <Dialog.Content>
          <TextInputInAPortal
            autoFocus
            mode='outlined'
            placeholder='Nome'
            dense
            value={name}
            onChangeText={setName}
            error={error && !!error.name}
          />
          <HelperText type='error' visible={error && !!error.name}>
            {error && error.name}
          </HelperText>
        </Dialog.Content>
        <Dialog.Actions>
          {editing && (
            <IconButton
              color={theme.colors.error}
              icon='trash-can-outline'
              onPress={async () => {
                const { data } = await del({ variables: { id: editing.id } });
                if (data.deleteProfile.success) onDelete();
                else Alert.alert('O perfil precisa estar vazio para ser excluído.');
              }}
            />
          )}

          <Button onPress={onDismiss}>Cancelar</Button>
          <Button
            disabled={!name}
            onPress={async () => {
              if (editing) {
                const { data } = await update({
                  variables: { input: { id: editing.id, name } },
                });

                setError(data.updateProfile.error);
                if (!data.updateProfile.error) {
                  onConfirm(data.updateProfile.profile);
                }
              } else {
                const { data } = await create({
                  variables: { input: { name } },
                });

                setError(data.createProfile.error);
                if (!data.createProfile.error) {
                  onConfirm(data.createProfile.profile);
                }
              }
            }}
          >
            Confirmar
          </Button>
        </Dialog.Actions>
      </CustomDialog>
    </Portal>
  );
}
