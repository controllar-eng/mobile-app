import { useMutation } from '@apollo/client';
import React, { useEffect, useMemo, useState } from 'react';
import { Dimensions, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import ModalFilterPicker from 'react-native-modal-filter-picker';
import { Button, Dialog, Portal, RadioButton, TextInput } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import SwitchButton from 'switch-button-react-native';

import { INDICATORS } from '../components/device/DeviceItem';
import { BROADLINKS, CREATE_BROADLINK } from '../data/gql';
import { theme } from '../lib/utils';
import KeyboardAwareDialog from './KeyboardAwareDialog';
import TextInputInAPortal from './TextInputInPortal';

const isIOS = Platform.OS === 'ios';
const CustomDialog = Platform.OS === 'ios' ? KeyboardAwareDialog : Dialog;

export default function ActionsDevice({
  device,
  macArray,
  modelsKey,
  devicesDB,
  csvsDB,
  buttonRecord,
  luminosAvailable,
  broadlinks,
  controllStyle,
  isOpemControll,
  iconWindow,
  updatePicker,
  loadBroadlinks,
  setIconWindow,
  setControllStyle,
  setTextInputFocus,
  controllType,
  controllTypeChange,
  lumino,
  setLumino,
  multimediaName,
  setMultimediaName,
  emitter,
  setEmitter,
  macAddress,
  setMacAddress,
  channel,
  setChannel,
  freq,
  setFreq,
}) {
  const [broadlinkMac, setbroadlinkMac] = useState('');
  const [broadlinkValue, setbroadlinkValue] = useState('');
  const [broadlinksData, setBroadlinksData] = useState([]);
  const [broadlinkName, setBroadlinkName] = useState('');

  const [selectedModel, setSelectedModel] = useState('');
  const [selectedDevice, setSelectedDevice] = useState('');
  const [selectedStyle, setSelectedStyle] = useState('');

  const [modalPicker, setModalPicker] = useState('');
  const [advancedMode, setAdvancedMode] = useState(false);
  const [dbMode, setDbMode] = useState(false);

  const [emitterMode, setEmitterMode] = useState(false);
  const [dimensions] = useState(Dimensions.get('window'));

  useEffect(() => {
    if (broadlinks && broadlinks.data && broadlinksData.length === 0) setBroadlinksData(broadlinks.data.broadlinks);
  }, [broadlinks, broadlinks.data]);

  const typeText = {
    GATE: 'Portão',
    WINDOW: 'Janela',
    TV: 'TV',
    AIR: 'Ar Condicionado',
    FANRF: 'Ventilador - RF',
    FANIR: 'Ventilador - IR',
    RF_DEFAULT: 'RF Default',
  };
  const regexPattern = /^(CLOSE_|STOP_|OPEN_).*$/;

  const pickersCommonProps = {
    cancelButtonText: 'Cancelar',
    placeholderText: 'Digite Aqui...',
    onCancel: () => setModalPicker(''),
  };

  const [createBroadlink] = useMutation(CREATE_BROADLINK, {
    update(cache, { data: { createBroadlink } }) {
      if (!createBroadlink) return;
      const { broadlinks } = cache.readQuery({ query: BROADLINKS });
      cache.writeQuery({
        query: BROADLINKS,
        data: {
          broadlinks:
            broadlinks.filter(broadlink => broadlink.id == createBroadlink.id).length > 0
              ? broadlinks.filter(broadlink => (broadlink.id == createBroadlink.id ? createBroadlink : broadlink))
              : [...broadlinks, createBroadlink],
        },
      });
    },
  });

  const macs = useMemo(() => {
    return macArray.map(data => {
      const foundBroadlink = broadlinksData.find(({ mac }) => data.mac === mac);
      const label = foundBroadlink ? `${foundBroadlink.name} - ${data.mac}` : data.all;

      if (data.mac === macAddress) {
        setBroadlinkName(label);
      }

      return { key: data.mac, label: label };
    });
  }, [macArray, broadlinksData, macAddress]);

  const firstTextSwitch = () => (device?.buttons[0]?.emitter === 'THMedia' ? 'THMedia' : 'Blaster');
  const secondTextSwitch = () => (device?.buttons[0]?.emitter === 'THMedia' ? 'Blaster' : 'THMedia');
  const isWindowButton = s => regexPattern.test(s);
  const getEmitterCorrectName = emitter => (emitter === 'Blaster' ? 'broadlink' : emitter);

  const IconComp = name => {
    const Comp = INDICATORS[name].comp;
    return <Comp {...INDICATORS[name].on} size={24} />;
  };

  const PickerType = useMemo(() => {
    const types = Object.entries(typeText).map(([key, label]) => ({ key, label }));
    return (
      <ModalFilterPicker
        {...pickersCommonProps}
        visible={modalPicker === 'type'}
        onSelect={({ key }) => {
          if (key !== controllType) {
            setSelectedDevice('');
            setSelectedModel('');
            setSelectedStyle('');
            controllTypeChange(key);
            setDbMode(false);
            setAdvancedMode(false);
            setEmitterMode(false);
          }
          setModalPicker('');
        }}
        options={types}
      />
    );
  }, [modalPicker, controllType]);

  const PickerModel = useMemo(
    () => (
      <ModalFilterPicker
        {...pickersCommonProps}
        visible={modalPicker === 'model'}
        onSelect={val => {
          setSelectedDevice('');
          setSelectedStyle('');
          setSelectedModel(val.label);
          updatePicker({ model: val.label, device: '', style: '' });
          setModalPicker('');
        }}
        options={modelsKey}
      />
    ),
    [modalPicker, modelsKey.length],
  );

  const PickerDevice = useMemo(() => {
    const devices = [];
    selectedModel !== '' &&
      modelsKey !== null &&
      modelsKey !== undefined &&
      modelsKey.map(
        ({ label }) =>
          label === selectedModel && devicesDB.map(data => devices.push({ key: data.trim(), label: data })),
      );

    return (
      <ModalFilterPicker
        {...pickersCommonProps}
        visible={modalPicker === 'device'}
        onSelect={({ key }) => {
          if (key !== selectedDevice) {
            setSelectedStyle('');
            updatePicker({ model: selectedModel, device: key, style: '' });
            setSelectedDevice(key);
          }
          setModalPicker('');
        }}
        options={devices}
      />
    );
  }, [modalPicker, selectedDevice, selectedModel, devicesDB.length, modelsKey.modelsKey]);

  const PickerStyle = useMemo(() => {
    const styles = [];
    selectedModel !== '' &&
      selectedDevice !== '' &&
      modelsKey.map(
        ({ label }) =>
          label === selectedModel &&
          devicesDB.map(
            data => data === selectedDevice && csvsDB.map(data => styles.push({ key: data.trim(), label: data })),
          ),
      );

    return (
      <ModalFilterPicker
        {...pickersCommonProps}
        visible={modalPicker === 'style'}
        onSelect={({ key }) => {
          if (key !== selectedStyle) {
            setSelectedStyle(key);
            updatePicker({ model: selectedModel, device: selectedDevice, style: key });
          }
          setModalPicker('');
        }}
        options={styles}
      />
    );
  }, [modalPicker, selectedStyle, selectedModel, selectedDevice, csvsDB.length, modelsKey.length, devicesDB.length]);

  const PickerLuminos = useMemo(() => {
    const luminos = [];
    Array.from({ length: 200 }, (_, i) => i + 600).map(
      number => luminosAvailable.includes(number) && luminos.push({ key: number, label: number }),
    );
    return (
      <ModalFilterPicker
        {...pickersCommonProps}
        visible={modalPicker === 'lumino'}
        onSelect={({ key }) => {
          if (key !== lumino) {
            setLumino(key);
          }
          setModalPicker('');
        }}
        options={luminos}
      />
    );
  }, [modalPicker, lumino, luminosAvailable.length]);

  const PickerBroadlinks = useMemo(
    () => (
      <ModalFilterPicker
        {...pickersCommonProps}
        visible={modalPicker === 'mac'}
        onSelect={({ key }) => {
          if (key !== macAddress)
            setMacAddress({
              value: key,
              dataValue: { device: selectedDevice, model: selectedModel, style: selectedStyle },
              updateType: 'mac',
            });
          setModalPicker('');
        }}
        options={macs}
      />
    ),
    [modalPicker, macAddress, selectedDevice, selectedModel, selectedStyle, macs.length, macs],
  );

  const PickerEditBroadlinks = useMemo(
    () => (
      <ModalFilterPicker
        {...pickersCommonProps}
        visible={modalPicker === 'edit'}
        onSelect={({ key }) => {
          if (key !== broadlinkMac) setbroadlinkMac(key);
          setModalPicker('');
        }}
        options={macs}
      />
    ),
    [modalPicker, broadlinkMac, macs.length],
  );

  const PickerChannel = useMemo(() => {
    const channels = [];
    channels.push({ key: 0, label: 'Gravação de sinal RF' });
    Array.from({ length: 8 }, (_, i) => i + 1).map(number => {
      channels.push({ key: number, label: number });
    });
    return (
      <ModalFilterPicker
        {...pickersCommonProps}
        visible={modalPicker === 'channel'}
        onSelect={({ key }) => {
          if (key !== channel)
            setChannel({
              value: key == 0 ? null : key,
              dataValue: { device: selectedDevice, model: selectedModel, style: selectedStyle },
              updateType: 'channel',
            });
          setModalPicker('');
        }}
        options={channels}
      />
    );
  }, [modalPicker, channel, selectedDevice, selectedModel, selectedStyle]);

  const PickerControllStyles = useMemo(() => {
    const array = length => Array.from({ length }, (_, i) => ({ key: String(i + 1), label: `Estilo ${i + 1}` }));
    const controllStyles = controllType.includes('FAN')
      ? array(2)
      : controllType.includes('AIR')
      ? [{ key: '', label: 'Sem estilização' }, ...array(1)]
      : [{ key: '', label: 'Sem estilização' }, ...array(4)];

    return (
      <ModalFilterPicker
        {...pickersCommonProps}
        visible={modalPicker === 'controllStyle'}
        onSelect={({ key }) => {
          if (key !== controllStyle) {
            setControllStyle(key);
          }
          setModalPicker('');
        }}
        options={controllStyles}
      />
    );
  }, [modalPicker, controllStyle, controllType]);

  const RadioButtons = useMemo(
    () =>
      Array.from({ length: 3 }, (_, i) => i + 1).map(number => (
        <>
          <RadioButton
            status={freq === (number + 37) * 1000 ? 'checked' : 'unchecked'}
            onPress={() =>
              setFreq({
                value: (number + 37) * 1000,
                dataValue: { device: selectedDevice, model: selectedModel, style: selectedStyle },
                updateType: 'freq',
              })
            }
          />
          <Text>{number + 37}KHz</Text>
        </>
      )),
    [freq, selectedDevice, selectedModel, selectedStyle],
  );

  async function handleEditBroadlink() {
    if (broadlinkValue) {
      let _id = null;
      broadlinksData.map(({ mac, id }) => {
        if (mac === broadlinkMac && id !== undefined) _id = id;
      });
      const result = await createBroadlink({
        variables: {
          id: _id,
          mac: broadlinkMac,
          name: broadlinkValue,
        },
      });
      broadlinksData.map(({ mac, id }) => {
        if (mac === broadlinkMac && id !== undefined) _id = id;
      });
      result &&
        result.data != undefined &&
        broadlinksData.filter(data => {
          if (data.mac !== broadlinkMac) {
            return data;
          }
        }) &&
        setBroadlinksData([
          ...broadlinksData,
          {
            id: result.data.createBroadlink.id,
            mac: broadlinkMac,
            name: broadlinkValue,
          },
        ]);
      setbroadlinkMac('');
      setbroadlinkValue('');
    }
  }

  return (
    <View>
      <View style={[{ width: '100%', paddingHorizontal: 3 }, !isIOS && { marginTop: -10 }]}>
        <TextInput
          label='Digite o Nome do Dispositivo...'
          style={[
            styles.textInput,
            dimensions.height < 720 && { height: 45 },
            buttonRecord !== '' && { display: 'none' },
          ]}
          value={multimediaName}
          onChangeText={setMultimediaName}
          onFocus={() => setTextInputFocus(true)}
          onBlur={() => setTextInputFocus(false)}
        />
      </View>
      <View style={(buttonRecord !== '' || isOpemControll) && { display: 'none' }}>
        <View
          style={[device && { display: 'none' }, styles.containerPicker, { height: dimensions.height < 720 ? 40 : 50 }]}
        >
          <TouchableOpacity style={styles.modalPicker} onPress={() => setModalPicker('type')}>
            <Text style={[{ fontSize: 16 }, { color: controllType != '' ? theme.colors.black : theme.colors.gray700 }]}>
              {controllType ? typeText[controllType] : 'Selecione o Tipo de Controle'}
            </Text>
            <Icon name='chevron-down' size={21} />
            {PickerType}
          </TouchableOpacity>
        </View>
        <View
          style={[
            styles.containerPicker,
            (controllType !== 'TV' || isOpemControll) && { display: 'none' },
            { height: dimensions.height < 720 ? 40 : 50 },
          ]}
        >
          <TouchableOpacity
            onPress={() => {
              setEmitterMode(false);
              setAdvancedMode(false);
              setDbMode(!dbMode);
            }}
            style={styles.modalPicker}
          >
            <Text style={[{ fontSize: 16 }, { color: dbMode ? theme.colors.blue500 : theme.colors.gray800 }]}>
              Banco de Dados
            </Text>
            <Icon name={!dbMode ? 'chevron-down' : 'chevron-up'} size={21} />
          </TouchableOpacity>
        </View>
        <View style={(!(dbMode && controllType === 'TV') || isOpemControll) && { display: 'none' }}>
          <View style={[styles.containerPicker, { height: dimensions.height < 720 ? 40 : 50 }]}>
            <TouchableOpacity style={styles.modalPicker} onPress={() => setModalPicker('model')}>
              <Text
                style={[
                  styles.textInsideContainer,
                  { color: selectedModel != '' ? theme.colors.black : theme.colors.gray700 },
                ]}
              >
                {selectedModel != '' ? selectedModel : 'Selecione o Modelo'}
              </Text>
              <Icon name='chevron-down' size={21} />
              {PickerModel}
            </TouchableOpacity>
          </View>
          <View style={[styles.containerPicker, { height: dimensions.height < 720 ? 40 : 50 }]}>
            <TouchableOpacity style={styles.modalPicker} onPress={() => setModalPicker('device')}>
              <Text
                style={[
                  styles.textInsideContainer,
                  { color: selectedDevice ? theme.colors.black : theme.colors.gray700 },
                ]}
              >
                {selectedDevice ? selectedDevice : 'Selecione a Função'}
              </Text>
              <Icon name='chevron-down' size={21} />
              {PickerDevice}
            </TouchableOpacity>
          </View>
          <View style={[styles.containerPicker, { height: dimensions.height < 720 ? 40 : 50 }]}>
            <TouchableOpacity style={styles.modalPicker} onPress={() => setModalPicker('style')}>
              <Text
                style={[
                  styles.textInsideContainer,
                  { color: selectedStyle ? theme.colors.black : theme.colors.gray700 },
                ]}
              >
                {selectedStyle ? selectedStyle : 'Selecione o Grupo'}
              </Text>
              <Icon name='chevron-down' size={21} />
              {PickerStyle}
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={[
            isOpemControll && { display: 'none' },
            styles.containerPicker,
            { height: dimensions.height < 720 ? 40 : 50 },
          ]}
        >
          <TouchableOpacity
            onPress={() => {
              setAdvancedMode(!advancedMode);
              setDbMode(false);
              setEmitterMode(false);
            }}
            style={styles.modalPicker}
          >
            <Text style={[{ fontSize: 16 }, { color: advancedMode ? theme.colors.blue500 : theme.colors.gray800 }]}>
              Modo Avancado
            </Text>
            <Icon name={!advancedMode ? 'chevron-down' : 'chevron-up'} size={21} />
          </TouchableOpacity>
        </View>
        <View style={(!advancedMode || isOpemControll) && { display: 'none' }}>
          <View style={[styles.containerPicker, styles.freqContainer]}>
            <Text>Frequencia</Text>
            {RadioButtons}
          </View>
          <View
            style={[
              styles.containerPicker,
              controllType === 'WINDOW' && { display: 'none' },
              { height: dimensions.height < 720 ? 40 : 50 },
            ]}
          >
            <TouchableOpacity style={styles.modalPicker} onPress={() => !device && setModalPicker('lumino')}>
              <Text style={[styles.textInsideContainer, { color: lumino ? theme.colors.black : theme.colors.gray700 }]}>
                {lumino ? `Lumino ~  ${lumino}` : device ? `Lumino ~  ${device.id}` : 'Def. Lumino'}
              </Text>
              <Icon name='chevron-down' size={21} color={device ? theme.colors.gray700 : theme.colors.black} />
              {PickerLuminos}
            </TouchableOpacity>
          </View>
          <Text
            style={[
              styles.iconTitle,
              controllType !== 'WINDOW' && !isWindowButton(controllType) && { display: 'none' },
            ]}
          >
            Icone:
          </Text>
          <View
            style={[
              styles.containerPicker,
              controllType !== 'WINDOW' && !isWindowButton(controllType) && { display: 'none' },
              { height: dimensions.height < 720 ? 40 : 50 },
            ]}
          >
            <View style={styles.iconsContainer}>
              <TouchableOpacity
                onPress={() => setIconWindow(0)}
                style={[styles.modalPicker, styles.windowIcons, iconWindow === 0 && styles.buttonIcon]}
              >
                {IconComp('OPEN_CURTAIN')}
                {IconComp('STOP_CURTAIN')}
                {IconComp('CLOSE_CURTAIN')}
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setIconWindow(1)}
                style={[styles.modalPicker, styles.windowIcons, iconWindow === 1 && styles.buttonIcon]}
              >
                {IconComp('OPEN_SHUTTER1')}
                {IconComp('STOP_SHUTTER1')}
                {IconComp('CLOSE_SHUTTER1')}
              </TouchableOpacity>
              <TouchableOpacity
                onPress={() => setIconWindow(2)}
                style={[styles.modalPicker, styles.windowIcons, iconWindow === 2 && styles.buttonIcon]}
              >
                {IconComp('OPEN_SHUTTER2')}
                {IconComp('STOP_SHUTTER2')}
                {IconComp('CLOSE_SHUTTER2')}
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View
          style={[
            isOpemControll && { display: 'none' },
            styles.containerPicker,
            { height: dimensions.height < 720 ? 40 : 50 },
          ]}
        >
          <TouchableOpacity
            onPress={() => {
              setAdvancedMode(false);
              setDbMode(false);
              setEmitterMode(!emitterMode);
            }}
            style={styles.modalPicker}
          >
            <Text style={[{ color: emitterMode ? theme.colors.blue500 : theme.colors.gray800, fontSize: 16 }]}>
              Tipo de Emissor
            </Text>
            <Icon name={!emitterMode ? 'chevron-down' : 'chevron-up'} size={21} />
          </TouchableOpacity>
        </View>
        <View style={[styles.emitterContainer, (!emitterMode || isOpemControll) && { display: 'none' }]}>
          <SwitchButton
            onValueChange={val => {
              setEmitter({
                value: val === 1 ? getEmitterCorrectName(firstTextSwitch()) : getEmitterCorrectName(secondTextSwitch()),
                dataValue: { device: selectedDevice, model: selectedModel, style: selectedStyle },
                updateType: 'emitter',
              });
            }}
            switchdirection='rtl'
            text1={firstTextSwitch()}
            text2={secondTextSwitch()}
            switchWidth={140}
            switchHeight={35}
            btnBackgroundColor={theme.colors.blue500}
            fontColor={theme.colors.gray600}
            activeFontColor={theme.colors.white}
          />
          <View style={[styles.containerPicker, styles.modalPicker, { height: dimensions.height < 720 ? 40 : 50 }]}>
            <View style={{ width: '80%', justifyContent: 'center' }}>
              <TouchableOpacity onPress={() => setModalPicker('mac')}>
                <Text
                  style={[
                    styles.textInsideContainer,
                    { color: macAddress ? theme.colors.black : theme.colors.gray700 },
                  ]}
                >
                  {macAddress
                    ? broadlinkName
                      ? broadlinkName
                      : 'Blaster desconectado...'
                    : `Def. ${emitter === 'broadlink' ? 'Blaster' : 'THMedia'}`}
                </Text>
                {PickerBroadlinks}
              </TouchableOpacity>
            </View>
            <View style={[styles.macIcons, { paddingRight: 10 }]}>
              <TouchableOpacity onPress={() => setModalPicker('edit')}>
                <Text style={{ fontSize: 16 }}>{'✏️'}</Text>
                {PickerEditBroadlinks}
              </TouchableOpacity>
              <Portal>
                <CustomDialog visible={broadlinkMac !== ''} onDismiss={() => setbroadlinkMac('')}>
                  <Dialog.Title>Editar Broadlink: {broadlinkMac}</Dialog.Title>
                  <Dialog.Content>
                    <View>
                      <TextInputInAPortal
                        style={{ flexGrow: 1 }}
                        autoFocus
                        mode='outlined'
                        placeholder='Nome Broadlink'
                        value={broadlinkValue}
                        onChangeText={setbroadlinkValue}
                      />
                    </View>
                  </Dialog.Content>
                  <Dialog.Actions>
                    <Button onPress={() => setbroadlinkMac('')}>Fechar</Button>
                    <Button onPress={handleEditBroadlink}>Confirmar</Button>
                  </Dialog.Actions>
                </CustomDialog>
              </Portal>
            </View>
            <View style={styles.macIcons}>
              <TouchableOpacity onPress={() => loadBroadlinks()}>
                <Icon size={dimensions.height < 720 ? 24 : 30} name='refresh' color='black' />
              </TouchableOpacity>
            </View>
          </View>
          <View
            style={[
              styles.containerPicker,
              emitter !== 'THMedia' && { display: 'none' },
              { height: dimensions.height < 720 ? 40 : 50 },
            ]}
          >
            <TouchableOpacity style={styles.modalPicker} onPress={() => setModalPicker('channel')}>
              <Text
                style={[
                  styles.textInsideContainer,
                  { color: channel ? theme.colors.black : channel === 0 ? theme.colors.black : theme.colors.gray700 },
                ]}
              >
                {channel ? 'Canal ' + channel : channel === 0 ? 'Gravação de sinal RF' : 'Selecione o Canal'}
              </Text>
              <Icon name='chevron-down' size={21} />
              {PickerChannel}
            </TouchableOpacity>
          </View>
        </View>
        <View
          style={[
            styles.containerPicker,
            ((!controllType.includes('TV') && !controllType.includes('FAN') && !controllType.includes('AIR')) ||
              isOpemControll ||
              (controllType.includes('FAN') && device)) && { display: 'none' },
            { height: dimensions.height < 720 ? 40 : 50 },
          ]}
        >
          <TouchableOpacity style={styles.modalPicker} onPress={() => setModalPicker('controllStyle')}>
            <Text style={[{ fontSize: 16 }, { color: controllStyle ? theme.colors.black : theme.colors.gray700 }]}>
              {controllStyle ? 'Estilo ' + controllStyle : 'Estilo de Controle'}
            </Text>
            {PickerControllStyles}
            <Icon name='chevron-down' size={21} />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  containerPicker: { borderBottomWidth: 1, borderColor: theme.colors.gray200, justifyContent: 'center', width: '100%' },
  modalPicker: {
    paddingHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  freqContainer: { height: 40, flexDirection: 'row', alignItems: 'center', paddingHorizontal: 15 },
  pickerMac: { flexDirection: 'row', paddingRight: 15 },
  macIcons: { width: '10%', justifyContent: 'center', alignItems: 'flex-end' },
  windowIcons: { borderWidth: 1, width: '30%' },
  iconTitle: { fontSize: 17, marginLeft: 15, color: theme.colors.gray700, marginTop: 5 },
  iconsContainer: { flexDirection: 'row', justifyContent: 'space-between' },
  buttonIcon: { backgroundColor: theme.colors.gray400 },
  textInput: { fontSize: 16, backgroundColor: theme.colors.gray100 },
  emitterContainer: { alignItems: 'center', marginTop: 2 },
  textInsideContainer: { fontSize: 16, marginLeft: 15 },
});
