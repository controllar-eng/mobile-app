import React, { useState, useContext } from 'react';
import Icon from 'react-native-vector-icons/Ionicons';
import { Button, Menu, TouchableRipple, Colors, Divider } from 'react-native-paper';
import DeviceGroupDialog from './DeviceGroupDialog';
import { DEVICE_GROUPS } from '../../data/gql';
import { useQuery } from '@apollo/client';
import { sort } from '../../lib/utils';
import { Keyboard, StyleSheet, View, Text } from 'react-native';

export const ALL = { id: 'ALL', name: 'Todos os Luminos' };
const ON = { id: 'ON', name: 'Ligados', icon: 'lightbulb-on-outline' };
const MULTIMEDIA = { id: 'MULTIMEDIA', name: 'Multimidia', icon: 'television' };

const WindowIcons = ['CLOSE', 'OPEN', 'STOP'];

export const getGroupFilter = deviceGroup => dev => {
  if (deviceGroup?.id === ALL.id) return true;
  if (deviceGroup?.id === ON.id) return dev.on && !WindowIcons.includes(dev.indicator.split('_')[0]);
  if (deviceGroup?.id === MULTIMEDIA.id) return dev.type == 'MULTIMEDIA';
  else return deviceGroup?.id === dev.group_id;
};

export default function DeviceGroupPicker({ deviceGroup, setDeviceGroup, oldVersion, small }) {
  const [open, setOpen] = useState(false);

  const [dialogOpen, setDialogOpen] = useState(false);
  const [editingGroup, setEditingGroup] = useState();

  const { loading, error, data } = useQuery(DEVICE_GROUPS);
  return (
    <>
      <DeviceGroupDialog
        editing={editingGroup}
        visible={dialogOpen}
        onDismiss={() => setDialogOpen(false)}
        onConfirm={value => {
          // if current, replace
          if (editingGroup.id === deviceGroup.id) setDeviceGroup(value);
          setDialogOpen(false);
        }}
        onDelete={() => {
          if (editingGroup.id === deviceGroup.id) setDeviceGroup(null);
          setDialogOpen(false);
        }}
      />
      <Menu
        statusBarHeight={32}
        style={styles.menu}
        visible={open}
        onDismiss={() => setOpen(false)}
        anchor={
          <View style={[styles.parentView, !small ? styles.smallParentView : styles.bigParentView]}>
            <Button
              icon={deviceGroup?.icon || 'table-column'}
              color={!small ? 'white' : Colors.blue300}
              onPress={() => {
                Keyboard.dismiss();
                setOpen(true);
              }}
              style={styles.button}
            >
              <Text style={styles.textbutton}>
                {deviceGroup?.name.length > 10 ? deviceGroup?.name.substring(0, 10) : deviceGroup?.name}
              </Text>
              <Icon name='chevron-down' />
            </Button>
          </View>
        }
      >
        <Menu.Item
          key='all-devices'
          onPress={() => {
            setOpen(false);
            setDeviceGroup(ALL);
          }}
          icon={deviceGroup?.id === ALL.id ? 'table-column' : 'swap-horizontal'}
          title='Todos os Luminos'
          titleStyle={deviceGroup?.id === ALL.id ? { fontWeight: '700' } : undefined}
        />
        <Menu.Item
          key='on-devices'
          onPress={() => {
            setOpen(false);
            setDeviceGroup(ON);
          }}
          icon={deviceGroup?.id === ON.id ? 'lightbulb-on' : 'lightbulb-on-outline'}
          title='Ligados'
          titleStyle={deviceGroup?.id === ON.id ? { fontWeight: '700' } : undefined}
        />
        {
          // Multimedia Device Filter Menu Item
          !oldVersion && (
            <Menu.Item
              key='multimedia-devices'
              onPress={() => {
                setOpen(false);
                setDeviceGroup(MULTIMEDIA);
              }}
              icon={deviceGroup?.id === MULTIMEDIA.id ? 'television' : 'television'}
              title='Multimidia'
              titleStyle={deviceGroup?.id === MULTIMEDIA.id ? { fontWeight: '700' } : undefined}
            />
          )
        }

        <Divider />

        {data &&
          sort(data.deviceGroups, 'name').map(item => (
            <TouchableRipple
              key={item.id}
              onPress={() => {
                setOpen(false);
                setDeviceGroup(item);
              }}
              onLongPress={() => {
                if (item !== null) {
                  setOpen(false);
                  setEditingGroup(item);
                  setDialogOpen(true);
                }
              }}
            >
              <Menu.Item
                icon={deviceGroup?.id === item.id ? 'table-column' : 'swap-horizontal'}
                title={item.name}
                titleStyle={deviceGroup?.id === item.id ? { fontWeight: '700' } : undefined}
              />
            </TouchableRipple>
          ))}
      </Menu>
    </>
  );
}

const styles = StyleSheet.create({
  button: { justifyContent: 'space-between' },
  menu: { paddingLeft: '28%' },
  parentView: { elevation: 2, borderRadius: 25 },
  textbutton: { fontSize: 9 },
  smallParentView: { backgroundColor: Colors.blue300 },
  bigParentView: { borderWidth: 1, borderColor: Colors.blue300, backgroundColor: Colors.white, marginTop: 4 },
});
