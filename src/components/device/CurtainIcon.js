import React from 'react';
import { View } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

export default function CurtainIcon(props) {
  const { name, size, ...values } = props;

  let newName1 = name === 'open' ? 'ios-caret-back-outline' : 'ios-caret-forward-outline';
  let newName2 = name === 'close' ? 'ios-caret-back-outline' : 'ios-caret-forward-outline';

  if (name !== 'open' && name != 'close') return <Icon name={name} {...values} />;
  return (
    <View style={{ flexDirection: 'row', padding: 0 }}>
      <Icon name={newName1} size={size - 6} {...values} style />
      <Icon name={newName2} size={size - 6} {...values} />
    </View>
  );
}
