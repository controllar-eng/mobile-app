import React, { useState, useEffect, useContext } from 'react';
import { Button, TextInput, Portal, Dialog, HelperText, IconButton, useTheme } from 'react-native-paper';
import { useMutation } from '@apollo/client';
import KeyboardAwareDialog from '../KeyboardAwareDialog';
import {
  CREATE_DEVICE_GROUP,
  DEVICE_GROUPS,
  UPDATE_DEVICE_GROUP,
  DELETE_DEVICE_GROUP,
  DEVICE_STATES,
} from '../../data/gql';
import { Alert } from 'react-native';
import { AppContext } from '../../lib/context';
import TextInputInAPortal from '../TextInputInPortal';

const CustomDialog = Platform.OS === 'ios' ? KeyboardAwareDialog : Dialog;

export default function DeviceGroupDialog({ visible, onConfirm, onDismiss, onDelete, editing, devices }) {
  const [name, setName] = useState();
  const [error, setError] = useState();

  const { client } = useContext(AppContext);

  const [update] = useMutation(UPDATE_DEVICE_GROUP, { client });

  const [del] = useMutation(DELETE_DEVICE_GROUP, {
    client,
    update(cache, { data: { deleteDeviceGroup } }) {
      if (!deleteDeviceGroup.success) return;
      const { deviceGroups } = cache.readQuery({ query: DEVICE_GROUPS });
      cache.writeQuery({
        query: DEVICE_GROUPS,
        data: {
          deviceGroups: deviceGroups.filter(deviceGroup => deviceGroup.id != editing.id),
        },
      });
    },
  });

  const [create] = useMutation(CREATE_DEVICE_GROUP, {
    client,
    update(cache, { data: { createDeviceGroup } }) {
      if (!createDeviceGroup.deviceGroup) return;
      const { deviceGroups } = cache.readQuery({ query: DEVICE_GROUPS });
      cache.writeQuery({
        query: DEVICE_GROUPS,
        data: {
          deviceGroups: [...deviceGroups, createDeviceGroup.deviceGroup],
        },
      });

      const { deviceStates } = cache.readQuery({ query: DEVICE_STATES });
      const deviceIds = devices.map(dev => dev.id);

      cache.writeQuery({
        query: DEVICE_STATES,
        data: {
          deviceStates: deviceStates.map(device =>
            deviceIds.includes(device.id)
              ? {
                  ...device,
                  // eslint-disable-next-line camelcase
                  group_id: createDeviceGroup.deviceGroup.id,
                }
              : device,
          ),
        },
      });
    },
  });

  useEffect(() => setName(editing ? editing.name : ''), [editing, visible]);

  const theme = useTheme();

  return (
    <Portal>
      <CustomDialog visible={visible} onDismiss={onDismiss}>
        <Dialog.Title>{editing ? `Editar Setor: ${editing.name}` : 'Novo Setor'}</Dialog.Title>
        <Dialog.Content>
          <TextInputInAPortal
            autoFocus
            mode='outlined'
            placeholder='Nome'
            dense
            value={name}
            onChangeText={setName}
            error={error && !!error.name}
          />
          <HelperText type='error' visible={error && !!error.name}>
            {error && error.name}
          </HelperText>
        </Dialog.Content>
        <Dialog.Actions>
          {editing && (
            <IconButton
              color={theme.colors.error}
              icon='trash-can-outline'
              onPress={async () => {
                const { data } = await del({ variables: { id: editing.id } });
                if (data.deleteDeviceGroup.success) onDelete();
                else Alert.alert('O setor precisa estar vazio para ser excluído.');
              }}
            />
          )}

          <Button onPress={onDismiss}>Cancelar</Button>
          <Button
            disabled={!name}
            onPress={async () => {
              if (editing) {
                const { data } = await update({
                  variables: { input: { id: editing.id, name } },
                });

                setError(data.updateDeviceGroup.error);
                if (!data.updateDeviceGroup.error) {
                  onConfirm(data.updateDeviceGroup.deviceGroup);
                }
              } else {
                const { data } = await create({
                  variables: {
                    input: { name, deviceIds: devices.map(dev => dev.id) },
                  },
                });

                setError(data.createDeviceGroup.error);
                if (!data.createDeviceGroup.error) {
                  onConfirm(data.createDeviceGroup.deviceGroup);
                }
              }
            }}
          >
            Confirmar
          </Button>
        </Dialog.Actions>
      </CustomDialog>
    </Portal>
  );
}
