import React, { memo, useCallback, useMemo } from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { List, Subheading, useTheme, Caption, Colors } from 'react-native-paper';
import { DEVICE_TYPE } from '../../data/gql';
import DeviceSlider from './DeviceSlider';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { theme as _theme } from '../../lib/utils';
import CurtainIcon from './CurtainIcon';

const createIndicator = (name1, name2, colorOn, colorOff, size) => ({
  comp: name1 === 'open' || name1 === 'close' ? CurtainIcon : Icon,
  on: { name: name1, color: colorOn, size: size ?? 24 },
  off: { name: name2, color: colorOff, size: size ?? 24 },
});

export const INDICATORS = {
  LIGHTBULB: createIndicator('lightbulb-on', 'lightbulb-outline', Colors.yellow600, Colors.blue100),
  ENGINE: createIndicator('engine', 'engine-outline', Colors.red500, Colors.red100),
  PLUG: createIndicator('power-plug', 'power-plug-outline', Colors.red500, Colors.red100),
  CIRCLE_RED: createIndicator('circle', 'circle-outline', Colors.red400, Colors.red100, 16),
  CIRCLE_YELLOW: createIndicator('circle', 'circle-outline', Colors.yellow600, Colors.yellow500, 16),
  UP: createIndicator('arrow-up-bold', 'arrow-up-bold', Colors.blue500, Colors.blue100),
  STOP: createIndicator('stop', 'stop', Colors.blue500, Colors.blue100),
  DOWN: createIndicator('arrow-down-bold', 'arrow-down-bold', Colors.blue500, Colors.blue100),
  SNOWFLAKE: createIndicator('snowflake', 'snowflake', Colors.blue500, Colors.blue100),
  OTHER: createIndicator('domino-mask', 'domino-mask', Colors.blue500, Colors.blue100),
  TV: createIndicator('television', 'television', Colors.blue500, Colors.blue100),
  AIR: createIndicator('air-conditioner', 'air-conditioner', Colors.blue500, Colors.blue100),
  WINDOW: createIndicator('solar-panel', 'solar-panel', Colors.blue500, Colors.blue100),
  GATE: createIndicator('gate', 'gate', Colors.blue500, Colors.blue100),
  OPEN_CURTAIN: createIndicator('open', 'open', Colors.blue500, Colors.blue100),
  CLOSE_CURTAIN: createIndicator('close', 'close', Colors.blue500, Colors.blue100),
  STOP_CURTAIN: createIndicator('stop-circle-outline', 'stop-circle-outline', Colors.blue500, Colors.blue100),
  OPEN_SHUTTER1: createIndicator('arrow-up-drop-circle', 'arrow-up-drop-circle', Colors.blue500, Colors.blue100),
  CLOSE_SHUTTER1: createIndicator('arrow-down-drop-circle', 'arrow-down-drop-circle', Colors.blue500, Colors.blue100),
  STOP_SHUTTER1: createIndicator('stop-circle-outline', 'stop-circle-outline', Colors.blue500, Colors.blue100),
  OPEN_SHUTTER2: createIndicator('chevron-double-up', 'chevron-double-up', Colors.blue500, Colors.blue100),
  CLOSE_SHUTTER2: createIndicator('chevron-double-down', 'chevron-double-down', Colors.blue500, Colors.blue100),
  STOP_SHUTTER2: createIndicator('stop-circle-outline', 'stop-circle-outline', Colors.blue500, Colors.blue100),
  TV_: createIndicator('television', 'television', Colors.blue500, Colors.blue100),
  FAN: createIndicator('fan', 'fan', Colors.blue500, Colors.blue100),
  TIMER: createIndicator('clock-time-three-outline', 'clock-time-three-outline', Colors.black500, Colors.black100),
  RF_DEFAULT: createIndicator('waveform', 'waveform', Colors.blue500, Colors.blue100),
  AIR_: createIndicator('air-conditioner', 'air-conditioner', Colors.blue500, Colors.blue100),
};

const regexPattern = /^(GATE|RF_DEFAULT|CLOSE_|STOP_|OPEN_).*$/;

// There's a device TYPE.SENSOR, but Controllar wants this user-changeable
export function isSensor(device) {
  return ['CIRCLE_RED', 'CIRCLE_YELLOW'].includes(device.indicator);
}

const LeftIcon = React.memo(props => {
  const on = props.device.type !== DEVICE_TYPE.PULSE && props.device.on;
  const offOnly = regexPattern.test(props.device.indicator);
  const i = props.device.indicator;
  const indicator = i.includes('TV_') ? 'TV_' : i.includes('AIR_') ? 'AIR_' : i.includes('FAN') ? 'FAN' : i;
  const IconComp = INDICATORS[indicator].comp;

  return (
    <List.Icon
      icon={() => (
        <View style={{ flexDirection: 'row' }}>
          <IconComp {...props} {...INDICATORS[indicator][!on || offOnly ? 'off' : 'on']} />
          {!props.device.isConnected && !props.selectionMode && <Icon name='wifi-off' size={16} color={'red'} />}
        </View>
      )}
      {...props}
    />
  );
});

function DeviceItem({ device, selected, select, unselect, selectionMode, setDeviceState, openModal }) {
  const theme = useTheme();

  const handlePress = useCallback(async () => {
    if (selected) {
      unselect(device);
    } else if (selectionMode) {
      select(device);
    } else if (!isSensor(device)) {
      let intensity;

      if (device.type === DEVICE_TYPE.PULSE) intensity = 50;
      else if (device.on) intensity = 0;
      else if (device.type === DEVICE_TYPE.DIMMER) intensity = device.intensity;
      else if (regexPattern.test(device.indicator)) intensity = 0;
      else intensity = 50;

      if (intensity === 50 && device.startFan) {
        setDeviceState({ variables: { id: device.startFan.id, intensity: 50 } });
        await new Promise(r => setTimeout(r, 100));
        setDeviceState({ variables: { id: device.startFan.id, intensity: 0 } });
      } else {
        setDeviceState({ variables: { id: device.id, intensity } });
        if (device.type === DEVICE_TYPE.PULSE) {
          await new Promise(r => setTimeout(r, 100));
          setDeviceState({ variables: { id: device.id, intensity: 0 } });
        }
      }
    }
  }, [selected, selectionMode, device.on, device.intensity, device.indicator, select, unselect]);

  const handleLongPress = useCallback(() => {
    if (!selectionMode) select(device);
  }, [selectionMode, select]);

  const on = device.type !== DEVICE_TYPE.PULSE && device.on;
  const n = device.name;

  const subheading = useMemo(
    () => (
      <Subheading style={regexPattern.test(device.indicator) || !on ? styles.off : undefined}>
        {n[3] === '#' ? n.substring(5) : n.includes('lightbulb') ? n.substring(0, n.length - 18) : n}
      </Subheading>
    ),
    [device.name, on],
  );

  const left = useCallback(
    props => <LeftIcon {...props} device={device} selectionMode={selectionMode} />,
    [on, device.indicator, device.isConnected, selectionMode],
  );

  const right = useCallback(
    () => (
      <View style={styles.right}>
        {device.type === DEVICE_TYPE.DIMMER && <Caption>{device.intensity * 2}%</Caption>}
        {selected ? (
          <List.Icon icon='check' style={styles.icon} color={theme.colors.primary} />
        ) : // If device is a multimedia and indicator isn't gate, the controll icon show at list
        device.indicator.includes('TV') || device.indicator.includes('AIR') || device.indicator.includes('FAN') ? (
          <TouchableOpacity onPress={() => openModal()}>
            <List.Icon icon='remote-tv' style={styles.icon} color={Colors.blue500} />
          </TouchableOpacity>
        ) : (
          device.type == 'DIMMER' && (
            <DeviceSlider
              id={device.id}
              intensity={device.intensity}
              disabled={selectionMode}
              setDeviceState={setDeviceState}
            />
          )
        )}
      </View>
    ),
    [selected, device.intensity, selectionMode],
  );

  return (
    <List.Item
      onPress={() => setTimeout(handlePress, 0)}
      style={selected ? styles.itemSelected : styles.item}
      onLongPress={handleLongPress}
      title={subheading}
      left={left}
      right={right}
    />
  );
}

export default memo(DeviceItem);

const styles = StyleSheet.create({
  itemSelected: { height: 56, backgroundColor: 'lightgray' },
  item: { height: 56 },
  right: { flexDirection: 'row', alignItems: 'center' },
  icon: { marginVertical: 0, marginLeft: 52 },
  off: { color: Colors.grey500 },
});
