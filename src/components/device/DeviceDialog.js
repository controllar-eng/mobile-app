import React, { useState, useEffect } from 'react';
import { Button, Portal, Dialog, HelperText, Caption, Colors } from 'react-native-paper';
import { useMutation } from '@apollo/client';
import KeyboardAwareDialog from '../KeyboardAwareDialog';
import { UPDATE_DEVICE } from '../../data/gql';
import TextInputInAPortal from '../TextInputInPortal';
import { View, StyleSheet } from 'react-native';
import { INDICATORS } from './DeviceItem';

const CustomDialog = Platform.OS === 'ios' ? KeyboardAwareDialog : Dialog;

export default function DeviceDialog({ onConfirm, onDismiss, onChange, editing, open = !!editing, listen }) {
  const [name, setName] = useState();
  const [oldName, setOldName] = useState();
  const [indicator, setIndicator] = useState('LIGHTBULB');
  const [error, setError] = useState();

  const [update] = useMutation(UPDATE_DEVICE);

  useEffect(() => {
    const name = editing?.name.includes('lightbulb')
      ? editing?.name.substring(0, editing?.name.length - 18)
      : editing?.name || '';
    setName(name);
    setOldName(name);
    setIndicator(editing?.indicator || 'LIGHTBULB');
  }, [editing]);

  useEffect(() => onChange(name), [name]);

  const regexPattern = /^(TV|WINDOW|RF_DEFAULT|TIMER|GATE|AIR|CLOSE_|STOP_|OPEN_|FAN|FAN).*$/;

  return (
    <Portal>
      <CustomDialog visible={open} onDismiss={onDismiss}>
        <Dialog.Title>Editar Lumino: {oldName}</Dialog.Title>
        <Dialog.Content>
          <View
            style={{
              flexDirection: 'row',
              alignItems: 'center',
              flexWrap: 'wrap',
            }}
          >
            {Object.entries(INDICATORS).map(([key, val]) => {
              const Comp = val.comp;
              if (!regexPattern.test(key))
                return (
                  <Comp
                    key={key}
                    style={styles.icon}
                    size={24}
                    onPress={() => setIndicator(key)}
                    {...INDICATORS[key][indicator === key ? 'on' : 'off']}
                    {...(key !== indicator && key === 'CIRCLE_RED' && { color: Colors.red200 })}
                    {...(key !== indicator && key === 'CIRCLE_YELLOW' && { color: Colors.yellowA700 })}
                  />
                );
            })}
          </View>
          <View>
            <TextInputInAPortal
              style={{ flexGrow: 1 }}
              autoFocus
              mode='outlined'
              placeholder='Nome'
              dense
              value={name}
              onChangeText={setName}
              error={error && !!error.name}
            />
          </View>
          <HelperText type='error' visible={error && !!error.name}>
            {error && error.name}
          </HelperText>
          {listen && <Caption>Escutando pulsadores e painéis Controllar...</Caption>}
        </Dialog.Content>
        <Dialog.Actions>
          <Button onPress={onDismiss}>{listen ? 'Fechar' : 'Cancelar'}</Button>
          <Button
            disabled={!name || !editing?.id}
            onPress={async () => {
              let _name = name;
              if (editing?.type === 'MULTIMEDIA' && editing?.indicator === 'LIGHTBULB') {
                _name += editing?.name.substring(editing?.name.length - 18);
              }

              const { data } = await update({
                variables: { id: editing.id, name: _name, indicator },
              });

              setError(data.updateDevice.error);
              if (!data.updateDevice.error && onConfirm) {
                onConfirm(data.updateDevice.device);
              }
            }}
          >
            Confirmar
          </Button>
        </Dialog.Actions>
      </CustomDialog>
    </Portal>
  );
}

const styles = StyleSheet.create({ icon: { marginHorizontal: 7, padding: 7 } });
