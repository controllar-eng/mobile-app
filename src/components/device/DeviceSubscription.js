import { useContext, useEffect, useState, useRef } from 'react';

import { AppContext } from '../../lib/context';
import { createSubscriptionClient } from '../../lib/apollo';
import { BROADLINKS_CONNECTEDS, BROADLINK_ERROR, DEVICE_STATE_CHANGED, STATE_FRAGMENT } from '../../data/gql';

// https://github.com/apollographql/react-apollo/issues/3549
export default function DeviceSubscription({ handleDeviceChange }) {
  const {
    instance,
    setShowPrompt,
    client,
    SET_BROADLINK_ERROR,
    BROADLINKS_CONNECTEDS: _BROADLINKS_CONNECTEDS,
    SET_BROADLINKS_CONNECTEDS,
  } = useContext(AppContext);

  const [wsClient, setWsClient] = useState();

  useEffect(() => {
    let forceClose = false;

    const ws = createSubscriptionClient(instance, setShowPrompt);

    ws.request({ query: DEVICE_STATE_CHANGED }).subscribe({
      next: ({ data }) => {
        const { id, intensity, on } = data.deviceStateChanged;

        const prev = client.readFragment({
          id: `DeviceState:${id}`,
          fragment: STATE_FRAGMENT,
        });

        if (prev && (prev.on !== on || prev.intensity !== intensity)) {
          client.writeFragment({
            id: `DeviceState:${id}`,
            fragment: STATE_FRAGMENT,
            data: { on, intensity },
          });
        }
      },
    });

    ws.request({ query: BROADLINK_ERROR }).subscribe({
      next: ({ data }) => {
        const { error } = data.broadlinkError;
        SET_BROADLINK_ERROR(error);
      },
    });

    ws.request({ query: BROADLINKS_CONNECTEDS }).subscribe({
      next: ({ data }) => {
        if (JSON.stringify(data.broadlinksConnecteds) != JSON.stringify(_BROADLINKS_CONNECTEDS)) {
          SET_BROADLINKS_CONNECTEDS(data.broadlinksConnecteds);
        }
      },
    });

    setWsClient(ws);

    const offList = [
      ws.onConnected(() => console.log('onConnected') || setShowPrompt(false)),
      ws.onReconnected(() => console.log('onReconnected') || setShowPrompt(false)),
      ws.onDisconnected(() => {
        console.log('onDisconnected');
        if (!forceClose) setShowPrompt(true);
      }),
      ws.onError(e => console.log('onError', e.message) || setShowPrompt(true)),
    ];

    return () => {
      forceClose = true;

      offList.forEach(off => off());

      ws.close();
    };
  }, [client, _BROADLINKS_CONNECTEDS]);

  const handleDeviceChangeRef = useRef(handleDeviceChange || null);

  function useSubscription(wsClient, query) {
    useEffect(() => {
      if (wsClient) {
        const sub = wsClient.request({ query }).subscribe({
          next: ({ data }) => handleDeviceChangeRef.current && handleDeviceChangeRef.current(data),
        });

        return () => sub.unsubscribe();
      }
    }, [wsClient, query]);
  }

  useSubscription(wsClient, DEVICE_STATE_CHANGED);
  useSubscription(wsClient, BROADLINK_ERROR);
  useSubscription(wsClient, BROADLINKS_CONNECTEDS);

  useEffect(() => {
    handleDeviceChangeRef.current = handleDeviceChange;
  }, [handleDeviceChange]);

  // Using ws above client directly as a workaround to
  // https://github.com/apollographql/apollo-client/issues/6520
  // const { data, error, loading } = useSubscription(DEVICE_STATE_CHANGED, {
  //  onSubscriptionData: useCallback(handleStateChange, [])
  // })
  return null;
}
