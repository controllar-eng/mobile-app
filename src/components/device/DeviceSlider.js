import React, { useState, useEffect, useMemo } from 'react';
import { useTheme, Colors } from 'react-native-paper';
import Slider from '@react-native-community/slider';
import { StyleSheet, Platform } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

// Avoid using on debug https://github.com/facebook/react-native/issues/26705
const thumb =
  process.env.NODE_ENV === 'development' ? undefined : Icon.getImageSourceSync('circle', 16, Colors.blue500);

export default function DeviceSlider({ id, setDeviceState, disabled, color, ...props }) {
  const theme = useTheme();
  const tintColor = color ? color : theme.colors.primary;

  const [intensity, setIntensity] = useState(props.intensity);
  const [isDragging, setIsDragging] = useState(false);

  useEffect(() => {
    if (!isDragging) setIntensity(props.intensity);
  }, [props.intensity]);

  return (
    <Slider
      thumbImage={thumb}
      disabled={disabled}
      value={intensity}
      minimumValue={1}
      maximumValue={50}
      style={styles.slider}
      minimumTrackTintColor={tintColor}
      thumbTintColor={Platform.OS === 'android' ? tintColor : undefined}
      onValueChange={value => {
        setDeviceState({
          variables: { id, intensity: Math.round(value) },
        });
      }}
      onSlidingStart={() => setIsDragging(true)}
      onSlidingComplete={value => {
        setIsDragging(false);
        setIntensity(value);
      }}
    />
  );
}

const styles = StyleSheet.create({
  slider: { width: Platform.OS === 'android' ? 100 : 80, height: '100%' },
});
