import React, { useState, useContext } from 'react';
import { IconButton, Menu, Divider, TouchableRipple } from 'react-native-paper';
import { DEVICE_GROUPS, SET_DEVICE_GROUP, DEVICE_STATES } from '../../data/gql';
import { AppContext } from '../../lib/context';
import { useQuery, useMutation } from '@apollo/client';
import DeviceGroupDialog from './DeviceGroupDialog';
import { sort } from '../../lib/utils';
import { StyleSheet } from 'react-native';

export default function SetDeviceListGroup({ devices, onConfirm }) {
  const [open, setOpen] = useState(false);

  const [dialogOpen, setDialogOpen] = useState(false);

  const { client } = useContext(AppContext);
  const { loading, error, data } = useQuery(DEVICE_GROUPS, { client });

  const [setDeviceGroup] = useMutation(SET_DEVICE_GROUP, { client });

  async function handlePress(id) {
    setOpen(false);

    const deviceIds = devices.map(dev => dev.id);

    const { data } = await setDeviceGroup({
      variables: {
        groupId: id,
        deviceIds,
      },
      update(cache, { data: { setDeviceGroup } }) {
        if (!setDeviceGroup.success) return;

        const { deviceStates } = cache.readQuery({
          query: DEVICE_STATES,
        });

        cache.writeQuery({
          query: DEVICE_STATES,
          data: {
            deviceStates: deviceStates.map(device =>
              deviceIds.includes(device.id)
                ? {
                    ...device,
                    // eslint-disable-next-line camelcase
                    group_id: id,
                  }
                : device,
            ),
          },
        });
      },
    });

    if (data.setDeviceGroup.success) {
      onConfirm();
    }
  }

  return (
    <>
      <DeviceGroupDialog
        visible={dialogOpen}
        onDismiss={() => setDialogOpen(false)}
        onConfirm={newGroup => {
          setDialogOpen(false);
          onConfirm(newGroup);
        }}
        devices={devices}
      />
      <Menu
        statusBarHeight={40}
        visible={open}
        onDismiss={() => setOpen(false)}
        anchor={
          <IconButton icon='table-column-plus-after' onPress={() => setOpen(true)} color='white' style={styles.icon} />
        }
      >
        <Menu.Item title='Definir Setor...' disabled />

        {data &&
          sort(data.deviceGroups, 'name').map(item => (
            <Menu.Item
              icon='arrow-decision-outline'
              title={item.name}
              key={item.id}
              onPress={() => handlePress(item.id)}
            />
          ))}

        <Divider />

        <Menu.Item icon='broom' title='Limpar Setorização' key='clear-group' onPress={() => handlePress(null)} />

        <Menu.Item
          icon='plus'
          title='Novo Setor...'
          onPress={() => {
            setOpen(false);
            setDialogOpen(true);
          }}
        />
      </Menu>
    </>
  );
}

const styles = StyleSheet.create({ icon: { marginLeft: 0 } });
