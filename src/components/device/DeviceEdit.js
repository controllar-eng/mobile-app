import React, { useState, useEffect, useContext } from 'react';
import { IconButton } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import DeviceDialog from './DeviceDialog';
import { useQuery } from '@apollo/client';
import { DEVICE_STATES } from '../../data/gql';
import { AppContext } from '../../lib/context';

export default function DeviceEdit({ device, onConfirm, icon, listen, editMultimedia }) {
  const { setHandleDeviceChange } = useContext(AppContext);

  const { data } = useQuery(DEVICE_STATES);

  const [editing, setEditing] = useState();
  const [open, setOpen] = useState(false);
  const [name, setName] = useState();

  // maybe refactor these things to DeviceDialog?
  useEffect(() => {
    if (listen && open && name === (editing?.name || '')) {
      setHandleDeviceChange(() => ({ deviceStateChanged }) => {
        const device = data?.deviceStates.find(dev => dev.id === deviceStateChanged.id);
        setEditing(device);
      });
      return () => setHandleDeviceChange(null);
    }
  }, [!!data, listen, open, editing, name]);

  return (
    <>
      <DeviceDialog
        open={open}
        editing={editing}
        onDismiss={() => {
          setOpen(false);
          setEditing(null);
        }}
        onConfirm={edited => {
          if (!listen) setOpen(false);
          if (onConfirm) onConfirm(edited);
          setEditing(null);
        }}
        onChange={setName}
        listen={listen}
      />
      <IconButton
        style={{ marginHorizontal: -2 }}
        onPress={() => {
          if (device && device.type === 'MULTIMEDIA' && !device.name.includes('lightbulb')) editMultimedia(device);
          else {
            setEditing(device);
            setOpen(true);
          }
        }}
        color='white'
        icon={props => <Icon size={26} name={icon || 'pencil'} color='white' />}
      />
    </>
  );
}
