import React, { memo, useState, useLayoutEffect } from 'react';
import { View, Dimensions, StyleSheet } from 'react-native';
import { Dialog, Colors } from 'react-native-paper';
import KeyboardSpacer from 'react-native-keyboard-spacer';

const windowHeight = Dimensions.get('window').height;
const halfWindowHeight = windowHeight / 2;

// https://github.com/callstack/react-native-paper/pull/963#issuecomment-575992021
export default memo(({ children, ...rest }) => {
  const [topSpacing, setTopSpacing] = useState(10);
  const [height, setHeight] = useState(0);

  const onLayout = ({
    nativeEvent: {
      layout: { height: _height },
    },
  }) => {
    if (!height && height !== _height) {
      setHeight(_height);
    }
  };

  useLayoutEffect(() => {
    const newTopSpacing = -halfWindowHeight + height;
    setTopSpacing(newTopSpacing);
  }, [height]);

  return (
    <Dialog {...rest} style={styles.dialog}>
      <View onLayout={onLayout} style={styles.layout}>
        {children}
      </View>
      <KeyboardSpacer topSpacing={topSpacing} />
    </Dialog>
  );
});

const styles = StyleSheet.create({
  dialog: { backgroundColor: 'transparent' },
  layout: { backgroundColor: Colors.white },
});
