import React from 'react';
import { View, Platform, StyleSheet } from 'react-native';
import { Checkbox, TouchableRipple, Subheading } from 'react-native-paper';

const LabeledCheck = ({ checked, label, ...props }) => (
  <TouchableRipple {...props}>
    <View style={styles.view}>
      {Platform.OS === 'android' ? (
        <Checkbox status={checked ? 'checked' : 'unchecked'} />
      ) : (
        <Checkbox status={checked ? 'checked' : 'indeterminate'} />
      )}
      <Subheading>{label}</Subheading>
    </View>
  </TouchableRipple>
);

const styles = StyleSheet.create({
  view: { flexDirection: 'row', alignItems: 'center' },
});

export default LabeledCheck;
