import React from 'react';
import { Button, Portal, Dialog, useTheme, Colors } from 'react-native-paper';
import { ScrollView, View, StyleSheet } from 'react-native';
import KeyboardAwareDialog from '../KeyboardAwareDialog';
import SceneIcon, { sceneIconConfig } from './SceneIcon';

const CustomDialog = Platform.OS === 'ios' ? KeyboardAwareDialog : Dialog;

const IconPickerDialog = ({ visible, onConfirm, onDismiss, current }) => {
  const theme = useTheme();

  return (
    <Portal>
      <Dialog visible={visible} onDismiss={onDismiss} style={styles.dialog}>
        <Dialog.Title>Alterar Ícone</Dialog.Title>
        <Dialog.ScrollArea>
          <ScrollView>
            <View style={styles.mainView}>
              {sceneIconConfig.glyphs.map(glyph => (
                <SceneIcon
                  key={glyph.css}
                  name={glyph.css}
                  size={32}
                  style={[styles.icon, current === glyph.css && styles.iconCurrent]}
                  onPress={() => onConfirm(glyph.css)}
                />
              ))}
            </View>
          </ScrollView>
        </Dialog.ScrollArea>
        <Dialog.Actions>
          <Button onPress={onDismiss}>Cancelar</Button>
        </Dialog.Actions>
      </Dialog>
    </Portal>
  );
};

const styles = StyleSheet.create({
  dialog: { maxHeight: '100%' },
  mainView: {
    flexDirection: 'row',
    flexWrap: 'wrap',
    padding: -8,
    opacity: 0.54,
    justifyContent: 'space-evenly',
  },
  icon: {
    padding: 8,
  },
  iconCurrent: {
    backgroundColor: Colors.blue500,
  },
});

export default IconPickerDialog;
