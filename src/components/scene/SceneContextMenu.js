import { useMutation } from '@apollo/client';
import AsyncStorage from '@react-native-community/async-storage';
import { useNavigation } from '@react-navigation/native';
import React, { cloneElement, useContext, useState } from 'react';
import { Alert, NativeModules } from 'react-native';
import { Menu } from 'react-native-paper';
import { DELETE_SCENE } from '../../data/gql';
import { AppContext } from '../../lib/context';

// TODO: Implement on IOS

const SceneContextMenu = ({ anchor, editing, query }) => {
  const { profile, createClient, instance } = useContext(AppContext);

  const navigation = useNavigation();
  const [open, setOpen] = useState(false);

  const [deleteScene] = useMutation(DELETE_SCENE, {
    update(cache) {
      const { scenes, now } = cache.readQuery({
        query,
        // eslint-disable-next-line camelcase
        variables: { profile_id: profile.id },
      });
      cache.writeQuery({
        query,
        data: { now, scenes: scenes.filter(scene => scene.id != editing.id) },
        // eslint-disable-next-line camelcase
        variables: { profile_id: profile.id },
      });
    },
  });

  async function saveWidget(id) {
    const {
      SharedStorage1,
      SharedStorage2,
      SharedStorage3,
      SharedStorage4,
      SharedStorage5,
      SharedStorage6,
      SharedStorage7,
      SharedStorage8,
    } = NativeModules;
    const SharedStorageSceneName = { 1: SharedStorage1, 2: SharedStorage3, 3: SharedStorage5, 4: SharedStorage7 };
    const SharedStorageSceneInstance = { 1: SharedStorage2, 2: SharedStorage4, 3: SharedStorage6, 4: SharedStorage8 };

    setOpen(false);
    if (!editing.actions || editing.actions.length === 0) {
      Alert.alert('Erro!', 'Cena sem luminos não pode ser adicionada ao widget');
      return;
    }
    Alert.alert('Sucesso!', `Adicionando cena ${editing.name} ao widget`);
    await AsyncStorage.setItem(`@widget_${id}_scene`, JSON.stringify(editing));
    await AsyncStorage.setItem(`@widget_${id}_scene_instance`, JSON.stringify(instance));
    SharedStorageSceneName[id].set(JSON.stringify({ text: editing.name }));
    SharedStorageSceneInstance[id].set(JSON.stringify({ text: instance.name }));
    navigation.navigate('DeviceList');
  }

  return (
    <Menu
      statusBarHeight={45}
      visible={open}
      onDismiss={() => setOpen(false)}
      anchor={cloneElement(anchor, { onLongPress: e => setOpen(true) })}
    >
      <Menu.Item
        icon='pencil'
        title='Editar'
        onPress={() => {
          setOpen(false);
          navigation.navigate('SceneForm', { scene: editing });
        }}
      />
      <Menu.Item
        icon='delete'
        title='Excluir'
        onPress={() => {
          setOpen(false);
          deleteScene({ variables: { id: editing.id } });
          createClient(); // TODO: use cache after it
        }}
      />
      <Menu.Item icon='chart-tree' title='Widget 04' onPress={async () => await saveWidget(4)} />
      <Menu.Item icon='chart-tree' title='Widget 03' onPress={async () => await saveWidget(3)} />
      <Menu.Item icon='chart-tree' title='Widget 02' onPress={async () => await saveWidget(2)} />
      <Menu.Item icon='chart-tree' title='Widget 01' onPress={async () => await saveWidget(1)} />
    </Menu>
  );
};

export default SceneContextMenu;
