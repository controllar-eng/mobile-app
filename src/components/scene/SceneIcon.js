import React from 'react';

import { createIconSetFromFontello } from 'react-native-vector-icons';
import sceneIconConfig from '../../../assets/scenes.config.json';

const FALLBACK_ICON = 'home';

const FontelloIcon = createIconSetFromFontello(sceneIconConfig, 'scenes');

const SceneIcon = ({ name, ...props }) => <FontelloIcon name={name || FALLBACK_ICON} {...props} />;

export default SceneIcon;

export { sceneIconConfig };
