import React, { useState, cloneElement, useContext } from 'react';
import { Menu } from 'react-native-paper';
import { useNavigation } from '@react-navigation/native';
import { useMutation } from '@apollo/client';
import { DELETE_TIMER } from '../../data/gql';
import { AppContext } from '../../lib/context';

const TimerContextMenu = ({ anchor, editing, query }) => {
  const { profile, createClient } = useContext(AppContext);

  const navigation = useNavigation();
  const [open, setOpen] = useState(false);

  const [deleteTimer] = useMutation(DELETE_TIMER, {
    update(cache) {
      const { timers, now } = cache.readQuery({
        query,
        // eslint-disable-next-line camelcase
        variables: { profile_id: profile.id },
      });
      cache.writeQuery({
        query,
        data: { now, timers: timers.filter(timer => timer.id != editing.id) },
        // eslint-disable-next-line camelcase
        variables: { profile_id: profile.id },
      });
    },
  });

  return (
    <Menu
      statusBarHeight={70}
      visible={open}
      onDismiss={() => setOpen(false)}
      anchor={cloneElement(anchor, { onLongPress: e => setOpen(true) })}
    >
      <Menu.Item
        icon='pencil'
        title='Editar'
        onPress={() => {
          setOpen(false);
          navigation.navigate('TimerForm', { timer: editing });
        }}
      />
      <Menu.Item
        icon='delete'
        title='Excluir'
        onPress={() => {
          setOpen(false);
          deleteTimer({ variables: { id: editing.id } });
          createClient(); // TODO: use cache after it
        }}
      />
    </Menu>
  );
};

export default TimerContextMenu;
