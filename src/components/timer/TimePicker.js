import React from 'react';
import { WheelPicker, TimePicker as TimePickerRN } from 'react-native-wheel-picker-android';
import { useTheme } from 'react-native-paper';
import { View, Text, StyleSheet } from 'react-native';
import { pad } from '../../lib/utils';

const styleProps = (theme, withLabel = false) => ({
  isCyclic: true,
  itemStyle: { height: 140 },
  style: { flex: 1, height: 140, marginRight: withLabel ? -25 : 0 },
  selectedItemTextSize: 20,
  itemTextFontFamily: theme.fonts.medium.fontFamily,
  indicatorColor: 'grey',
});

const TimePickerWithSecs = ({ value, disabled, onChange }) => {
  const theme = useTheme();

  const parts = (value ? value : '00:00:00').split(':');
  const idx = [+parts[0], +parts[1], +parts[2] || 0];

  const style = styleProps(theme, true);
  return (
    <View style={[styles.mainView, disabled && styles.disabled]} pointerEvents={disabled ? 'none' : 'auto'}>
      <WheelPicker
        {...style}
        selectedItem={idx[0]}
        data={[...Array(24)].map((_, num) => pad(num))}
        onItemSelected={hour => onChange(`${pad(hour)}:${parts[1]}:${parts[2]}`)}
      />
      <Text style={styles.label}>h</Text>
      <WheelPicker
        {...style}
        selectedItem={idx[1]}
        data={[...Array(60)].map((_, num) => pad(num))}
        onItemSelected={minute => onChange(`${parts[0]}:${pad(minute)}:${parts[2]}`)}
      />
      <Text style={styles.label}>min</Text>
      <WheelPicker
        {...style}
        selectedItem={idx[2]}
        data={[...Array(60)].map((_, num) => pad(num))}
        onItemSelected={second => onChange(`${parts[0]}:${parts[1]}:${pad(second)}`)}
      />
      <Text style={styles.label}>seg</Text>
    </View>
  );
};

const TimePicker = ({ value, onChange, disabled }) => {
  const theme = useTheme();

  return (
    <View style={disabled && styles.disabled} pointerEvents={disabled ? 'none' : 'auto'}>
      <TimePickerRN
        {...styleProps(theme, false)}
        pointerEvents={disabled ? 'none' : 'auto'}
        format24
        initDate={value}
        onTimeSelected={onChange}
        minutes={[...Array(60)].map((_, num) => pad(num))}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  mainView: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  disabled: { opacity: 0.4 },
  label: { marginTop: Platform.OS === 'ios' ? 5 : 8, width: 25 },
  disabled: { opacity: 0.4 },
});

export { TimePicker, TimePickerWithSecs };
