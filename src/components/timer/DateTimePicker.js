import React, { useState } from 'react';
import { Button } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import { format } from 'date-fns';
import { default as RNDateTimePicker } from '@react-native-community/datetimepicker';
import { View, StyleSheet } from 'react-native';

/**
 * If detached from Expo or Expo solves this: https://expo.canny.io/feature-requests/p/support-react-native-date-picker
 * Also if this ever gets solved: https://github.com/react-native-community/datetimepicker/issues/14
 */
const DateTimePickerAndroid = ({ disabled, onChange, value, mode, ...props }) => {
  const [open, setOpen] = useState(false);
  return (
    <>
      <Button
        disabled={disabled}
        icon={({ size, color }) => (
          <Icon name={`md-${mode === 'time' ? 'time-outline' : 'calendar-outline'}`} size={19} color={color} />
        )}
        mode='contained'
        onPress={() => setOpen(true)}
        labelStyle={styles.label}
      >
        {format(value, mode === 'time' ? 'HH:mm' : 'dd/MM/yyyy')}
      </Button>
      {open && (
        <RNDateTimePicker
          {...props}
          mode={mode}
          value={value}
          onChange={(e, time) => {
            setOpen(false);
            onChange(e, time);
          }}
        />
      )}
    </>
  );
};

const DateTimePickerIOS = ({ disabled, ...props }) => (
  <View pointerEvents={disabled ? 'none' : 'auto'}>
    <RNDateTimePicker {...props} style={[styles.picker, disabled && styles.disabled]} />
  </View>
);

const styles = StyleSheet.create({
  picker: { height: 140 },
  disabled: { opacity: 0.2 },
  label: { fontSize: 24 },
});

const DateTimePicker = Platform.OS === 'android' ? DateTimePickerAndroid : DateTimePickerIOS;

export default DateTimePicker;
