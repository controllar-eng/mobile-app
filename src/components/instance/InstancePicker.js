import React, { useContext, useState } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Button, Colors, Divider, Menu, TouchableRipple } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import { AppContext, INSTANCE_LOCAL_NET } from '../../lib/context';
import InstanceDialog from './InstanceDialog';

export default function InstancePicker() {
  const { instance, instanceList, setInstanceList, setInstance, createClient, serverToken, setOldInstance } =
    useContext(AppContext);

  const [open, setOpen] = useState(false);
  const [editing, setEditing] = useState();

  return (
    <>
      <InstanceDialog
        editing={editing}
        onDismiss={() => setEditing(null)}
        onConfirm={edited => {
          if (edited.id === instance.id) {
            setInstance(edited);
            setOldInstance(edited);
          }
          setInstanceList([edited, ...instanceList.filter(item => item.id !== edited.id)]);
          setEditing(null);
        }}
        onDelete={edited => {
          // TODO should search on local net
          if (edited.id === instance.id) {
            setInstance(INSTANCE_LOCAL_NET);
            setOldInstance(INSTANCE_LOCAL_NET);
          }

          setInstanceList([...instanceList.filter(item => item.id !== edited.id)]);
          setEditing(null);
        }}
        list={instanceList}
      />
      <Menu
        statusBarHeight={32}
        visible={open}
        onDismiss={() => setOpen(false)}
        anchor={
          <View style={styles.parentView}>
            <Button icon='home-group' color='white' style={styles.button} onPress={() => setOpen(true)}>
              <Text style={styles.textbutton}>
                {instance?.name} <Icon name='chevron-down' />
              </Text>
            </Button>
          </View>
        }
      >
        {instance.id === INSTANCE_LOCAL_NET.id &&
          serverToken &&
          !instanceList.find(inst => inst.id === serverToken.id) && (
            <>
              <Menu.Item
                icon='home-plus-outline'
                title='Memorizar Instalação'
                onPress={async () => {
                  setOpen(false);
                  setEditing({
                    id: serverToken.id,
                    name: '',
                    token: serverToken.token,
                  });
                }}
              />
              <Divider style={styles.divider} />
            </>
          )}
        {instance.id !== INSTANCE_LOCAL_NET.id && (
          <>
            <Menu.Item
              icon='home-search-outline'
              title='Buscar na Rede'
              onPress={() => {
                setOpen(false);
                createClient(INSTANCE_LOCAL_NET);
                setOldInstance(INSTANCE_LOCAL_NET);
              }}
            />
            <Divider style={styles.divider} />
          </>
        )}

        <Menu.Item title='Instalações Memorizadas' disabled />

        {instanceList.map(item => (
          <TouchableRipple
            key={item.id}
            onPress={() => {
              setOpen(false);
              createClient(item);
              setOldInstance(item);
            }}
            onLongPress={() => {
              setOpen(false);
              setEditing(item);
            }}
          >
            <Menu.Item
              icon={item.id === instance?.id ? 'home-group' : 'home-import-outline'}
              title={item.name}
              titleStyle={item.id === instance?.id ? { fontWeight: '700' } : undefined}
            />
          </TouchableRipple>
        ))}
      </Menu>
    </>
  );
}

const styles = StyleSheet.create({
  button: { borderRadius: 0, justifyContent: 'space-between' },
  parentView: { backgroundColor: Colors.blue300, elevation: 2, borderRadius: 25 },
  divider: { height: 1 },
  textbutton: { fontSize: 9 },
});
