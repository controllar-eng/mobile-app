import React, { useState, useEffect } from 'react';
import { Button, TextInput, Portal, Dialog, HelperText, IconButton, useTheme } from 'react-native-paper';
import KeyboardAwareDialog from '../KeyboardAwareDialog';
import { Alert } from 'react-native';
import TextInputInAPortal from '../TextInputInPortal';

const CustomDialog = Platform.OS === 'ios' ? KeyboardAwareDialog : Dialog;

export default function InstanceDialog({ onConfirm, onDismiss, onDelete, editing, list }) {
  const theme = useTheme();

  const [name, setName] = useState();
  const [error, setError] = useState();

  useEffect(() => setName(editing?.name), [editing]);

  return (
    <Portal>
      <CustomDialog visible={!!editing} onDismiss={onDismiss}>
        <Dialog.Title>
          {editing?.name ? <>Editar Instalação: {editing?.name}</> : <>Memorizar Instalação</>}
        </Dialog.Title>
        <Dialog.Content>
          <TextInputInAPortal
            autoFocus
            mode='outlined'
            placeholder='Nome'
            dense
            value={name}
            onChangeText={setName}
            error={error && !!error.name}
          />
          <HelperText type='error' visible={error && !!error.name}>
            {error && error.name}
          </HelperText>
        </Dialog.Content>
        <Dialog.Actions>
          {editing?.name !== '' && (
            <IconButton
              color={theme.colors.error}
              icon='trash-can-outline'
              onPress={() => {
                Alert.alert(
                  'Remover Instalação',
                  `Tem CERTEZA que quer remover a instalação ${editing.name}?`,
                  [
                    { text: 'Cancelar', onPress: onDismiss, style: 'cancel' },
                    { text: 'REMOVER', onPress: () => onDelete(editing) },
                  ],
                  { cancelable: false },
                );
              }}
            />
          )}
          <Button onPress={onDismiss}>Cancelar</Button>
          <Button
            disabled={!name}
            onPress={() => {
              if (list.find(item => item.name === name && item.id !== editing.id))
                setError({ name: 'Nome já utilizado' });
              else onConfirm({ ...editing, name: name.substr(0, 15) });
            }}
          >
            Confirmar
          </Button>
        </Dialog.Actions>
      </CustomDialog>
    </Portal>
  );
}
