import { StyleSheet, Text, View } from 'react-native';
import { ActivityIndicator } from 'react-native-paper';
import React from 'react';
import { theme } from '../lib/utils';
export function LoadingControlls({ title }) {
  return (
    <View style={styles.loadingData}>
      <Text style={styles.loadingDataText}>{title}</Text>
      <ActivityIndicator size='large' color={theme.colors.red} />
    </View>
  );
}

const styles = StyleSheet.create({
  loadingData: {
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'row',
    flexDirection: 'row',
    width: '100%',
  },
  loadingDataText: {
    color: theme.colors.red,
    fontSize: 15,
    paddingRight: 5,
  },
});
