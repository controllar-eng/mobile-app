import { useQuery } from '@apollo/client';
import React, { useContext, useEffect, useRef, useState } from 'react';
import { Keyboard, Modal, Platform, SectionList, StyleSheet, TouchableOpacity, View } from 'react-native';
import {
  ActivityIndicator,
  Button,
  Caption,
  Colors,
  Dialog,
  IconButton,
  List,
  Menu,
  Portal,
  Subheading,
  Text,
} from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import { DEVICE_STATES, DEVICE_TYPE, LIST_BUTTONS } from '../data/gql';
import { AppContext } from '../lib/context';
import { arrayToHash, sort, theme } from '../lib/utils';
import LabeledCheck from './LabeledCheck';
import TextInputInAPortal from './TextInputInPortal';
import { createControlls } from './controlls/createControlls';
import DeviceGroupPicker, { ALL, getGroupFilter } from './device/DeviceGroupPicker';
import { INDICATORS, isSensor } from './device/DeviceItem';
import DeviceSlider from './device/DeviceSlider';

const isIOS = Platform.OS === 'ios';

const regexPattern = /^(GATE|CLOSE_|STOP_|OPEN_).*$/;

const fanButtons = [
  'speed-one',
  'speed-two',
  'speed-three',
  'ventilation',
  'exhaust',
  'direction',
  'speed-plus',
  'speed-minus',
];

const delayDevice = (id, intensity) => ({
  // eslint-disable-next-line camelcase
  group_id: 'null',
  id,
  indicator: 'TIMER',
  name: 'DELAY',
  type: 'SWITCH',
  on: true,
  intensity,
});

const buttonDevice = (id, name) => ({
  // eslint-disable-next-line camelcase
  group_id: 'null',
  id,
  indicator: 'RF_DEFAULT',
  name,
  type: 'SWITCH',
  on: true,
  intensity: 50,
});

function actionsToStates(actions, states, listButtons) {
  return actions.map((action, index) => {
    if (action.code === 3000) return delayDevice(index + 3000, action.intensity);
    if (action.code >= 1000 && action.code < 3000) {
      const name = listButtons?.data?.listButtons.find(button => button.id === action.code).buttonName;
      return {
        ...buttonDevice(action.code, name),
        storeState: action.storeState,
        sensor: action.sensor,
      };
    }
    return {
      ...states[action.code],
      on: action.intensity > 0,
      ...(action.intensity > 0 && { intensity: action.intensity }),
      storeState: action?.storeState,
      sensor: action?.sensor,
    };
  });
}
function statesToActions(states) {
  return Object.values(states).map(dev => {
    const item = {
      code: dev.id,
      intensity: dev.on ? (dev.type !== 'MULTIMEDIA' && dev.intensity) || 50 : 0,
    };
    if (dev.storeState != undefined) item.storeState = dev.storeState;
    if (dev.sensor != undefined) item.sensor = dev.sensor;
    if (item.code >= 4000) item.code = item.code - 4000;
    return item;
  });
}

function DeviceItem({
  device,
  listen,
  setDeviceState,
  added,
  changeOrder,
  index,
  length,
  setItemIndexShowed,
  setMultimediaDevice,
  showDuplicate,
  duplicate,
  setVerificationLumino,
  sensor,
}) {
  const hideHighligthOffOnly = regexPattern.test(device.indicator) || device.type === 'PULSE';
  const intervalIdRef = useRef(null);
  const [isPressing, setIsPressing] = useState(false);
  const [backgroundColor, setBackgroundColor] = useState(Colors.gray100);
  const [modalOpen, setModalOpen] = useState(false);

  useEffect(() => {
    const handleInterval = async () => {
      const [id, direction, press] = isPressing;
      if (itemCanChange(direction)) changeOrder(id, direction, index, press);
    };

    if (isPressing) {
      const [id, direction, press] = isPressing;
      setBackgroundColor(Colors.blue200);
      if (press === 'singlePress' && itemCanChange(direction)) {
        intervalIdRef.current = setInterval(handleInterval);
        return () => {
          setBackgroundColor(Colors.gray100);
          clearInterval(intervalIdRef.current);
          setIsPressing(false);
        };
      }
      intervalIdRef.current = setInterval(handleInterval, 600);
      return () => {
        setBackgroundColor(Colors.gray100);
        clearInterval(intervalIdRef.current);
      };
    }
  }, [isPressing, index]);

  const selectButtonsVisible =
    device?.buttons?.length > 0 && !device?.indicator.includes('FAN') && !device?.indicator.includes('LIGHTBULB');

  const toggleState = () => {
    if (device.type === 'PULSE') return;
    setTimeout(() => setDeviceState({ id: device.id, on: !device.on, storeState: device.storeState }, added), 0);
  };

  const itemCanChange = direction => (index < length - 1 && direction === 'down') || (index > 0 && direction === 'up');

  const handlePress = (direction, press) => {
    setItemIndexShowed();
    setIsPressing([device.id, direction, press]);
  };
  return (
    <List.Item
      key={device.id >= 1000 && device.id < 3000 ? (device.id + index).toString() : device.id.toString()}
      onPress={listen || (device.id > 1000 && device.id < 4000) ? () => null : toggleState}
      style={[styles.listItem, { backgroundColor }]}
      title={
        <Subheading style={[hideHighligthOffOnly || !device.on ? styles.offTitle : undefined, { fontSize: 14 }]}>
          {device.name[3] === '#'
            ? device.name.substring(5)
            : device.name.includes('lightbulb')
            ? device.name.substring(0, device.name.length - 18)
            : device.name}
          {device.id >= 3000 && device.id <= 4000 && ` - ${device.intensity / 1000} segundo(s)`}
          {device?.buttons?.length > 0 &&
            !device?.indicator.includes('FAN') &&
            !device?.indicator.includes('LIGHTBULB') && (
              <View style={styles.help}>
                <Text style={styles.helpText}>* Pressione e segure</Text>
                <Text style={styles.helpText}>para adicionar botões</Text>
              </View>
            )}
        </Subheading>
      }
      left={props => (
        <List.Icon
          icon={props => {
            const i = device.indicator;
            const indicator = i.includes('TV_') ? 'TV_' : i.includes('AIR_') ? 'AIR_' : i.includes('FAN') ? 'FAN' : i;
            const IconComp = INDICATORS[indicator].comp;
            return (
              <IconComp
                {...props}
                color='lightgray'
                {...INDICATORS[indicator][device.on && !hideHighligthOffOnly ? 'on' : 'off']}
              />
            );
          }}
          {...props}
        />
      )}
      right={props => (
        <View style={[sensor ? styles.none : styles.row]}>
          {device.type === DEVICE_TYPE.DIMMER && <Text style={styles.percentText}>{device.intensity * 2}%</Text>}
          {device.type === 'DIMMER' && (
            <DeviceSlider
              id={device.id}
              disabled={listen}
              intensity={device.intensity}
              setDeviceState={({ variables: { intensity } }) =>
                setDeviceState({ id: device.id, intensity, on: true, storeState: device.storeState }, added)
              }
            />
          )}
          <Icon name='lock-closed' size={16} style={!device.storeState && styles.none} />
          <Icon name='eye' size={16} style={!device.sensor ? styles.none : { marginRight: 5 }} color={Colors.blue500} />
          <Text>{device.sensor}</Text>
          <View style={!added ? styles.none : styles.upDownView}>
            <TouchableOpacity
              style={index === 0 && styles.none}
              onPress={() => handlePress('up', 'singlePress')}
              onLongPress={() => handlePress('up', 'longPress')}
              onPressOut={() => setIsPressing(false)}
            >
              <Icon name='chevron-up' size={26} />
            </TouchableOpacity>
            <TouchableOpacity
              style={index === length - 1 && styles.none}
              onPress={() => handlePress('down', 'singlePress')}
              onLongPress={() => handlePress('down', 'longPress')}
              onPressOut={() => setIsPressing(false)}
            >
              <Icon name='chevron-down' size={26} />
            </TouchableOpacity>
          </View>
          <Modal animationType='none' transparent={true} visible={modalOpen}>
            <View style={styles.containerOpacity}>
              <View style={styles.containerGray} />
            </View>
            <TouchableOpacity style={{ flex: 1 }} onPress={() => setModalOpen(false)} />
            <View style={styles.modalSelectView}>
              <TouchableOpacity
                style={selectButtonsVisible && added ? styles.modalButton : styles.none}
                onPress={() => {
                  setMultimediaDevice(device);
                  setModalOpen(false);
                }}
              >
                <Icon name='apps' size={16} />
                <Text style={styles.modalText}>Selecionar Botões</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={added && device.type !== 'PULSE' ? styles.modalButton : styles.none}
                onPress={() => {
                  let it;
                  if (device.id > 1000 && device.id < 3000) it = { ...device, storeState: !device.storeState };
                  else
                    it = {
                      id: device.id,
                      on: device.type === 'PULSE' ?? device.on,
                      intensity: device.intensity,
                      storeState: !device.storeState,
                    };
                  setDeviceState(it, added);
                  setModalOpen(false);
                }}
              >
                <Icon name='lock-closed' size={18} />
                <Text style={styles.modalText}>Manter o status anterior ao desligar a cena</Text>
                <MaterialCommunityIcons
                  name={device.storeState ? 'circle-slice-8' : 'circle-outline'}
                  size={18}
                  color={Colors.blue500}
                  style={{ marginLeft: 5 }}
                />
              </TouchableOpacity>
              <TouchableOpacity
                style={added && device.type === 'PULSE' ? styles.modalButton : styles.none}
                onPress={() => {
                  setModalOpen(false);
                  setVerificationLumino(device.id);
                }}
              >
                <Icon name='eye' size={16} />
                <Text style={styles.modalText}>Definir lumino de verificação de status</Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={added && showDuplicate && device.type !== 'PULSE' ? styles.modalButton : styles.none}
                onPress={() => duplicate(device)}
              >
                <Icon name='duplicate-outline' size={16} />
                <Text style={styles.modalText}>Duplicar</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity style={{ flex: 1 }} onPress={() => setModalOpen(false)} />
          </Modal>
        </View>
      )}
      onLongPress={() => added && setModalOpen(true)}
    />
  );
}

function Actions({
  actions,
  navigation,
  updateFn,
  initialOffOnly,
  icon = 'save-outline',
  sensor,
  setVerificationLumino,
}) {
  const { setHandleDeviceChange } = useContext(AppContext);

  const [deviceGroup, setDeviceGroup] = useState(ALL);

  const { data } = useQuery(DEVICE_STATES);
  const listButtons = useQuery(LIST_BUTTONS);

  const [list, setList] = useState([]);
  const [states, setStates] = useState();

  const [listen, setListen] = useState(true);

  const [offOnly, setOffOnly] = useState(initialOffOnly || false);

  const [hasLuminosAdded, setHasLuminosAdded] = useState(false);
  const [hasLuminosOn, setHasLuminosOn] = useState(false);

  const sectionListRef = useRef(null);
  const [delayModal, setDelayModal] = useState(false);
  const [delay, setDelay] = useState('');
  const [scrollOffset, setScrollOffset] = useState(0);
  const [itemIndexShowed, setItemIndexShowed] = useState(0);
  const [addedList, setAddedList] = useState(false);
  const [dataButtons, setDataButtons] = useState();
  const [multimediaDevice, setMultimediaDevice] = useState(null);
  const [open, setOpen] = useState(false);

  useEffect(() => {
    if (!data) return;

    const newData = { deviceStates: {} };
    newData.deviceStates = data?.deviceStates.map(value => {
      const buttons = listButtons?.data?.listButtons?.filter(button => button.multimedia_device_id === value.id);
      return { ...value, buttons };
    });
    const hash = arrayToHash(newData.deviceStates);
    setDataButtons(newData);
    setStates(hash);

    if (actions && (listen || !addedList)) {
      setAddedList(true);
      const aux = [];
      const alreadyAdded = [];
      for (let i = 0; i < actions.length; i++) {
        const element = { ...actions[i] };
        const elementsWithSameCode = actions.filter(
          el => el.code === element.code && !alreadyAdded.some(code => code === el.code),
        );
        if (elementsWithSameCode.length > 1 && element.code != 3000) {
          aux.push({ ...element, code: element.code + 4000 });
          alreadyAdded.push(element.code);
        } else {
          aux.push(element);
        }
      }
      aux.forEach(action => {
        if (action.code >= 4000) {
          hash[action.code] = {
            ...hash[action.code - 4000],
            on: action.intensity > 0,
            ...(action.intensity > 0 && { intensity: action.intensity }),
            ...{ id: action.code },
          };
        } else {
          hash[action.code] = {
            ...hash[action.code],
            on: action.intensity > 0,
            ...(action.intensity > 0 && { intensity: action.intensity }),
          };
        }
      });
      setList(
        actionsToStates(
          aux.filter(action => !isSensor(hash[action.code])),
          hash,
          listButtons,
        ),
      );
    }
  }, [data, listButtons?.data?.listButtons, actions]);

  useEffect(() => {
    setHasLuminosOn(list.filter(device => device.on).length > 0);
    setHasLuminosAdded(list.length > 0);
  }, [list]);

  useEffect(() => setListen(actions?.length === 0), [actions?.length]);

  const setDeviceState = ({ id, ...rest }, add) => {
    if (sensor) {
      const _list = list?.map(item => {
        if (item.id === sensor) item.sensor = id;
        return item;
      });
      setList(_list);
      setVerificationLumino(false);
    }
    let newItem;

    if (id > 1000 && id < 4000) {
    } else {
      if (isSensor(states[id])) return;
      newItem = { ...states[id], ...rest };
      setStates({ ...states, [id]: newItem });
    }
    if (add) {
      let exists = false;
      const aux = Object.assign([], list).map(item => {
        if (item.id === id) {
          item = newItem;
          exists = true;
        }
        return item;
      });
      if (exists) setList(aux);
      else setList([newItem, ...list.filter(item => item.id !== id)]);
    }
  };

  useEffect(() => {
    if (listen) {
      setHandleDeviceChange(
        () =>
          ({ deviceStateChanged }) =>
            deviceStateChanged &&
            setDeviceState(
              { ...deviceStateChanged, storeState: list.find(el => el.id === deviceStateChanged.id)?.storeState },
              true,
            ),
      );
      return () => setHandleDeviceChange(null);
    } else setHandleDeviceChange(null);
  }, [listen, states, list]);

  useEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <Menu
          statusBarHeight={45}
          visible={open}
          onDismiss={() => setOpen(false)}
          anchor={
            <Button onPress={() => setOpen(true)}>
              <Icon name={'options'} size={24} />
            </Button>
          }
        >
          <Menu.Item
            title={hasLuminosAdded ? 'Desfazer Seleção' : 'Selecionar Todos'}
            onPress={() => {
              if (!hasLuminosAdded) {
                const listAux = [];
                const dataGroup = dataButtons.deviceStates.filter(
                  dev => getGroupFilter(deviceGroup)(dev) && !isSensor(dev),
                );
                for (let i = 0; i < dataGroup.length; i++) listAux.push(dataGroup[i]);
                setList(listAux);
              } else setList([]);
              setOpen(false);
            }}
          />
          <Menu.Item
            title={hasLuminosOn ? 'Desligar Todos' : 'Ligar Todos'}
            onPress={() => {
              const listAux = [];
              for (let i = 0; i < list.length; i++) {
                if (!fanButtons.some(el => list[i].name.includes(el))) {
                  const device = Object.assign({}, list[i]);
                  device.on = !hasLuminosOn;
                  listAux.push(device);
                }
              }
              setList(listAux);
              setOpen(false);
            }}
          />
        </Menu>
      ),
    });
  });

  const ITEM_HEIGHT = 56;

  const emptyComp = <Caption style={styles.empty}>Nenhum lumino</Caption>;

  function changeOrder(id, direction, scrollIndex, press) {
    const item = list.find(item => item.id === id);
    const index = list.findIndex(item => item.id === id);
    const aux = [...list];
    const isScroll = Platform.OS === 'android' || (Platform.OS === 'ios' && press === 'singlePress');

    if (direction === 'down' && index > 0) {
      const newIndex = scrollIndex - (itemIndexShowed - (Platform.OS === 'ios' ? 2 : 1));
      isScroll && scrollTo(newIndex < 0 ? 0 : newIndex);
      aux[index] = aux[index - 1];
      aux[index - 1] = item;
    } else if (index < list.length - 1) {
      const newIndex = scrollIndex - (itemIndexShowed + (Platform.OS === 'ios' ? 0 : 1));
      isScroll && scrollTo(newIndex < 0 ? 0 : newIndex);
      aux[index] = aux[index + 1];
      aux[index + 1] = item;
    }
    setList(aux);
  }

  function handleAddDelay() {
    const delayInt = parseFloat(delay.replace(',', '.'));
    setDelay('');
    setDelayModal(false);

    if (isNaN(delayInt) || !delayInt || delayInt <= 0) return;

    const idsBigger3000 = list
      .map(item => item.id)
      .filter(id => id >= 3000 && id < 4000)
      .sort((a, b) => a - b);
    const lastBigger3000 = idsBigger3000[idsBigger3000.length - 1] ?? 3000;

    setList([delayDevice(lastBigger3000 + 1, delayInt * 1000), ...list]);
    scrollTo(list.length >= 3 ? list.length - 3 : 0);
  }

  const scrollTo = index => {
    if (sectionListRef.current) {
      const nextIndex = index;
      sectionListRef.current.scrollToLocation({
        sectionIndex: 0,
        itemIndex: nextIndex,
        viewPosition: 0,
        animated: true,
      });
    }
  };

  const controlls = createControlls(
    handleButton,
    multimediaDevice,
    () => {},
    '',
    [],
    false,
    false,
    '',
    dataButtons?.deviceStates,
    false,
  );

  function handleButton(button) {
    setList([buttonDevice(button.id, button.buttonName), ...list]);
    setMultimediaDevice(null);
  }

  function handleDuplicate(device) {
    const aux = { ...device };
    aux.id = aux.id + 4000;
    if (list.find(dev => dev.id === aux.id) || device.id > 4000) return;
    setList([aux, ...list]);
    setStates({ ...states, [aux.id]: aux });
  }

  return (
    <View style={styles.flex1}>
      <Modal
        animationType='none'
        transparent={true}
        style={styles.modal}
        visible={multimediaDevice !== null}
        onDismiss={() => setMultimediaDevice(null)}
      >
        <View style={styles.containerOpacity}>
          <View style={styles.containerGray} />
        </View>
        <TouchableOpacity style={{ flex: 1 }} onPress={() => setMultimediaDevice(null)} />
        {multimediaDevice && controlls[multimediaDevice.indicator]}
        <TouchableOpacity style={{ flex: 1 }} onPress={() => setMultimediaDevice(null)} />
      </Modal>
      {!listen && (
        <Portal>
          <Dialog visible={delayModal} onDismiss={() => setDelayModal(false)} style={styles.dialog}>
            <Dialog.Title>Delay</Dialog.Title>
            <Dialog.Content>
              <View style={{ flexDirection: 'row', alignItems: 'flex-end' }}>
                <TextInputInAPortal
                  style={{ marginRight: 5, height: 25 }}
                  autoFocus
                  mode='outlined'
                  placeholder='5'
                  value={delay}
                  keyboardType='numeric'
                  onChangeText={setDelay}
                />
                <Text style={{ fontSize: 14, color: 'gray' }}>segundos</Text>
              </View>
            </Dialog.Content>
            <Dialog.Actions style={{ alignItems: 'center' }}>
              <Button
                onPress={() => {
                  setDelayModal(false);
                  setDelay('');
                }}
              >
                Fechar
              </Button>
              <Button onPress={handleAddDelay}>Confirmar</Button>
            </Dialog.Actions>
          </Dialog>
        </Portal>
      )}
      {!sensor && (
        <>
          <LabeledCheck
            style={styles.labeledCheck}
            label='Somente desligamento'
            checked={offOnly}
            onPress={() => {
              Keyboard.dismiss();
              setOffOnly(!offOnly);
            }}
          />
          <LabeledCheck
            style={styles.labeledCheck}
            label='Escutar pulsadores e painéis Controllar'
            checked={listen}
            onPress={() => {
              Keyboard.dismiss();
              setTimeout(() => setListen(!listen), 0);
            }}
          />
        </>
      )}

      {!listen && !sensor && (
        <View>
          <DeviceGroupPicker deviceGroup={deviceGroup} setDeviceGroup={setDeviceGroup} />
        </View>
      )}
      <View style={list.length && !sensor && !listen > 0 ? { zIndex: 1, height: 40 } : styles.none}>
        <TouchableOpacity style={styles.buttonAddDelay} onPress={() => setDelayModal(true)}>
          <Icon name='time-outline' size={32} />
          <Icon name='add' size={18} />
        </TouchableOpacity>
      </View>
      {dataButtons && states ? (
        <SectionList
          onScroll={event => setScrollOffset(Math.floor(event.nativeEvent.contentOffset.y / 56))}
          ref={sectionListRef}
          style={list.length && !listen && !sensor && { marginTop: -40 }}
          renderSectionFooter={({ section }) => (section.data.length == 0 ? emptyComp : null)}
          windowSize={10}
          getItemLayout={(_, index) => ({
            length: ITEM_HEIGHT,
            offset: ITEM_HEIGHT * index,
            index,
          })}
          keyExtractor={({ id }, index) => (id >= 1000 && id < 3000 ? (id + index).toString() : id.toString())}
          sections={
            listen
              ? [{ data: list.filter(dev => dev).reverse() }]
              : sensor
              ? [
                  {
                    title: 'Selecione o lumino de verificação de status',
                    data: sort(
                      dataButtons.deviceStates
                        .filter(dev => getGroupFilter(deviceGroup)(dev) && isSensor(dev))
                        .map(dev => states[dev.id]),
                      'name',
                    ),
                  },
                ]
              : [
                  {
                    title: 'Adicionados',
                    data: list
                      .filter(dev => (dev.id >= 1000 || getGroupFilter(deviceGroup)(dev)) && !isSensor(dev))
                      .reverse(),
                  },
                  {
                    title: 'Não Adicionados',
                    data: sort(
                      dataButtons.deviceStates
                        .filter(dev => getGroupFilter(deviceGroup)(dev) && !isSensor(dev))
                        .map(dev => states[dev.id])
                        .filter(dev => !list.some(item => item.id === dev.id)),
                      'name',
                    ),
                  },
                ]
          }
          renderSectionHeader={({ section: { title } }) => <Text style={styles.header}>{title}</Text>}
          renderItem={({ item, index }) => {
            const added = list.find(dev => dev.id === item.id);
            const fanButtonDevices = [
              'lightbulb',
              'speed-one',
              'speed-two',
              'speed-three',
              'direction',
              'speed-plus',
              'speed-minus',
              'ventilation',
              'exhaust',
            ];
            if (!fanButtonDevices.some(value => item.name.includes(value)) || item.name.includes('lightbulb'))
              return (
                <View style={styles.row}>
                  <View style={styles.flex1}>
                    <DeviceItem
                      device={item}
                      setDeviceState={setDeviceState}
                      listen={listen}
                      added={added}
                      changeOrder={changeOrder}
                      index={index}
                      length={list.length}
                      setItemIndexShowed={() => setItemIndexShowed(index - scrollOffset)}
                      setMultimediaDevice={setMultimediaDevice}
                      showDuplicate={item.id < 1000 && !list.find(dev => dev.id === item.id + 4000)}
                      duplicate={handleDuplicate}
                      setVerificationLumino={setVerificationLumino}
                      sensor={sensor}
                    />
                  </View>
                  <IconButton
                    style={styles.rightIcon}
                    size={18}
                    color={added ? Colors.red500 : Colors.blue500}
                    icon={
                      sensor
                        ? actions.find(action => action.code === sensor)?.sensor === item.id
                          ? 'check'
                          : ''
                        : added
                        ? 'minus'
                        : 'plus'
                    }
                    onPress={() =>
                      setTimeout(() => setList(added ? list.filter(dev => dev.id !== item.id) : [item, ...list], 0))
                    }
                  />
                </View>
              );
            return <View />;
          }}
        />
      ) : (
        <ActivityIndicator style={styles.loading} />
      )}
      <View style={{ marginVertical: isIOS ? 35 : 10 }}>
        <View style={{ alignItems: 'center', justifyContent: 'center' }}>
          <TouchableOpacity onPress={() => updateFn(statesToActions(list), offOnly)} style={styles.selectButton}>
            <Text style={styles.saveText}>{icon === 'save-outline' ? 'Salvar' : 'Próximo'}</Text>
            <Icon name={icon} size={18} color='white' />
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  flex1: { flex: 1 },
  row: { flexDirection: 'row', alignItems: 'center' },
  loading: { padding: 32 },
  rightIcon: { marginHorizontal: 0 },
  labeledCheck: { marginLeft: 10, marginRight: 10 },
  offTitle: { color: Colors.grey500 },
  listItem: { height: 56, paddingRight: 0 },
  header: { padding: 8, paddingLeft: 16, backgroundColor: Colors.grey100, marginTop: -1 },
  empty: { padding: 4, paddingLeft: 16 },
  buttonSelectAll: {
    padding: 5,
    alignItems: 'center',
    backgroundColor: theme.colors.blue500,
    borderTopWidth: 1,
    borderColor: 'white',
  },
  selectButton: {
    borderWidth: 1,
    padding: 4,
    borderRadius: 20,
    width: '35%',
    alignItems: 'center',
    backgroundColor: theme.colors.blue500,
    flexDirection: 'row',
    justifyContent: 'center',
  },
  buttonAddDelay: {
    zIndex: 2,
    top: 5,
    right: 15,
    position: 'absolute',
    flexDirection: 'row',
    alignItems: 'flex-end',
    backgroundColor: theme.colors.gray100,
  },
  upDownView: { flexDirection: 'row', justifyContent: 'center', width: 60 },
  none: { display: 'none' },
  dialog: { marginHorizontal: '25%', alignItems: 'center' },
  containerGray: { width: '100%', backgroundColor: theme.colors.gray850, flex: 1 },
  modal: { backgroundColor: 'red', position: 'absolute', top: 0, left: 0, right: 0, bottom: 0 },
  help: { justifyContent: 'center', alignItems: 'center', paddingLeft: 20 },
  helpText: { fontSize: 8, justifyContent: 'center', alignItems: 'center' },
  saveText: { color: 'white', fontSize: 13, marginRight: 4 },
  containerOpacity: { opacity: 0.4, height: '100%', width: '100%', position: 'absolute' },
  modalButton: {
    paddingVertical: 10,
    borderBottomWidth: 1,
    marginBottom: 4,
    borderBottomColor: 'lightgray',
    width: '100%',
    gap: 8,
    flexDirection: 'row',
    alignItems: 'center',
  },
  percentText: { fontSize: 10, color: theme.colors.gray700 },
  modalSelectView: { alignItems: 'center', backgroundColor: 'white', borderRadius: 10, margin: 20, padding: 20 },
  modalText: { fontSize: 12, textAlign: 'left', marginHorizontal: 5 },
});

export default Actions;
