import { useState, useEffect, createElement } from 'react';
import { TextInput } from 'react-native-paper';

// https://github.com/callstack/react-native-paper/issues/1668#issuecomment-586005762
// terrible terrible hack indeed

// Use this whenever you need a TextInput inside that appears inside of a Portal
//
// Wait what now?
// This seems to fix this bug: https://github.com/callstack/react-native-paper/issues/1668
export default function TextInputInAPortal(props) {
  const [value, setValue] = useState(props.value);

  useEffect(() => {
    if (value !== props.value) setValue(props.value);
  }, [props.value]);

  const onChangeText = text => {
    setValue(text);
    props.onChangeText(text);
  };
  return createElement(TextInput, { ...props, value, onChangeText });
}
