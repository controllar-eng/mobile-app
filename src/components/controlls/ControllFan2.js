import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React, { useEffect } from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { theme } from '../../lib/utils';

let OLD_BUTTONS = [];

export function ControllFan2({ setControll, deviceMultimedia, buttonRecord, testControll, isForm, luminos, isUpdate }) {
  const buttons = deviceMultimedia.buttons?.map(button => button.buttonName) ?? [];

  const getColorEqual = buttonName =>
    buttonRecord === buttonName
      ? theme.colors.red
      : buttons.includes(buttonName)
      ? theme.colors.blue500
      : theme.colors.gray600;
  const showLumino = n => (isUpdate ? !OLD_BUTTONS.includes(n) : isForm);

  useEffect(() => {
    if (OLD_BUTTONS.length === 0) OLD_BUTTONS = buttons;
  }, [buttons.length]);

  return (
    <View style={styles.containerModal}>
      <View style={{ width: '100%', flexDirection: 'row' }}>
        <View style={styles.container}>
          <View style={styles.rowContainer}>
            <View style={styles.fiftyContainer}>
              <TouchableOpacity
                style={[styles.buttonIconControl, { backgroundColor: getColorEqual('power-off'), width: 100 }]}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'power-off'))}
                onLongPress={() => setControll('power-off')}
              >
                <Icon size={32} color='white' name='power' />
                <Text style={{ color: 'white' }}>Off</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.fiftyContainer}>
              <TouchableOpacity
                style={[styles.buttonIconControl, { backgroundColor: theme.colors.white, width: 100 }]}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'direction'))}
                onLongPress={() => setControll('direction')}
              >
                <Icon size={32} color={getColorEqual('direction')} name='rotate-3d-variant' />
                <Text style={showLumino('direction') && styles.lumino}>
                  {showLumino('direction') ? luminos[0] : ''}
                </Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.rowContainer}>
            <View style={styles.speedContainer}>
              <View>
                <TouchableOpacity
                  style={[styles.buttonIconControl, { backgroundColor: theme.colors.white, width: 120 }]}
                  onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'speed-plus'))}
                  onLongPress={() => setControll('speed-plus')}
                >
                  <Icon size={32} color={getColorEqual('speed-plus')} name='plus' />
                  <Text style={showLumino('speed-plus') && styles.lumino}>
                    {showLumino('speed-plus') ? luminos[1] : ''}
                  </Text>
                </TouchableOpacity>
              </View>
              <View>
                <TouchableOpacity
                  style={[styles.buttonIconControl, { backgroundColor: theme.colors.white, width: 120 }]}
                  onPress={() =>
                    testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'speed-minus'))
                  }
                  onLongPress={() => setControll('speed-minus')}
                >
                  <Icon size={32} color={getColorEqual('speed-minus')} name='minus' />
                  <Text style={showLumino('speed-minus') && styles.lumino}>
                    {showLumino('speed-minus') ? luminos[2] : ''}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={[styles.rowContainer, styles.textRow]}>
            <Text style={{ color: theme.colors.black }}>Velocidade</Text>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  containerModal: {
    backgroundColor: theme.colors.gray200,
    borderRadius: 17,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 17,
    elevation: 3,
    marginVertical: 17,
  },
  buttonIconControl: {
    width: '100%',
    paddingVertical: 10,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    borderRadius: 30,
  },
  rowContainer: { width: '100%', flexDirection: 'row', marginVertical: 8 },
  fiftyContainer: { width: '50%', alignItems: 'center' },
  speedContainer: {
    borderWidth: 1,
    flexDirection: 'row',
    width: '76%',
    marginHorizontal: '12%',
    justifyContent: 'space-between',
    padding: 10,
    borderRadius: 30,
    borderColor: theme.colors.gray600,
  },
  container: { alignItems: 'center', justifyContent: 'space-around', width: '100%' },
  textRow: { justifyContent: 'center', marginTop: 0 },
  lumino: { position: 'absolute', fontSize: 12, color: theme.colors.red, bottom: -1 },
});
