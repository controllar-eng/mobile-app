import React from 'react';

import { ControllAir } from './ControllAir';
import { ControllFan1 } from './ControllFan1';
import { ControllFan2 } from './ControllFan2';
import { ControllTV } from './ControllTV';
import { ControllTV1 } from './ControllTV1';
import { ControllTV2 } from './ControllTV2';
import { ControllTV3 } from './ControllTV3';
import { ControllTV4 } from './ControllTV4';
import { ControllWindow } from './ControllWindow';
import { ControllGate } from './ControllGate';
import { ControllAir1 } from './ControllAir1';

export const createControlls = (
  testControll,
  multimediaDevice,
  setModalCopy,
  buttonRecord,
  buttonLuminos,
  device,
  isOpemControll,
  controllType,
  computedData,
  isForm,
) => {
  const commonProps = { testControll, deviceMultimedia: multimediaDevice, buttonRecord };

  const controllGate = (
    <ControllGate {...commonProps} setControll={data => setModalCopy(data)} controllType={controllType} />
  );

  return {
    TV: <ControllTV {...commonProps} setControll={data => isOpemControll && setModalCopy(data)} />,
    AIR: <ControllAir {...commonProps} setControll={data => setModalCopy(data)} isForm={isForm} />,
    GATE: controllGate,
    RF_DEFAULT: controllGate,
    WINDOW: <ControllWindow {...commonProps} setControll={data => setModalCopy(data)} luminos={buttonLuminos} />,
    TV_1: <ControllTV1 {...commonProps} setControll={data => isOpemControll && setModalCopy(data)} />,
    TV_2: <ControllTV2 {...commonProps} setControll={data => isOpemControll && setModalCopy(data)} />,
    TV_3: <ControllTV3 {...commonProps} setControll={data => isOpemControll && setModalCopy(data)} />,
    TV_4: <ControllTV4 {...commonProps} setControll={data => isOpemControll && setModalCopy(data)} />,
    AIR_1: <ControllAir1 {...commonProps} setControll={data => setModalCopy(data)} isForm={isForm} />,
    FANRF_1: (
      <ControllFan1
        {...commonProps}
        setControll={data => setModalCopy(data)}
        isForm={isForm}
        luminos={buttonLuminos}
        isUpdate={device}
        computedData={computedData}
      />
    ),
    FANIR_1: (
      <ControllFan1
        {...commonProps}
        setControll={data => setModalCopy(data)}
        isForm={isForm}
        luminos={buttonLuminos}
        isUpdate={device}
        computedData={computedData}
      />
    ),
    FANRF_2: (
      <ControllFan2
        {...commonProps}
        setControll={data => setModalCopy(data)}
        isForm={isForm}
        luminos={buttonLuminos}
        isUpdate={device}
      />
    ),
    FANIR_2: (
      <ControllFan2
        {...commonProps}
        setControll={data => setModalCopy(data)}
        isForm={isForm}
        luminos={buttonLuminos}
        isUpdate={device}
      />
    ),
    OPEN_CURTAIN: controllGate,
    CLOSE_CURTAIN: controllGate,
    STOP_CURTAIN: controllGate,
    OPEN_SHUTTER1: controllGate,
    CLOSE_SHUTTER1: controllGate,
    STOP_SHUTTER1: controllGate,
    OPEN_SHUTTER2: controllGate,
    CLOSE_SHUTTER2: controllGate,
    STOP_SHUTTER2: controllGate,
  };
};
