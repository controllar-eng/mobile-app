import { Dimensions, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React, { useState } from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { theme } from '../../lib/utils';

export function ControllTV({ setControll, deviceMultimedia, buttonRecord, testControll }) {
  const [dimensions] = useState(Dimensions.get('window'));

  const buttons = deviceMultimedia.buttons?.map(button => button.buttonName) ?? [];

  const getColorEqual = buttonName =>
    buttonRecord === buttonName
      ? theme.colors.red
      : buttons.includes(buttonName)
      ? theme.colors.blue500
      : theme.colors.gray600;

  return (
    <View style={styles.containerModal}>
      <View style={{ width: '100%', flexDirection: 'row' }}>
        <View style={{ alignItems: 'center', justifyContent: 'space-between', width: '45%' }}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.containerButton}>
              <View style={[styles.boxButtonChannelVol, styles.boxShadow]}>
                <Icon
                  size={dimensions.height < 720 ? 32 : 36}
                  color={getColorEqual('up-channel')}
                  name='chevron-up'
                  onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'up-channel'))}
                  onLongPress={() => setControll('up-channel')}
                />
                <Text style={styles.textVolumeChannel}>CH</Text>
                <Icon
                  size={dimensions.height < 720 ? 32 : 36}
                  color={getColorEqual('down-channel')}
                  name='chevron-down'
                  onPress={() =>
                    testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'down-channel'))
                  }
                  onLongPress={() => setControll('down-channel')}
                />
              </View>
            </View>
            <View style={styles.containerButton}>
              <View style={[styles.boxButtonChannelVol, styles.boxShadow]}>
                <Icon
                  size={dimensions.height < 720 ? 32 : 36}
                  color={getColorEqual('volume-plus')}
                  name='volume-plus'
                  onPress={() =>
                    testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'volume-plus'))
                  }
                  onLongPress={() => setControll('volume-plus')}
                />
                <Text style={styles.textVolumeChannel}>VOL</Text>
                <Icon
                  size={dimensions.height < 720 ? 32 : 36}
                  color={getColorEqual('volume-minus')}
                  name='volume-minus'
                  onPress={() =>
                    testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'volume-minus'))
                  }
                  onLongPress={() => setControll('volume-minus')}
                />
              </View>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.containerButton}>
              <View style={[styles.containerIconControl, styles.boxShadow, { width: '80%' }]}>
                <TouchableOpacity
                  style={styles.buttonIconControl}
                  onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'mute'))}
                  onLongPress={() => setControll('mute')}
                >
                  <Icon size={dimensions.height < 720 ? 22 : 26} color={getColorEqual('mute')} name='volume-mute' />
                  <Text style={styles.textButtonControl}>Mudo</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.containerButton}>
              <View style={[styles.containerIconControl, styles.boxShadow, { width: '80%' }]}>
                <TouchableOpacity
                  style={styles.buttonIconControl}
                  onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'list'))}
                  onLongPress={() => setControll('list')}
                >
                  <Icon
                    size={dimensions.height < 720 ? 22 : 26}
                    color={getColorEqual('list')}
                    name='view-list-outline'
                  />
                  <Text style={styles.textButtonControl}>Lista</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.containerButton}>
              <View style={[styles.containerIconControl, styles.boxShadow, { width: '80%' }]}>
                <TouchableOpacity
                  style={styles.buttonIconControl}
                  onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'back'))}
                  onLongPress={() => setControll('back')}
                >
                  <Icon size={dimensions.height < 720 ? 20 : 26} color={getColorEqual('back')} name='keyboard-return' />
                  <Text style={styles.textButtonControl}>Voltar</Text>
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.containerButton}>
              <View style={[styles.containerIconControl, styles.boxShadow, { width: '80%' }]}>
                <TouchableOpacity
                  style={styles.buttonIconControl}
                  onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'source'))}
                  onLongPress={() => setControll('source')}
                >
                  <Icon size={dimensions.height < 720 ? 18 : 26} color={getColorEqual('source')} name='shield-home' />
                  <Text style={styles.textButtonControl}>Source</Text>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </View>
        <View style={{ alignItems: 'center', justifyContent: 'space-around', width: '55%' }}>
          <View
            style={[
              styles.circle,
              styles.boxShadow,
              dimensions.height < 720 ? { width: 150, height: 150 } : { width: 170, height: 170 },
            ]}
          >
            <View style={styles.upDownIcons}>
              <Icon
                size={dimensions.height < 720 ? 32 : 36}
                color={getColorEqual('chevron-up')}
                name='chevron-up'
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'chevron-up'))}
                onLongPress={() => setControll('chevron-up')}
              />
            </View>
            <View style={styles.medCircle}>
              <View style={{ marginRight: 20 }}>
                <Icon
                  size={dimensions.height < 720 ? 32 : 36}
                  color={getColorEqual('chevron-left')}
                  name='chevron-left'
                  onPress={() =>
                    testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'chevron-left'))
                  }
                  onLongPress={() => setControll('chevron-left')}
                />
              </View>
              <View style={[styles.okContainer, { backgroundColor: getColorEqual('ok') }]}>
                <TouchableOpacity
                  style={styles.okButton}
                  onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'ok'))}
                  onLongPress={() => setControll('ok')}
                >
                  <Text style={{ fontSize: 11, fontWeight: 'bold', color: theme.colors.gray100 }}>OK</Text>
                </TouchableOpacity>
              </View>
              <View style={{ marginLeft: 20 }}>
                <Icon
                  size={dimensions.height < 720 ? 32 : 36}
                  color={getColorEqual('chevron-right')}
                  name='chevron-right'
                  onPress={() =>
                    testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'chevron-right'))
                  }
                  onLongPress={() => setControll('chevron-right')}
                />
              </View>
            </View>
            <View style={styles.upDownIcons}>
              <Icon
                size={dimensions.height < 720 ? 32 : 36}
                color={getColorEqual('chevron-down')}
                name='chevron-down'
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'chevron-down'))}
                onLongPress={() => setControll('chevron-down')}
              />
            </View>
          </View>

          <View style={styles.powerRow}>
            <View
              style={[
                styles.containerIconControl,
                styles.boxShadow,
                {
                  width: '40%',
                  marginRight: 10,
                },
              ]}
            >
              <TouchableOpacity
                style={styles.buttonIconControl}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'power'))}
                onLongPress={() => setControll('power')}
              >
                <Icon size={dimensions.height < 720 ? 24 : 30} color={getColorEqual('power')} name='power' />
                <Text style={styles.textButtonControl}>Ligar</Text>
              </TouchableOpacity>
            </View>
            <View style={[styles.containerIconControl, styles.boxShadow, { width: '40%' }]}>
              <TouchableOpacity
                style={styles.buttonIconControl}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'power-off'))}
                onLongPress={() => setControll('power-off')}
              >
                <Icon size={dimensions.height < 720 ? 24 : 30} color={getColorEqual('power-off')} name='power' />
                <Text style={styles.textButtonControl}>Desligar</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  containerModal: {
    backgroundColor: theme.colors.gray200,
    borderRadius: 17,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 17,
    elevation: 3,
    marginVertical: 17,
    paddingBottom: 17,
  },
  boxButtonChannelVol: {
    backgroundColor: theme.colors.gray100,
    marginLeft: 30,
    borderRadius: 10,
    marginRight: 30,
    width: '80%',
    alignItems: 'center',
  },
  okContainer: { width: 50, height: 50, borderRadius: 50, justifyContent: 'center', alignItems: 'center' },
  boxShadow: { elevation: 3, shadowColor: theme.colors.gray600 },
  upDownIcons: { width: '100%', alignItems: 'center' },
  textButtonControl: { color: theme.colors.black, fontSize: 10 },
  containerButton: { width: '50%', marginTop: 17, alignItems: 'center' },
  containerIconControl: { alignItems: 'center', backgroundColor: theme.colors.gray100, borderRadius: 5 },
  buttonIconControl: {
    width: '100%',
    paddingBottom: 8,
    paddingTop: 8,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  circle: { justifyContent: 'space-between', borderRadius: 90, backgroundColor: theme.colors.gray100 },
  medCircle: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' },
  powerRow: {
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  textVolumeChannel: { marginTop: 3, marginBottom: 3, fontSize: 14, fontWeight: 'bold' },
  okButton: { alignItems: 'center', flex: 1, justifyContent: 'center', width: '100%' },
});
