import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React, { useEffect } from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { theme } from '../../lib/utils';

let OLD_BUTTONS = [];

export function ControllFan1({
  setControll,
  deviceMultimedia,
  buttonRecord,
  testControll,
  isForm,
  luminos,
  isUpdate,
  computedData,
}) {
  const buttons = deviceMultimedia.buttons?.map(button => button.buttonName) ?? [];
  const l = 'lightbulb';
  const o = 'outline';

  const getColorEqual = buttonName =>
    buttonRecord === buttonName
      ? theme.colors.red
      : buttons.includes(buttonName)
      ? theme.colors.blue500
      : theme.colors.gray600;
  const lightOn = () =>
    computedData?.filter(
      data => data.name === l || (data.name.includes('lightbulb') && data.name.includes(deviceMultimedia.id)),
    )[0]?.on;
  const showLumino = n => (isUpdate ? !OLD_BUTTONS.includes(n) : isForm);

  useEffect(() => {
    if (OLD_BUTTONS.length === 0) OLD_BUTTONS = buttons;
  }, [buttons.length]);

  return (
    <View style={styles.containerModal}>
      <View style={{ width: '100%', flexDirection: 'row' }}>
        <View style={styles.container}>
          <View style={styles.rowContainer}>
            <View style={styles.fiftyContainer}>
              <TouchableOpacity
                style={[styles.buttonIconControl, { backgroundColor: getColorEqual('power-off'), width: 100 }]}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'power-off'))}
                onLongPress={() => setControll('power-off')}
              >
                <Icon size={32} color='white' name='power' />
                <Text style={{ color: 'white' }}>Off</Text>
              </TouchableOpacity>
            </View>
            <View style={styles.fiftyContainer}>
              <TouchableOpacity
                style={[
                  styles.buttonIconControl,
                  { width: 100 },
                  { backgroundColor: isForm ? theme.colors.white : lightOn() ? 'yellow' : 'white' },
                ]}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === l))}
                onLongPress={() => setControll(l)}
              >
                <Icon
                  size={32}
                  color={
                    isForm
                      ? getColorEqual(l)
                      : lightOn()
                      ? 'black'
                      : buttons.includes(l)
                      ? theme.colors.blue500
                      : theme.colors.gray600
                  }
                  name={
                    computedData
                      ? lightOn()
                        ? `${l}-on-${o}`
                        : buttons.includes(l)
                        ? `${l}-off-${o}`
                        : `${l}-${o}`
                      : `${l}-${o}`
                  }
                />
                <View style={showLumino('lightbulb') && styles.lumino}>
                  <Text style={{ fontSize: 12, color: theme.colors.red }}>
                    {showLumino('lightbulb') ? luminos[0] : ''}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.rowContainer}>
            <View style={styles.speedContainer}>
              <View>
                <TouchableOpacity
                  style={[styles.buttonIconControl, { backgroundColor: getColorEqual('speed-one'), width: 80 }]}
                  onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'speed-one'))}
                  onLongPress={() => setControll('speed-one')}
                >
                  <Icon size={32} color='white' name='numeric-1-circle-outline' />
                  <View style={showLumino('speed-one') && styles.lumino}>
                    <Text style={{ fontSize: 12, color: theme.colors.red }}>
                      {showLumino('speed-one') ? luminos[1] : ''}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View>
                <TouchableOpacity
                  style={[styles.buttonIconControl, { backgroundColor: getColorEqual('speed-two'), width: 80 }]}
                  onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'speed-two'))}
                  onLongPress={() => setControll('speed-two')}
                >
                  <Icon size={32} color='white' name='numeric-2-circle-outline' />
                  <View style={showLumino('speed-two') && styles.lumino}>
                    <Text style={{ fontSize: 12, color: theme.colors.red }}>
                      {showLumino('speed-two') ? luminos[2] : ''}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View>
                <TouchableOpacity
                  style={[styles.buttonIconControl, { backgroundColor: getColorEqual('speed-three'), width: 80 }]}
                  onPress={() =>
                    testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'speed-three'))
                  }
                  onLongPress={() => setControll('speed-three')}
                >
                  <Icon size={32} color='white' name='numeric-3-circle-outline' />
                  <View style={showLumino('speed-three') && styles.lumino}>
                    <Text style={{ fontSize: 12, color: theme.colors.red }}>
                      {showLumino('speed-three') ? luminos[3] : ''}
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={[styles.rowContainer, styles.textRow]}>
            <Text style={{ color: theme.colors.gray600 }}>Mínimo</Text>
            <Text style={{ color: theme.colors.gray600 }}>Maximo</Text>
          </View>
          <View style={[styles.rowContainer, styles.ventilationRow]}>
            <View style={styles.fiftyContainer}>
              <TouchableOpacity
                style={[styles.buttonIconControl, { backgroundColor: getColorEqual('ventilation'), width: '96%' }]}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'ventilation'))}
                onLongPress={() => setControll('ventilation')}
              >
                <Text style={styles.ventExhausText}>Ventilação</Text>
                <View style={showLumino('ventilation') && styles.lumino}>
                  <Text style={{ fontSize: 12, color: theme.colors.red }}>
                    {showLumino('ventilation') ? luminos[4] : ''}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={styles.fiftyContainer}>
              <TouchableOpacity
                style={[styles.buttonIconControl, { backgroundColor: getColorEqual('exhaust'), width: '96%' }]}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'exhaust'))}
                onLongPress={() => setControll('exhaust')}
              >
                <Text style={styles.ventExhausText}>Exaustão</Text>
                <View style={showLumino('exhaust') && styles.lumino}>
                  <Text style={{ fontSize: 12, color: theme.colors.red }}>
                    {showLumino('exhaust') ? luminos[5] : ''}
                  </Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  containerModal: {
    backgroundColor: theme.colors.gray200,
    borderRadius: 17,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 17,
    elevation: 3,
    marginVertical: 17,
  },
  buttonIconControl: {
    width: '100%',
    paddingVertical: 7,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    borderRadius: 30,
  },
  rowContainer: { width: '100%', flexDirection: 'row', marginVertical: 8 },
  fiftyContainer: { width: '50%', alignItems: 'center' },
  speedContainer: {
    borderWidth: 1,
    flexDirection: 'row',
    width: '76%',
    marginHorizontal: '12%',
    justifyContent: 'space-between',
    padding: 10,
    borderRadius: 30,
    borderColor: theme.colors.gray600,
  },
  container: { alignItems: 'center', justifyContent: 'space-around', width: '100%' },
  textRow: { justifyContent: 'space-between', marginHorizontal: '15%', width: '70%', marginTop: 0 },
  ventilationRow: { justifyContent: 'space-between', marginHorizontal: '12%', width: '76%', marginTop: 0 },
  lumino: {
    position: 'absolute',
    bottom: -8,
    backgroundColor: theme.colors.white,
    borderRadius: 30,
    paddingHorizontal: 5,
    borderWidth: 1,
    borderColor: theme.colors.gray600,
  },
  ventExhausText: { color: 'white', padding: 7, fontSize: 16 },
});
