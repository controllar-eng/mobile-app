import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { theme } from '../../lib/utils';

export function ControllWindow({ setControll, deviceMultimedia, buttonRecord, testControll, luminos }) {
  const buttons = deviceMultimedia.buttons?.map(button => button.buttonName) ?? [];

  const getColorEqual = buttonName =>
    buttonRecord === buttonName
      ? theme.colors.red
      : buttons.includes(buttonName)
      ? theme.colors.blue500
      : theme.colors.gray600;

  return (
    <View style={styles.containerModal}>
      <View style={styles.buttonsRow}>
        <TouchableOpacity
          style={[styles.containerIconControl, styles.boxShadow]}
          onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'power'))}
          onLongPress={() => setControll('power')}
        >
          <Icon size={60} color={getColorEqual('power')} name='chevron-up' />
          <Text style={styles.lumino}>{luminos[0] ?? ''}</Text>
        </TouchableOpacity>
        <Text style={styles.textButtonControl}>Subir/Abrir</Text>
      </View>
      <View style={styles.buttonsRow}>
        <TouchableOpacity
          style={[styles.containerIconControl, styles.boxShadow]}
          onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'stop'))}
          onLongPress={() => setControll('stop')}
        >
          <Icon size={60} color={getColorEqual('stop')} name='stop-circle-outline' />
          <Text style={styles.lumino}>{luminos[1] ?? ''}</Text>
        </TouchableOpacity>
        <Text style={styles.textButtonControl}>Parar</Text>
      </View>
      <View style={styles.buttonsRow}>
        <TouchableOpacity
          style={[styles.containerIconControl, styles.boxShadow]}
          onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'power-off'))}
          onLongPress={() => setControll('power-off')}
        >
          <Icon size={60} color={getColorEqual('power-off')} name='chevron-down' />
          <Text style={styles.lumino}>{luminos[2] ?? ''}</Text>
        </TouchableOpacity>
        <Text style={styles.textButtonControl}>Descer/Fechar</Text>
      </View>
    </View>
  );
}
const styles = StyleSheet.create({
  containerModal: {
    backgroundColor: theme.colors.gray200,
    borderRadius: 17,
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 17,
    flexDirection: 'row',
    padding: 22,
    elevation: 3,
  },
  containerIconControl: {
    alignItems: 'center',
    backgroundColor: theme.colors.gray100,
    borderRadius: 45,
    width: 90,
    height: 90,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 8,
  },
  boxShadow: { elevation: 3, shadowColor: theme.colors.gray600 },
  textButtonControl: { fontSize: 13 },
  buttonsRow: { justifyContent: 'center', alignItems: 'center' },
  lumino: { position: 'absolute', bottom: 0, fontSize: 12, color: theme.colors.red },
});
