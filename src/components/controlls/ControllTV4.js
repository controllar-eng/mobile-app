import { StyleSheet, Text, TouchableOpacity, View, ScrollView, Dimensions, Image } from 'react-native';
import React, { useState } from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import IconIon from 'react-native-vector-icons/Ionicons';
import { theme } from '../../lib/utils';
import netflix from '../../assets/netflix.png';
import netflixWhite from '../../assets/netflixWhite.png';
import netflixGray from '../../assets/netflixGray.png';
import youtube from '../../assets/youtube.png';
import youtubeRed from '../../assets/youtubeRed.png';
import youtubeGray from '../../assets/youtubeGray.png';
import assistent from '../../assets/assistent.png';
import assistentRed from '../../assets/assistentRed.png';
import assistentGray from '../../assets/assistentGray.png';

export function ControllTV4({ setControll, deviceMultimedia, buttonRecord, testControll }) {
  const [{ width }] = useState(Dimensions.get('window'));
  const [slide, setSlide] = useState(0);

  const buttons = deviceMultimedia.buttons?.map(button => button.buttonName) ?? [];

  const getColorEqual = buttonName =>
    buttonRecord === buttonName
      ? theme.colors.red
      : buttons.includes(buttonName)
      ? theme.colors.blue500
      : theme.colors.gray600;

  return (
    <View style={styles.containerModal}>
      <View style={{ width: '100%' }}>
        <View style={styles.rowContainer}>
          <View style={styles.thirtyContainer}>
            <TouchableOpacity
              style={[styles.buttonIconControl, { backgroundColor: getColorEqual('power') }]}
              onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'power'))}
              onLongPress={() => setControll('power')}
            >
              <Icon size={26} color='white' name='power' />
            </TouchableOpacity>
          </View>
          <View style={styles.thirtyContainer}>
            <TouchableOpacity
              style={[styles.buttonIconControl, { backgroundColor: theme.colors.white, height: 42 }]}
              onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'assistent'))}
              onLongPress={() => setControll('assistent')}
            >
              <Image
                source={
                  buttonRecord === 'assistent'
                    ? assistentRed
                    : buttons.includes('assistent')
                    ? assistent
                    : assistentGray
                }
              />
            </TouchableOpacity>
          </View>
          <View style={styles.thirtyContainer}>
            <TouchableOpacity
              style={[styles.buttonIconControl, { backgroundColor: theme.colors.white }]}
              onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'settings'))}
              onLongPress={() => setControll('settings')}
            >
              <IconIon size={26} color={getColorEqual('settings')} name='ios-settings-outline' />
            </TouchableOpacity>
          </View>
        </View>
        <View style={styles.circleContainer}>
          <View style={styles.circle}>
            <View style={styles.upDownIcons}>
              <Icon
                size={34}
                color={getColorEqual('chevron-up')}
                name='chevron-up'
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'chevron-up'))}
                onLongPress={() => setControll('chevron-up')}
              />
            </View>
            <View style={styles.medCircle}>
              <View style={{ marginRight: 20 }}>
                <Icon
                  size={34}
                  color={getColorEqual('chevron-left')}
                  name='chevron-left'
                  onPress={() =>
                    testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'chevron-left'))
                  }
                  onLongPress={() => setControll('chevron-left')}
                />
              </View>
              <View style={[styles.okContainer, { backgroundColor: getColorEqual('ok') }]}>
                <TouchableOpacity
                  style={styles.okButton}
                  onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'ok'))}
                  onLongPress={() => setControll('ok')}
                >
                  <Text style={styles.okText}>OK</Text>
                </TouchableOpacity>
              </View>
              <View style={{ marginLeft: 20 }}>
                <Icon
                  size={34}
                  color={getColorEqual('chevron-right')}
                  name='chevron-right'
                  onPress={() =>
                    testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'chevron-right'))
                  }
                  onLongPress={() => setControll('chevron-right')}
                />
              </View>
            </View>
            <View style={styles.upDownIcons}>
              <Icon
                size={34}
                color={getColorEqual('chevron-down')}
                name='chevron-down'
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'chevron-down'))}
                onLongPress={() => setControll('chevron-down')}
              />
            </View>
          </View>
        </View>
        <View style={styles.rowContainer}>
          <View style={styles.fiftyPercentContainer}>
            <View style={styles.volumeChannelControll}>
              <Icon
                size={34}
                color={getColorEqual('volume-minus')}
                name='minus'
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'volume-minus'))}
                onLongPress={() => setControll('volume-minus')}
              />
              <Text style={styles.textVolumeChannel}>VOL</Text>
              <Icon
                size={34}
                color={getColorEqual('volume-plus')}
                name='plus'
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'volume-plus'))}
                onLongPress={() => setControll('volume-plus')}
              />
            </View>
          </View>
          <View style={styles.fiftyPercentContainer}>
            <View style={styles.volumeChannelControll}>
              <Icon
                size={34}
                color={getColorEqual('down-channel')}
                name='chevron-down'
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'down-channel'))}
                onLongPress={() => setControll('down-channel')}
              />
              <Text style={styles.textVolumeChannel}>CH</Text>
              <Icon
                size={34}
                color={getColorEqual('up-channel')}
                name='chevron-up'
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'up-channel'))}
                onLongPress={() => setControll('up-channel')}
              />
            </View>
          </View>
        </View>
        <View style={styles.rowContainer}>
          <View style={styles.fiftyPercentContainer}>
            <View style={styles.volumeChannelControll}>
              <TouchableOpacity
                style={[styles.buttonIconControl, { backgroundColor: theme.colors.white }]}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'mute'))}
                onLongPress={() => setControll('mute')}
              >
                <IconIon size={22} color={getColorEqual('mute')} name='volume-mute' />
                <Text style={styles.textVolumeChannel}>Mudo</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.fiftyPercentContainer}>
            <View style={styles.volumeChannelControll}>
              <TouchableOpacity
                style={[styles.buttonIconControl, { backgroundColor: theme.colors.white }]}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'list'))}
                onLongPress={() => setControll('list')}
              >
                <Icon size={22} color={getColorEqual('list')} name='view-list-outline' />
                <Text style={styles.textVolumeChannel}>Lista</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <View style={styles.rowContainer}>
          <View style={styles.fiftyPercentContainer}>
            <View style={styles.volumeChannelControll}>
              <TouchableOpacity
                style={[styles.buttonIconControl, { backgroundColor: theme.colors.white }]}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'back'))}
                onLongPress={() => setControll('back')}
              >
                <IconIon size={22} color={getColorEqual('back')} name='ios-return-up-back' />
                <Text style={styles.textVolumeChannel}>Voltar</Text>
              </TouchableOpacity>
            </View>
          </View>
          <View style={styles.fiftyPercentContainer}>
            <View style={styles.volumeChannelControll}>
              <TouchableOpacity
                style={[styles.buttonIconControl, { backgroundColor: theme.colors.white }]}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'home'))}
                onLongPress={() => setControll('home')}
              >
                <IconIon size={22} color={getColorEqual('home')} name='home-outline' />
                <Text style={styles.textVolumeChannel}>Home</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
        <ScrollView
          showsHorizontalScrollIndicator={false}
          horizontal
          style={{ marginTop: 5 }}
          onScroll={({ nativeEvent }) => setSlide(nativeEvent.contentOffset.x >= width / 2 ? 1 : 0)}
        >
          <View style={{ flexDirection: 'row', width: width - 34, paddingHorizontal: '2%' }}>
            <View style={styles.fourtyContainer}>
              <TouchableOpacity
                style={[styles.buttonIconControl, { backgroundColor: theme.colors.white }]}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'slow'))}
                onLongPress={() => setControll('slow')}
              >
                <Icon size={26} color={getColorEqual('slow')} name='skip-backward' />
              </TouchableOpacity>
            </View>
            <View style={styles.fourtyContainer}>
              <TouchableOpacity
                style={[styles.buttonIconControl, { backgroundColor: theme.colors.white }]}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'pause'))}
                onLongPress={() => setControll('pause')}
              >
                <Icon size={26} color={getColorEqual('pause')} name='pause' />
              </TouchableOpacity>
            </View>
            <View style={styles.fourtyContainer}>
              <TouchableOpacity
                style={[styles.buttonIconControl, { backgroundColor: theme.colors.white }]}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'play'))}
                onLongPress={() => setControll('play')}
              >
                <Icon size={26} color={getColorEqual('play')} name='play' />
              </TouchableOpacity>
            </View>
            <View style={styles.fourtyContainer}>
              <TouchableOpacity
                style={[styles.buttonIconControl, { backgroundColor: theme.colors.white }]}
                onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'fast'))}
                onLongPress={() => setControll('fast')}
              >
                <Icon size={26} color={getColorEqual('fast')} name='skip-forward' />
              </TouchableOpacity>
            </View>
          </View>
          <View style={{ flexDirection: 'row', width: width - 34 }}>
            <View style={styles.fiftyPercentContainer}>
              <View style={styles.volumeChannelControll}>
                <TouchableOpacity
                  onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'netflix'))}
                  onLongPress={() => setControll('netflix')}
                  style={buttonRecord === 'netflix' && styles.recordNetflix}
                >
                  <Image
                    source={
                      buttonRecord === 'netflix' ? netflixWhite : buttons.includes('netflix') ? netflix : netflixGray
                    }
                    style={{ width: 85, height: 35 }}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={styles.fiftyPercentContainer}>
              <View style={styles.volumeChannelControll}>
                <TouchableOpacity
                  onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'youtube'))}
                  onLongPress={() => setControll('youtube')}
                >
                  <Image
                    source={
                      buttonRecord === 'youtube' ? youtubeRed : buttons.includes('youtube') ? youtube : youtubeGray
                    }
                    style={{ width: 90, height: 25 }}
                  />
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
        <View style={styles.containerSlide}>
          <Text style={styles.textSlide}> {slide ? '⚪⚫' : '⚫⚪'}</Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  containerModal: {
    backgroundColor: theme.colors.gray200,
    borderRadius: 17,
    justifyContent: 'space-between',
    alignItems: 'center',
    marginHorizontal: 17,
    elevation: 3,
    marginVertical: 17,
  },
  okContainer: { width: 70, height: 70, borderRadius: 35, justifyContent: 'center', alignItems: 'center' },
  boxShadow: { elevation: 3, shadowColor: theme.colors.gray600 },
  upDownIcons: { width: '100%', alignItems: 'center' },
  buttonIconControl: {
    width: '100%',
    paddingBottom: 5,
    paddingTop: 5,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'center',
    width: 60,
    borderRadius: 20,
  },
  circle: {
    justifyContent: 'space-between',
    borderRadius: 85,
    backgroundColor: theme.colors.gray100,
    width: 170,
    height: 170,
    elevation: 3,
    shadowColor: theme.colors.gray600,
  },
  medCircle: { flexDirection: 'row', alignItems: 'center', justifyContent: 'center', width: '100%' },
  textVolumeChannel: { marginHorizontal: 3, fontSize: 14, fontWeight: 'bold' },
  okButton: { alignItems: 'center', flex: 1, justifyContent: 'center', width: '100%' },
  volumeChannelControll: {
    flexDirection: 'row',
    backgroundColor: theme.colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    width: 130,
    padding: 5,
    borderRadius: 20,
    height: 40,
  },
  rowContainer: { width: '100%', flexDirection: 'row', marginVertical: 5 },
  circleContainer: { alignItems: 'center', justifyContent: 'space-around' },
  fiftyPercentContainer: { width: '50%', alignItems: 'center' },
  thirtyContainer: { width: '33%', alignItems: 'center' },
  okText: { fontSize: 18, fontWeight: 'bold', color: theme.colors.gray100 },
  fourtyContainer: { width: '25%', alignItems: 'center' },
  recordNetflix: { backgroundColor: theme.colors.red, width: '100%', alignItems: 'center', borderRadius: 15 },
  containerSlide: { height: 20, alignItems: 'center', justifyContent: 'center' },
  textSlide: { fontSize: 10, color: theme.colors.gray600 },
});
