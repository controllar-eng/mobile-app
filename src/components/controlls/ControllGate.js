import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { theme } from '../../lib/utils';
import { INDICATORS } from '../device/DeviceItem';

export function ControllGate({ setControll, deviceMultimedia, buttonRecord, testControll, controllType }) {
  const buttons = deviceMultimedia.buttons?.map(button => button.buttonName) ?? [];

  const iconNames = s =>
    s.includes('OPEN') ? 'Abrir' : s.includes('CLOSE') ? 'Fechar' : s.includes('STOP') ? 'Parar' : 'Botão';

  const IconComp = controllType === 'GATE' ? Icon : INDICATORS[controllType].comp;
  const props = controllType === 'GATE' ? { name: 'lock-open' } : { ...INDICATORS[controllType]['on'] };

  return (
    <View style={styles.containerModal}>
      <TouchableOpacity
        style={[styles.containerIconControl, styles.boxShadow]}
        onPress={() => testControll(deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'power-off'))}
        onLongPress={() => setControll('power-off')}
      >
        <IconComp
          size={40}
          color={
            buttonRecord === 'power-off'
              ? theme.colors.red
              : buttons.includes('power-off')
              ? theme.colors.blue500
              : theme.colors.gray600
          }
          {...props}
        />
        {controllType === 'GATE' ? (
          <View>
            <Text style={styles.textButtonControl}>Abrir /</Text>
            <Text style={styles.textButtonControl}>Fechar</Text>
          </View>
        ) : (
          <Text style={styles.textButtonControl}>{iconNames(controllType)}</Text>
        )}
      </TouchableOpacity>
    </View>
  );
}
const styles = StyleSheet.create({
  containerModal: {
    backgroundColor: theme.colors.gray200,
    borderRadius: 17,
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 17,
    paddingBottom: 17,
  },
  containerIconControl: {
    alignItems: 'center',
    backgroundColor: theme.colors.gray100,
    borderRadius: 50,
    width: 100,
    marginTop: 5,
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
  },
  boxShadow: { elevation: 3, shadowColor: theme.colors.gray600 },
});
