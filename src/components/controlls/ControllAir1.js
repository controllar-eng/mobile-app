import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import React, { useEffect, useState } from 'react';
import { Button, Dialog, Portal } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { theme } from '../../lib/utils';
import TextInputInAPortal from '../TextInputInPortal';
import KeyboardAwareDialog from '../KeyboardAwareDialog';

const CustomDialog = Platform.OS === 'ios' ? KeyboardAwareDialog : Dialog;

export function ControllAir1({ setControll, deviceMultimedia, buttonRecord, testControll, isForm }) {
  const [temperatures, setTemperatures] = useState([]);
  const [editTemperature, setEditTemperature] = useState();
  const [temperature, setTemperature] = useState();
  const [tempType, setTempType] = useState('cold');

  const [intensity, setIntensity] = useState();

  const buttons =
    deviceMultimedia.buttons?.map(({ buttonName }) =>
      buttonName.includes('temp')
        ? buttonName.includes('twenty')
          ? buttonName.split('-')[0] + '-' + buttonName.split('-')[1] + '-' + buttonName.split('-')[2]
          : buttonName.split('-')[0] + '-' + buttonName.split('-')[1]
        : buttonName,
    ) ?? [];

  const getColorEqual = buttonName =>
    buttonRecord === buttonName
      ? theme.colors.red
      : buttons.includes(buttonName)
      ? theme.colors.blue500
      : theme.colors.gray600;

  const getColorIncludes = buttonName =>
    buttonRecord?.includes(buttonName)
      ? theme.colors.red
      : buttons.includes(buttonName)
      ? theme.colors.blue500
      : theme.colors.gray600;
  useEffect(() => {
    let isOld = false;
    let temperatures = [15, 19, 22, 30];

    if (deviceMultimedia.buttons.length)
      deviceMultimedia.buttons.map(button => {
        let buttonSplited = button.buttonName.split('-');
        const temperature = buttonSplited[buttonSplited.length - 1];
        if (temperature / 1 > 0) {
          const textToNumber = { fifteen: 0, nineteen: 1, twenty: 2, thirty: 3 };
          temperatures[textToNumber[buttonSplited[1]]] = parseInt(temperature);
        } else isOld = true;
      });
    const position = { 15: 0, 19: 1, 22: 2, 30: 3 };
    setIntensity(temperatures[position[deviceMultimedia.intensity]]);
    setTemperatures(temperatures);
  }, []);

  const getButton = name => deviceMultimedia.buttons.find(({ buttonName: n }) => n.includes(name));

  return (
    <View style={styles.containerModal}>
      <View style={{ flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
        <View>
          <TouchableOpacity
            style={[styles.temperatureIcons, styles.boxShadow, { marginRight: 4 }]}
            onPress={() => {
              const button =
                deviceMultimedia.buttons[
                  deviceMultimedia.buttons
                    .map(function (e) {
                      return e.buttonName;
                    })
                    .indexOf('power')
                ];
              testControll(button);
              button != undefined && setIntensity(50);
            }}
            onLongPress={() => setControll('power')}
          >
            <Icon color={getColorEqual('power')} size={25} name='power' style={{ paddingRight: 5 }} />
            <Text>{' Ligar  '}</Text>
          </TouchableOpacity>
        </View>
        <View style={[styles.temperatureCenter]}>
          <Text style={{ fontSize: 50, color: theme.colors.blue500 }}>
            {intensity != undefined && intensity !== 50 && intensity !== 0 && `${intensity}º`}
          </Text>
        </View>
        <View>
          <TouchableOpacity
            style={[styles.temperatureIcons, styles.boxShadow, { marginLeft: 4 }]}
            onPress={() => {
              const button = deviceMultimedia.buttons.find(({ buttonName: n }) => n === 'power-off');
              testControll(button);
              button != undefined && setIntensity(0);
            }}
            onLongPress={() => setControll('power-off')}
          >
            <Icon color={getColorEqual('power-off')} size={25} name='power' style={{ paddingRight: 5 }} />
            <Text>Desligar</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={styles.row}>
        <View>
          <TouchableOpacity
            style={[styles.temperatureIcons, styles.boxShadow, styles.tempHotAndWarmButton, { borderColor: 'blue' }]}
            onPress={() => setTempType('cold')}
            onLongPress={() => setControll('temp-hot')}
          >
            <Icon
              color={getColorEqual('temp-hot')}
              size={25}
              name='thermometer-chevron-down'
              style={{ paddingRight: 5 }}
            />
            <Text style={styles.text}>{'Frio'}</Text>
          </TouchableOpacity>
        </View>
        <View>
          <TouchableOpacity
            style={[styles.temperatureIcons, styles.boxShadow, styles.tempHotAndWarmButton, { borderColor: 'red' }]}
            onPress={() => setTempType('hot')}
            onLongPress={() => setControll('temp-hot')}
          >
            <Icon
              color={getColorEqual('temp-hot')}
              size={25}
              name='thermometer-chevron-up'
              style={{ paddingRight: 5 }}
            />
            <Text style={styles.text}>{'Quente'}</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={tempType == 'hot' ? { display: 'none' } : styles.row}>
        <View style={styles.commandsIcons}>
          <TouchableOpacity
            style={
              isForm && (getButton('temp-fifteen-' + temperatures[0]) || getButton('temp-fifteen'))
                ? styles.editButton
                : { display: 'none' }
            }
            onPress={() => setEditTemperature(0)}
          >
            <Icon color={theme.colors.red} size={22} name='circle-edit-outline' />
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.buttonsCommandsIcons, styles.boxShadow, styles.coldButton]}
            onPress={() => {
              const button = getButton('temp-fifteen');
              testControll(button);
              button != undefined && setIntensity(temperatures[0]);
            }}
            onLongPress={() => setControll('temp-fifteen-' + temperatures[0].toString())}
          >
            <Text style={[{ color: getColorIncludes('temp-fifteen') }, { fontSize: 25 }]}>{temperatures[0]}º</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.commandsIcons}>
          <TouchableOpacity
            style={
              isForm && (getButton('temp-nineteen-' + temperatures[0]) || getButton('temp-nineteen'))
                ? styles.editButton
                : { display: 'none' }
            }
            onPress={() => setEditTemperature(1)}
          >
            <Icon color={theme.colors.red} size={22} name='circle-edit-outline' />
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.buttonsCommandsIcons, styles.boxShadow, styles.coldButton]}
            onPress={() => {
              const button = getButton('temp-nineteen');
              testControll(button);
              button != undefined && setIntensity(temperatures[1]);
            }}
            onLongPress={() => setControll('temp-nineteen-' + temperatures[1].toString())}
          >
            <Text style={[{ color: getColorIncludes('temp-nineteen') }, { fontSize: 25 }]}>{temperatures[1]}º</Text>
          </TouchableOpacity>
        </View>
      </View>
      <View style={tempType == 'cold' ? { display: 'none' } : styles.row}>
        <View style={styles.commandsIcons}>
          <TouchableOpacity
            style={
              isForm && (getButton('temp-twenty-two-' + temperatures[0]) || getButton('temp-twenty-two'))
                ? styles.editButton
                : { display: 'none' }
            }
            onPress={() => setEditTemperature(2)}
          >
            <Icon color={theme.colors.red} size={22} name='circle-edit-outline' />
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.buttonsCommandsIcons, styles.boxShadow, styles.hotButton]}
            onPress={() => {
              const button = getButton('temp-twenty-two');
              testControll(button);
              button != undefined && setIntensity(temperatures[2]);
            }}
            onLongPress={() => setControll('temp-twenty-two-' + temperatures[2].toString())}
          >
            <Text style={[{ color: getColorIncludes('temp-twenty-two') }, { fontSize: 25 }]}>{temperatures[2]}º</Text>
          </TouchableOpacity>
        </View>
        <View style={styles.commandsIcons}>
          <TouchableOpacity
            style={
              isForm && (getButton('temp-thirty-' + temperatures[0]) || getButton('temp-thirty'))
                ? styles.editButton
                : { display: 'none' }
            }
            onPress={() => setEditTemperature(3)}
          >
            <Icon color={theme.colors.red} size={22} name='circle-edit-outline' />
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.buttonsCommandsIcons, styles.boxShadow, styles.hotButton]}
            onPress={() => {
              const button = getButton('temp-thirty');
              testControll(button);
              button != undefined && setIntensity(temperatures[3]);
            }}
            onLongPress={() => setControll('temp-thirty-' + temperatures[3].toString())}
          >
            <Text style={[{ color: getColorIncludes('temp-thirty') }, { fontSize: 25 }]}>{temperatures[3]}º</Text>
          </TouchableOpacity>
        </View>
      </View>
      <Portal>
        <CustomDialog visible={editTemperature != null} onDismiss={() => setEditTemperature()}>
          <Dialog.Title>Editar Temperatura</Dialog.Title>
          <Dialog.Content>
            <View>
              <TextInputInAPortal
                style={{ flexGrow: 1 }}
                autoFocus
                mode='outlined'
                placeholder='Temperatura'
                value={temperature}
                onChangeText={setTemperature}
              />
            </View>
          </Dialog.Content>
          <Dialog.Actions>
            <Button
              onPress={() => {
                setTemperature();
                setEditTemperature();
              }}
            >
              Fechar
            </Button>
            <Button
              onPress={() => {
                if (Number.isInteger(parseInt(temperature))) {
                  const temps = temperatures;
                  temps[editTemperature] = parseInt(temperature);
                  setTemperatures(temps);
                  const numbetToText = {
                    0: 'temp-fifteen',
                    1: 'temp-nineteen',
                    2: 'temp-twenty-two',
                    3: 'temp-thirty',
                  };
                  const button = getButton(numbetToText[editTemperature]);
                  if (button)
                    button.buttonName = numbetToText[editTemperature] + '-' + parseInt(temperature).toString();
                  setEditTemperature();
                  setTemperature();
                }
              }}
            >
              Confirmar
            </Button>
          </Dialog.Actions>
        </CustomDialog>
      </Portal>
    </View>
  );
}
const styles = StyleSheet.create({
  containerModal: {
    backgroundColor: theme.colors.gray200,
    marginBottom: 17,
    borderRadius: 17,
    justifyContent: 'space-between',
    alignItems: 'center',
    margin: 17,
    padding: 15,
    elevation: 5,
  },
  temperatureIcons: {
    backgroundColor: 'white',
    padding: 7,
    borderRadius: 30,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  temperatureCenter: {
    width: 90,
    borderRadius: 45,
    height: 90,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 5,
    borderWidth: 1,
    borderColor: theme.colors.gray500,
  },
  commandsIcons: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonsCommandsIcons: {
    backgroundColor: 'white',
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
    width: 60,
    height: 60,
    marginHorizontal: 5,
  },
  boxShadow: { elevation: 5, shadowColor: theme.colors.gray600 },
  editButton: { position: 'absolute', top: -13, left: -5, elevation: 6, backgroundColor: 'white', borderRadius: 10 },
  text: { fontSize: 12, color: theme.colors.gray600 },
  tempHotAndWarmButton: { marginRight: 10, width: 110, borderWidth: 2 },
  row: { flexDirection: 'row', marginTop: 6 },
  coldButton: { borderWidth: 1, borderColor: 'blue' },
  hotButton: { borderWidth: 1, borderColor: 'red' },
});
