import { createStackNavigator } from '@react-navigation/stack';
import { Buffer } from 'buffer';
import React, { useContext, useEffect } from 'react';
import { Alert, BackHandler, StatusBar } from 'react-native';
import { useTheme } from 'react-native-paper';
import dgram from 'react-native-udp';
import { AppContext } from '../lib/context';
import LoadingScreen from '../screens/LoadingScreen';

const PORT_APP = 8080;
const PORT_BROADCAST = 80;
const BROADCAST_ADDR = '255.255.255.255';
let server;
try {
  server = dgram.createSocket('udp4');
  server.bind(PORT_APP, () => server.setBroadcast(true));
} catch (error) {
  console.log('Error creating socket');
}

const Stack = createStackNavigator();

export default function LoadingNavigator() {
  const { SSID, PASSWORD, SECURITY, fromWidget, oldInstance, setOldInstance, createClient } = useContext(AppContext);

  useEffect(() => {
    const interval = setInterval(() => sendPayloadToBroadcast(), 1000);
    return () => clearInterval(interval);
  }, []);

  useEffect(() => {
    const interval = setInterval(() => {
      if (fromWidget > 0) {
        Alert.alert(
          'Widget não executado',
          'Não foi possível se conectar ao imóvel. Verifique a conexão e tente novamente.',
          [
            {
              text: 'Ok',
              onPress: () => {
                if (oldInstance) {
                  setOldInstance(oldInstance);
                  createClient(oldInstance);
                }
                BackHandler.exitApp();
              },
            },
          ],
          { cancelable: false },
        );
      }
    }, 5000);
    return () => clearInterval(interval);
  }, [fromWidget]);

  function sendPayloadToBroadcast() {
    try {
      console.log('context: ', SSID, PASSWORD, SECURITY);
      const sum = (data, res) => {
        for (let i = 0; i < data.length; ++i) res = res + data[i];
        return res;
      };

      let bytearray = [];
      for (let i = 0; i < 0x88; i++) bytearray.push(0x00);
      bytearray[0x26] = 0x14;

      let payload = Buffer.from(bytearray);

      for (let i = 0; i < SSID.length; i++) payload[68 + i] = SSID[i].charCodeAt(0);
      for (let i = 0; i < PASSWORD.length; i++) payload[100 + i] = PASSWORD[i].charCodeAt(0);

      payload[0x84] = SSID.length;
      payload[0x85] = PASSWORD.length;
      payload[0x86] = SECURITY;
      const checksum = sum(payload, 0xbeaf);
      payload[0x20] = checksum & 0xff;
      payload[0x21] = checksum >>> 8;
      server.send(payload, 0, payload.length, PORT_BROADCAST, BROADCAST_ADDR, error => {
        if (error) console.log(error);
        else console.log("Sent : '" + payload + "'");
      });
    } catch (error) {
      console.log('Error sendind UDP Package');
    }
  }

  const theme = useTheme();

  return (
    <>
      <StatusBar barStyle={'light-content'} backgroundColor={theme.colors.primary} />

      <Stack.Navigator>
        <Stack.Screen
          name='LoadingScreen'
          options={{
            headerTitle: 'Controllar',
            headerStyle: {
              backgroundColor: theme.colors.primary,
              elevation: 3,
            },
            headerTitleStyle: { color: theme.colors.background },
            // avoid header height flicker on app start
            // https://github.com/react-navigation/react-navigation/issues/5936
            // https://github.com/react-navigation/react-navigation/issues/7178
            safeAreaInsets: Platform.OS === 'android' ? { top: 0 } : {},
          }}
          component={LoadingScreen}
        />
      </Stack.Navigator>
    </>
  );
}
