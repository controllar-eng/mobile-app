import { createMaterialBottomTabNavigator } from '@react-navigation/material-bottom-tabs';
import React, { useContext, useEffect, useState } from 'react';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

import { useQuery } from '@apollo/client';
import { useIsFocused } from '@react-navigation/native';
import { Alert, Dimensions, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import { Portal, useTheme } from 'react-native-paper';
import DeviceGroupPicker from '../components/device/DeviceGroupPicker';
import InstancePicker from '../components/instance/InstancePicker';
import ProfilePicker from '../components/profile/ProfilePicker';
import { LIST_BUTTONS, TIMER_TYPE } from '../data/gql';
import { AppContext } from '../lib/context';
import { theme as _theme } from '../lib/utils';
import CountdownListScreen from '../screens/CountdownListScreen';
import DeviceListScreen from '../screens/DeviceListScreen';
import NotificationsListScreen from '../screens/NotificationsListScreen';
import OnOffListScreen from '../screens/OnOffListScreen';
import SceneListScreen from '../screens/SceneListScreen';

const isIOS = Platform.OS === 'ios';
const Tab = createMaterialBottomTabNavigator();

const ROUTES = [
  ['DeviceForm', { screenType: 'multimedia' }],
  ['SceneForm', { scene: { actions: [] } }],
  ['TimerForm', { timer: { type: TIMER_TYPE.ONOFF_DATE, actions: [] } }],
  ['TimerForm', { timer: { type: TIMER_TYPE.COUNTDOWN, actions: [] } }],
  ['DeviceForm', { screenType: 'emitter' }],
];

export default function TabNavigator({ route, navigation }) {
  const { deviceGroup, setDeviceGroup, selectedDevices, setSelectedDevices } = useContext(AppContext);
  const listButtons = useQuery(LIST_BUTTONS);
  const [oldversion, setOldVersion] = useState(false);
  const theme = useTheme();
  const isFocused = useIsFocused();
  const [dimensions] = useState(Dimensions.get('window'));
  const [tab, setTab] = useState('DeviceList');

  const tabTextName = {
    DeviceList: ['Novo Dispositivo', 'Novo Equipamento'],
    SceneList: ['Nova Cena'],
    OnOffList: ['Novo Timer On Off'],
    CountdownList: ['Novo Timer Regressivo'],
  };

  useEffect(() => {
    const subscription = Dimensions.addEventListener('change', () => {});
    if (subscription != undefined) return () => subscription.remove(); // doesn't crash, `subscription.emitter` is preserved
  }, []);

  useEffect(() => {
    if (listButtons.error != undefined) {
      setOldVersion(true);
    }
  }, [listButtons]);

  function devicesToActions(devices) {
    return devices.map(dev => ({
      code: dev.id,
      intensity: dev.on ? dev.intensity : 0,
    }));
  }

  return (
    <>
      <Portal>
        {tabTextName?.[tab]?.map((_, index) => {
          const shouldDisplayButton = route?.state?.index !== undefined && isFocused && selectedDevices.length === 0;
          const isMultimediaGroup = deviceGroup?.id === 'MULTIMEDIA';
          const bottomValue = isIOS ? 100 + 50 * index : 50 + 50 * index;

          return (
            <TouchableOpacity
              key={index}
              style={
                shouldDisplayButton
                  ? tab === 'DeviceList'
                    ? isMultimediaGroup
                      ? [styles.buttonAdd, { bottom: bottomValue }]
                      : { display: 'none' }
                    : [styles.buttonAdd, { bottom: bottomValue }]
                  : { display: 'none' }
              }
              onPress={() => {
                tab === 'DeviceList' && oldversion
                  ? Alert.alert(
                      'Atenção',
                      'Para as novas funções de multimídia, seu sistema precisará de atualizações. Entre em contato com um técnico para mais informações.',
                    )
                  : navigation.navigate.apply(null, ROUTES[route?.state?.index + index * 4]);
              }}
            >
              <Text style={styles.newButtonText}>{tabTextName[tab][index]} +</Text>
            </TouchableOpacity>
          );
        })}
      </Portal>
      <View
        style={[
          tab === 'NotificationsList' ? { display: 'none' } : styles.pickerRow,
          dimensions.width > 380 && { flexDirection: 'row-reverse' },
        ]}
      >
        <View style={[{ flexDirection: 'row' }, !(tab === 'DeviceList') && { width: '100%' }]}>
          <View style={styles.picker}>
            <InstancePicker />
          </View>
          <View style={styles.picker}>
            <ProfilePicker />
          </View>
        </View>
        {tab === 'DeviceList' && (
          <View style={styles.picker}>
            <DeviceGroupPicker
              deviceGroup={deviceGroup}
              setDeviceGroup={setDeviceGroup}
              oldVersion={oldversion}
              small={dimensions.width < 380}
            />
          </View>
        )}
      </View>
      <Tab.Navigator
        initialRouteName={'DeviceList'}
        barStyle={{ backgroundColor: theme.colors.background }}
        tabBarOptions={{ showIcon: true }}
        lazy
      >
        <Tab.Screen
          name='DeviceList'
          component={DeviceListScreen}
          listeners={{ focus: () => setTab('DeviceList') }}
          options={{
            title: (
              <Text
                style={selectedDevices.length > 0 ? { display: 'none', color: _theme.colors.gray100 } : styles.iconText}
              >
                Luminos
              </Text>
            ),
            tabBarIcon: ({ focused, color }) => (
              <Icon
                name='lightbulb-outline'
                size={24}
                color={focused ? _theme.colors.blue500 : color}
                style={selectedDevices.length > 0 && { display: 'none' }}
              />
            ),
          }}
        />
        <Tab.Screen
          name='SceneList'
          component={SceneListScreen}
          listeners={{
            focus: () => {
              if (selectedDevices.length > 0) {
                navigation.navigate('SceneForm', {
                  scene: { actions: devicesToActions(selectedDevices.reverse()) },
                });
                setSelectedDevices([]);
              } else setTab('SceneList');
            },
          }}
          options={{
            title: <Text style={styles.iconText}>Cenas</Text>,
            tabBarIcon: ({ focused, color }) => (
              <>
                <Icon
                  name='movie-outline'
                  size={24}
                  color={focused || selectedDevices.length > 0 ? _theme.colors.blue500 : color}
                />
                <Icon
                  name='plus'
                  size={20}
                  color={_theme.colors.blue500}
                  style={selectedDevices.length > 0 ? styles.plusIcon : { display: 'none' }}
                />
              </>
            ),
          }}
        />
        <Tab.Screen
          name='OnOffList'
          component={OnOffListScreen}
          listeners={{
            focus: () => {
              if (selectedDevices.length > 0) {
                navigation.navigate('TimerForm', {
                  timer: {
                    type: TIMER_TYPE.ONOFF_DATE,
                    actions: devicesToActions(selectedDevices),
                  },
                });
                setSelectedDevices([]);
              } else setTab('OnOffList');
            },
          }}
          options={{
            title: <Text style={styles.iconText}>On Off</Text>,
            tabBarIcon: ({ focused, color }) => (
              <>
                <Icon
                  name='alarm'
                  size={24}
                  color={focused || selectedDevices.length > 0 ? _theme.colors.blue500 : color}
                />
                <Icon
                  name='plus'
                  size={20}
                  color={_theme.colors.blue500}
                  style={selectedDevices.length > 0 ? styles.plusIcon : { display: 'none' }}
                />
              </>
            ),
          }}
        />
        <Tab.Screen
          name='CountdownList'
          component={CountdownListScreen}
          listeners={{
            focus: () => {
              if (selectedDevices.length > 0) {
                navigation.navigate('TimerForm', {
                  timer: {
                    type: TIMER_TYPE.COUNTDOWN,
                    actions: devicesToActions(selectedDevices),
                  },
                });
                setSelectedDevices([]);
              } else setTab('CountdownList');
            },
          }}
          options={{
            title: <Text style={styles.iconText}>Regressivo</Text>,
            tabBarIcon: ({ focused, color }) => (
              <>
                <Icon
                  name='timer-sand'
                  size={24}
                  color={focused || selectedDevices.length > 0 ? _theme.colors.blue500 : color}
                />
                <Icon
                  name='plus'
                  size={20}
                  color={_theme.colors.blue500}
                  style={selectedDevices.length > 0 ? styles.plusIcon : { display: 'none' }}
                />
              </>
            ),
          }}
        />
        <Tab.Screen
          name='NotificationList'
          component={NotificationsListScreen}
          listeners={{ focus: () => setTab('NotificationsList') }}
          options={{
            title: <Text style={selectedDevices.length > 0 ? { display: 'none' } : styles.iconText}>Notificações</Text>,
            tabBarIcon: ({ focused, color }) => (
              <Icon
                name='bell-outline'
                size={24}
                color={focused ? _theme.colors.blue500 : color}
                style={selectedDevices.length > 0 && { display: 'none' }}
              />
            ),
          }}
        />
      </Tab.Navigator>
    </>
  );
}

const styles = StyleSheet.create({
  pickerRow: { display: 'flex', marginTop: 4 },
  picker: { flexGrow: 1, marginRight: 2, marginLeft: 2 },
  alertUpControll: { alignItems: 'center', justifyContent: 'center', flexDirection: 'row' },
  buttonConfirm: { backgroundColor: _theme.colors.blue500, padding: 11, borderRadius: 20, marginRight: 20 },
  textButtonConfirm: { color: 'white', fontSize: 14 },
  iconText: { color: _theme.colors.blue500, fontSize: 10 },
  plusIcon: { position: 'absolute', bottom: -6, right: -6, backgroundColor: _theme.colors.gray100 },
  buttonAdd: {
    position: 'absolute',
    right: 30,
    borderRadius: 20,
    backgroundColor: 'white',
    padding: 10,
    borderWidth: 1,
    borderColor: _theme.colors.blue500,
  },
  newButtonText: { color: _theme.colors.blue500, fontSize: 12, fontWeight: 'bold' },
});
