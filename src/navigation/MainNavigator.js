import React, { useState, createContext } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { Colors, useTheme } from 'react-native-paper';
import TabNavigator from './TabNavigator';
import { StatusBar } from 'react-native';
import SceneFormScreen from '../screens/SceneFormScreen';
import TimerFormScreen from '../screens/TimerFormScreen';
import OnOffTimerScreen from '../screens/OnOffTimerScreen';
import CountdownTimerScreen from '../screens/CountdownTimerScreen';
import DeviceFormScreen from '../screens/DeviceFormScreen';

export const MainContext = createContext({});

const Stack = createStackNavigator();

export default function MainNavigator() {
  const theme = useTheme();
  const [tab, setTab] = useState(true);

  const [headerProps, setHeaderProps] = useState({});

  return (
    <>
      <StatusBar
        barStyle={tab ? 'light-content' : 'dark-content'}
        backgroundColor={tab ? theme.colors.primary : theme.colors.background}
      />
      <MainContext.Provider value={{ setHeaderProps }}>
        <Stack.Navigator>
          <Stack.Screen
            name='TabNavigator'
            options={{
              headerTitle: 'Controllar',
              headerStyle: {
                backgroundColor: theme.colors.primary,
                elevation: 3,
              },
              headerTitleStyle: { color: theme.colors.background },
              // avoid header height flicker on app start
              // https://github.com/react-navigation/react-navigation/issues/5936
              // https://github.com/react-navigation/react-navigation/issues/7178
              safeAreaInsets: Platform.OS === 'android' ? { top: 0 } : {},
              ...headerProps,
            }}
            component={TabNavigator}
            listeners={{ focus: () => setTab(true) }}
          />
          <Stack.Screen
            name='DeviceForm'
            options={{ headerTitle: 'Novo Dispositivo', headerTintColor: Colors.blue500 }}
            component={DeviceFormScreen}
            listeners={{ focus: () => setTab(false) }}
          />
          <Stack.Screen
            name='TimerForm'
            options={{ headerTitle: 'Novo Timer', headerTintColor: Colors.blue500 }}
            component={TimerFormScreen}
            listeners={{ focus: () => setTab(false) }}
          />
          <Stack.Screen
            name='OnOffTimer'
            options={{ headerTitle: 'Timer On Off', headerTintColor: Colors.blue500 }}
            component={OnOffTimerScreen}
            listeners={{ focus: () => setTab(false) }}
          />
          <Stack.Screen
            name='CountdownTimer'
            options={{ headerTitle: 'Timer Regressivo', headerTintColor: Colors.blue500 }}
            component={CountdownTimerScreen}
            listeners={{ focus: () => setTab(false) }}
          />
          <Stack.Screen
            name='SceneForm'
            options={{ headerTitle: 'Nova Cena', headerTintColor: Colors.blue500 }}
            component={SceneFormScreen}
            listeners={{ focus: () => setTab(false) }}
          />
        </Stack.Navigator>
      </MainContext.Provider>
    </>
  );
}
