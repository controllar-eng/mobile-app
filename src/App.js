import { ApolloProvider } from '@apollo/client';
import { NavigationContainer } from '@react-navigation/native';
import React, { useEffect, useRef, useState } from 'react';
import {
  DeviceEventEmitter,
  Dimensions,
  Image,
  Modal,
  NativeModules,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import 'react-native-gesture-handler';
import { ActivityIndicator, DefaultTheme as PaperDefaultTheme, Provider as PaperProvider } from 'react-native-paper';
import Icon from 'react-native-vector-icons/Ionicons';
import MDIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import './lib/whyDidYouRender';

import AsyncStorage from '@react-native-community/async-storage';
import update from './assets/update.png';
import { ALL } from './components/device/DeviceGroupPicker';
import DeviceSubscription from './components/device/DeviceSubscription';
import { SERVER_TOKEN } from './data/gql';
import { createApolloClient } from './lib/apollo';
import { AppContext, DEFAULT_PROFILE, INSTANCE_LOCAL_NET } from './lib/context';
import useAsyncStorage from './lib/useAsyncStorage';
import { theme as _theme } from './lib/utils';
import LoadingNavigator from './navigation/LoadingNavigator';
import MainNavigator from './navigation/MainNavigator';

// https://github.com/oblador/react-native-vector-icons/issues/945
Icon.loadFont();
MDIcon.loadFont();

const theme = {
  ...PaperDefaultTheme,
  colors: {
    ...PaperDefaultTheme.colors,
    primary: '#2196F3',
    accent: '#0288D1',
  },
};

const DEFAULT_SETTINGS = { profile: DEFAULT_PROFILE, deviceGroup: ALL };

function isValid(host) {
  return (
    host &&
    new RegExp(/^(([a-z0-9]|[a-z0-9][a-z0-9\-]*[a-z0-9])\.)*([a-z0-9]|[a-z0-9][a-z0-9\-]*[a-z0-9])(:[0-9]+)?$/).test(
      host.toLowerCase(),
    )
  );
}

function useSettings(id) {
  const [settings, setSettings, hydrated] = useAsyncStorage('@settings', {});

  // logCurrentStorage()

  if (!settings[id] && id) settings[id] = DEFAULT_SETTINGS;

  return {
    profile: (settings[id] || DEFAULT_SETTINGS).profile,
    setProfile: profile => setSettings({ ...settings, [id]: { ...settings[id], profile } }),
    deviceGroup: (settings[id] || DEFAULT_SETTINGS).deviceGroup,
    setDeviceGroup: deviceGroup => setSettings({ ...settings, [id]: { ...settings[id], deviceGroup } }),
  };
}

export default function App() {
  const [SSID, setSSID] = useState('');
  const [PASSWORD, setPASSWORD] = useState('');
  const [SECURITY, setSECURITY] = useState(3);
  const [BROADLINK_ERROR, SET_BROADLINK_ERROR] = useState('');
  const [APP_NOTIFICATIONS, SET_APP_NOTIFICATIONS] = useState([]);
  const [BROADLINKS_CONNECTEDS, SET_BROADLINKS_CONNECTEDS] = useState([]);
  const [selectedDevices, setSelectedDevices] = useState([]);

  const navigationRef = useRef();

  const [showPrompt, setShowPrompt] = useState(false);
  const [client, setClient] = useState();
  const [handleDeviceChange, setHandleDeviceChange] = useState();
  const [enableHelp, setEnableHelp] = useState(false);
  const [dimensions] = useState(Dimensions.get('window'));

  // instance list and current selected instance
  const [instanceList, setInstanceList] = useAsyncStorage('@instance_list', []);
  const [instance, setInstance, hydrated] = useAsyncStorage('@instance', INSTANCE_LOCAL_NET);
  const [oldInstance, setOldInstance] = useAsyncStorage('@old_instance', INSTANCE_LOCAL_NET);
  const [fromWidget, setFromWidget] = useState(0);

  // server token
  const [serverToken, setServerToken] = useState();

  useEffect(() => {
    const subscription = DeviceEventEmitter.addListener('widget04Clicked', async () => {
      await getWidgetInstance(4);
      setFromWidget(4);
      NativeModules.WidgetModule.resetWidget();
    });
    return () => subscription.remove();
  }, []);

  useEffect(() => {
    const subscription = DeviceEventEmitter.addListener('widget03Clicked', async () => {
      await getWidgetInstance(3);
      setFromWidget(3);
      NativeModules.WidgetModule.resetWidget();
    });
    return () => subscription.remove();
  }, []);

  useEffect(() => {
    const subscription = DeviceEventEmitter.addListener('widget02Clicked', async () => {
      await getWidgetInstance(2);
      setFromWidget(2);
      NativeModules.WidgetModule.resetWidget();
    });
    return () => subscription.remove();
  }, []);

  useEffect(() => {
    const subscription = DeviceEventEmitter.addListener('widget01Clicked', async () => {
      await getWidgetInstance(1);
      setFromWidget(1);
      NativeModules.WidgetModule.resetWidget();
    });
    return () => subscription.remove();
  }, []);

  useEffect(() => {
    NativeModules.WidgetModule.wasStartedFromWidget().then(async startedFromWidget => {
      if (startedFromWidget && startedFromWidget > 0) {
        await getWidgetInstance(startedFromWidget);
        setFromWidget(startedFromWidget);
        NativeModules.WidgetModule.resetWidget();
      }
    });
  }, []);

  useEffect(() => {
    async function fetchData() {
      if (client) {
        try {
          const { data } = await client.query({ query: SERVER_TOKEN });
          setServerToken(data.serverToken);
        } catch (e) {
          console.log('Unable to get server token', e);
        }
      }
    }
    fetchData();
  }, [client]);

  const settings = useSettings(serverToken && serverToken.id);

  useEffect(() => {
    if (hydrated) createClient(instance);
  }, [hydrated]);

  async function getWidgetInstance(startedFromWidget) {
    setOldInstance(instance);
    const instance = await AsyncStorage.getItem(`@widget_${startedFromWidget}_scene_instance`);
    const jsonInstance = JSON.parse(instance);
    createClient(jsonInstance || INSTANCE_LOCAL_NET);
  }

  function createClient(newInstance = instance) {
    if (client) client.stop();

    setInstance(newInstance);

    if (newInstance.id || isValid(newInstance.host)) {
      setClient(createApolloClient(newInstance, setShowPrompt));
    } else setClient(null);
  }

  useEffect(() => {
    loadTeste();
  }, []);

  async function loadTeste() {
    try {
      // eslint-disable-next-line camelcase
      const multimedia_updates = await AsyncStorage.getItem('@multimedia_updates');
      // eslint-disable-next-line camelcase
      const instance_list = await AsyncStorage.getItem('@instance_list');
      const settings = await AsyncStorage.getItem('@settings');
      // eslint-disable-next-line camelcase
      if (!multimedia_updates) {
        // App don't send update message for user
        AsyncStorage.setItem('@multimedia_updates', 'true');
        if (JSON.parse(instance_list) || JSON.parse(settings)) setEnableHelp(true); // User already use the app
      }
    } catch (e) {
      console.log('Error reading data');
    }
  }

  const ModalUpdates = (
    <Modal visible={enableHelp} transparent={true}>
      <View style={styles.opacityContainer}>
        <View style={styles.containerGray} />
      </View>
      <TouchableOpacity style={{ flex: 1 }} onPress={() => setEnableHelp(false)} />
      <View style={[styles.containerWhite, { height: dimensions.height / 2, marginBottom: dimensions.height / 4 }]}>
        <View style={styles.closeIconContainer}>
          <MDIcon name='close' color={_theme.colors.blue500} size={26} onPress={() => setEnableHelp(false)} />
        </View>
        <View style={styles.imageContainer}>
          <Image source={update} style={{ width: 70, height: 70 }} />
          <Text style={styles.title}>Novidades no{'\n'}seu Controllar Gestão</Text>
        </View>
        <View style={{ marginTop: 10, marginLeft: 20 }}>
          <Text style={{ fontWeight: 'bold', fontSize: 16 }}>Veja o que mudou em seu aplicativo:</Text>
        </View>
        <View style={styles.changesContainer}>
          <Text style={styles.circle}>●</Text>
          <Text style={styles.changes}>
            A barra de navegação que alterna os menus se encontra na parte inferior do aplicativo, facilitando o acesso
            rápido;
          </Text>
        </View>
        <View style={styles.changesContainer}>
          <Text style={styles.circle}>●</Text>
          <Text style={styles.changes}>
            Agora você pode controlar dispositivos multimídia como como sua TV e seu Ar Condicionado no seu aplicativo;
          </Text>
        </View>
        <View style={styles.changesContainer}>
          <Text style={styles.circle}>●</Text>
          <Text style={styles.changes}>
            Para as novas funções de multimídia, seu sistema precisará de atualizações. Entre em contato com um técnico
            para mais informações.
          </Text>
        </View>
      </View>
    </Modal>
  );
  return (
    <AppContext.Provider
      value={{
        setHandleDeviceChange,
        setShowPrompt,
        client,
        createClient,
        instanceList,
        instance,
        setInstanceList,
        setInstance,
        serverToken,
        SSID,
        setSSID,
        PASSWORD,
        setPASSWORD,
        SECURITY,
        setSECURITY,
        BROADLINK_ERROR,
        SET_BROADLINK_ERROR,
        APP_NOTIFICATIONS,
        SET_APP_NOTIFICATIONS,
        BROADLINKS_CONNECTEDS,
        SET_BROADLINKS_CONNECTEDS,
        selectedDevices,
        setSelectedDevices,
        fromWidget,
        setFromWidget,
        oldInstance,
        setOldInstance,
        ...settings,
      }}
    >
      <PaperProvider theme={theme}>
        <NavigationContainer ref={navigationRef} theme={theme}>
          <DeviceSubscription handleDeviceChange={handleDeviceChange} />
          {hydrated ? (
            client && !showPrompt ? (
              <ApolloProvider client={client}>
                {ModalUpdates}
                <MainNavigator />
              </ApolloProvider>
            ) : (
              <LoadingNavigator />
            )
          ) : (
            <View style={styles.loadingView}>
              <ActivityIndicator size='large' />
            </View>
          )}
        </NavigationContainer>
      </PaperProvider>
    </AppContext.Provider>
  );
}

const styles = StyleSheet.create({
  loadingView: { flex: 1, justifyContent: 'center', alignItems: 'center' },
  containerGray: { width: '100%', backgroundColor: _theme.colors.gray850, flex: 1 },
  opacityContainer: { opacity: 0.4, height: '100%', width: '100%', position: 'absolute' },
  containerWhite: { width: 'auto', backgroundColor: 'white', marginHorizontal: 20, borderRadius: 20 },
  closeIconContainer: {
    width: '100%',
    height: '10%',
    flexDirection: 'row',
    marginTop: 5,
    justifyContent: 'flex-end',
    paddingRight: 10,
  },
  imageContainer: {
    width: '100%',
    height: '20%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: { color: _theme.colors.blue500, fontSize: 22, fontWeight: 'bold', marginLeft: 30 },
  circle: { fontWeight: 'bold', fontSize: 16, paddingLeft: 10, color: _theme.colors.blue500 },
  changes: { fontSize: 14, paddingLeft: 10 },
  changesContainer: { flexDirection: 'row', marginTop: 10, paddingHorizontal: 10, marginRight: 25 },
});

// export default codePush({
//   checkFrequency: codePush.CheckFrequency.ON_APP_RESUME,
//   installMode: codePush.InstallMode.IMMEDIATE,
//   mandatoryInstallMode: codePush.InstallMode.IMMEDIATE,
// })(App);
