import { AppRegistry } from 'react-native';
import App from './src/App';
import { name } from './app.json';
import 'core-js/actual/queue-microtask';

AppRegistry.registerComponent(name, () => App);
